import setuptools

setuptools.setup(
    name="ipfnpytools",
    version="0.1.0",
    url="https://gitlab.com/ipfn-reflectometry/ipfnpytools.git",

    author="Diogo Aguiam",
    author_email="daguiam@ipfn.tecnico.ulisboa.pt",

    description="Set of standalone tools that aid in data analysis by the IPFN group",
    long_description=open('README.rst').read(),

    packages=setuptools.find_packages(),

    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],

    package_data={'': ['*.mplstyle']},

    scripts=['ipfnpytools/cutoffs.py']
)
