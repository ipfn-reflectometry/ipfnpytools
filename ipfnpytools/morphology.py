import numpy as np


def dilation_1d(arr, radius):
    """
    Perform dilation on a 1D NumPy ndarray.

    Parameters
    ----------
        arr: ndarray
            The input 1D ndarray.
        radius: int 
            The radius of the dilation. A radius of 1 means a kernel size of 3 samples. A radius zero has no effect.

    Returns
    -------
        dilated_arr: ndarray
            The dilated 1D ndarray with the same shape as the input.
    """
    # Create a padded array
    dilated_arr = np.pad(arr, pad_width=radius)
    
    # Create a view of the input array with a stride window of size 2*radius+1
    dilated_arr = np.lib.stride_tricks.sliding_window_view(dilated_arr, window_shape=(2 * radius + 1,))

    # Find the maximum value along the last axis of the window view
    dilated_arr = np.max(dilated_arr, axis=-1)

    return dilated_arr