import numpy as np
from scipy.interpolate import griddata
import aug_sfutils as sf


def trhop_to_mp(time, rho, mode, shot=None, equilibrium='EQH', eq_shotfile=None):
    """Map (time, rho) -> (time, major radius at the inner and/or outer midplane)
    
    Parameters
    ----------
    time: 1D array
        Time instants.
    rho: 1D array
        1D array the size of `time`. Rho poloidal coordinates to map.
    mode: str {'outer', 'inner', 'both'}
        Choose whether to map to the outer or inner midplanes, or both.
    shot: int
        Shot number, optional.
    equilibrium: str {'EQH', 'IDE'}
        Equilibrium shot file name, optional. If `shot` is provided, load the corresponding equilibrium.
    eq_shotfile: dd.Shotfile
        Equilibrium shot file with prefilled scalar fields, optional.
        
    Returns
    -------
    major_radius_inner/outer: 1D array
        1D array the size of `time` and `rho`. 
        (time, rho) mapped to the inner and/or outer midplanes, depending on the selected `mode`.
    """
    
    # Check what equilibrium parameters the user provided --------------------------
    if (eq_shotfile is None) == (shot is None):
        raise ValueError("User must provide `shot` or `eq_shotfile`, but not both")
        
    if eq_shotfile is None:
        eq = sf.EQU(shot, diag=equilibrium)
    else:
        eq = eq_shotfile
        
        
    # Find the index in the Z mesh that corresponds to z=0, aka the midplane -----
    try:
        z_zero_index = np.where(eq.Zmesh==0.0)[0][0]
    except IndexError as e:
        raise IndexError(e, "Zmesh does not contain the z=0 value")
        
    # Build a matrix that contains rho poloidal as a function of time and major radius
    reference_values = np.array(np.sqrt((eq.pfm - eq.psi0)/(eq.psix-eq.psi0)), dtype=np.float32)
    reference_values = reference_values[:, z_zero_index, :].T
    reference_time = eq.time
    reference_radius = eq.Rmesh
    
    # Expand time and radius axis into 2D matrices to match `reference_values`
    time_matrix = reference_time[:, None] * np.ones_like(reference_radius)
    radius_matrix = np.ones_like(reference_time)[:, None] * reference_radius
    
    def mask_and_interpolate(mask):
        reference_time_masked = time_matrix[mask]
        reference_radius_masked = radius_matrix[mask]
        reference_values_masked = reference_values[mask]
        # Interpolate
        return griddata(
            points=(reference_time_masked.astype(np.float32), reference_values_masked.astype(np.float32)),
            values=(reference_radius_masked.astype(np.float32)),
            xi=(time.astype(np.float32), rho.astype(np.float32))
        )
        
    if (mode == 'outer') or (mode == 'both'):
        # Calculate a mask for which rho(t, r) is injective and lies on the outer midplane
        major_radius_outer = mask_and_interpolate(np.diff(reference_values, append=np.inf) > 0)
      
    if (mode == 'inner') or (mode == 'both'):
        # Calculate a mask for which rho(t, r) is injective and lies on the inner midplane
        major_radius_inner = mask_and_interpolate(np.diff(reference_values, prepend=np.inf) < 0)
        
    if mode == 'both':
        return major_radius_inner, major_radius_outer
    elif mode == 'outer':
        return major_radius_outer
    elif mode == 'inner':
        return major_radius_inner