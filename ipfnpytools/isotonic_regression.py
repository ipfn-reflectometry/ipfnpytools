"""
Compute the isotonic regression of a sequence of values.
"""
from __future__ import division

from builtins import zip
from builtins import range
import copy

import numpy as np


def isotonic_regression_increasing(y):
    # TODO: examples
    """Compute the non-decreasing isotonic regression of a sequence of values.

    This routine uses the Pool-Adjacent-Violators Algorithm (PAVA) without
    weights and was adapted from
    http://tullo.ch/articles/speeding-up-isotonic-regression/

    Parameters
    ----------
    y : float sequence
        Sequence of input values.

    Returns
    -------
    float sequence
        Sequence of non-decreasing values which are as close as possible to
        the input values 'y' in the least-squares sense.

    Examples
    --------

    """

    n = len(y)

    if n <= 1:
        return y

    solution = copy.copy(y)

    n -= 1

    # Repeat until there are no more adjacent violators.
    while True:
        i = 0
        pooled = 0
        while i < n:
            k = i
            while k < n and solution[k] >= solution[k + 1]:
                k += 1
            if solution[i] != solution[k]:
                # solution[i:k + 1] is a decreasing subsequence, so replace
                # each point in the subsequence with its average.
                numerator = 0.0
                denominator = 0.0
                for j in range(i, k + 1):
                    numerator += solution[j]
                    denominator += 1
                for j in range(i, k + 1):
                    solution[j] = numerator / denominator
                pooled = 1
            i = k + 1

        # Check for convergence
        if pooled == 0:
            break

    return solution


# TODO: examples
def isotonic_regression_decreasing(y):
    """Compute the non-increasing isotonic regression of a sequence of values.

    This routine uses the Pool-Adjacent-Violators Algorithm (PAVA) without
    weights and was adapted from
    http://tullo.ch/articles/speeding-up-isotonic-regression/

    Parameters
    ----------
    y : float sequence
        Sequence of input values.

    Returns
    -------
    float sequence
        Sequence of non-increasing values which are as close as possible to
        the input values 'y' in the least-squares sense.

    Examples
    --------

    """

    n = len(y)

    if n <= 1:
        return y

    solution = copy.copy(y)

    n -= 1

    # Repeat until there are no more adjacent violators.
    while True:
        i = 0
        pooled = 0
        while i < n:
            k = i
            while k < n and solution[k] <= solution[k + 1]:
                k += 1
            if solution[i] != solution[k]:
                # solution[i:k + 1] is an increasing subsequence, so replace
                # each point in the subsequence with its average.
                numerator = 0.0
                denominator = 0.0
                for j in range(i, k + 1):
                    numerator += solution[j]
                    denominator += 1
                for j in range(i, k + 1):
                    solution[j] = numerator / denominator
                pooled = 1
            i = k + 1

        # Check for convergence
        if pooled == 0:
            break

    return solution


def monotonic_to_strict(y, y_iso):
    # TODO: examples
    """Return a strictly monotonic sequence close to a monotonic sequence.

    Parameters
    ----------
    y : float sequence
        Sequence of input values.
    y_iso : float sequence
        Isotonic regression of 'y'.

    Returns
    -------
    float sequence
        Sequence of strictly increasing / decreasing values close to the
        inputs.

    Examples
    --------

    """

    # Treat inputs as numpy arrays
    y = np.asfarray(y)
    y_iso = np.asfarray(y_iso)

    # Determine boundaries of constant value subsequences
    y_iso_next = np.roll(y_iso, -1)
    y_iso_prev = np.roll(y_iso, 1)
    begin_equal = np.flatnonzero(
        np.logical_and(y_iso == y_iso_next, y_iso != y_iso_prev))
    end_equal = np.flatnonzero(
        np.logical_and(y_iso == y_iso_prev, y_iso != y_iso_next))

    # Compute displacement of boundaries
    abs_diffs = np.abs([
        y_iso[begin_equal] - [y[begin_equal], np.roll(y_iso, 1)[begin_equal]],
        y_iso[end_equal] - [y[end_equal], np.roll(y_iso, -1)[end_equal]]])
    abs_diffs[abs_diffs == 0] = np.nan
    displacements = (np.nanmin(abs_diffs, axis=(0, 1)) / 3)

    # Linearly displace subsequences
    y_strict = copy.copy(y_iso)
    if y_strict[-1] >= y_strict[0]:
        for begin, end, d in zip(begin_equal, end_equal + 1, displacements):
            y_strict[begin:end] += np.linspace(-d, d, end - begin)
    else:
        for begin, end, d in zip(begin_equal, end_equal + 1, displacements):
            y_strict[begin:end] -= np.linspace(-d, d, end - begin)
    return y_strict


def isotonic_regression(y, increasing=None, strict=False):
    # TODO: examples
    """Compute the isotonic regression of a sequence of values.

    Parameters
    ----------
    y : float sequence
        Sequence of input values.
    increasing: bool, optional
        If True, compute the non-decreasing isotonic regression. If False,
        compute the non-increasing variant. The default variant is chosen
        based on the difference between the last and first value of 'y'.
    strict : bool, optional
        If True, compute a strictly monotonic sequence close to the
        conventional isotonic regression.

    Returns
    -------
        solution : float sequence
            Sequence of non-decreasing / non-increasing values which are as
            close as possible to the input values 'y' in the least-squares
            sense (or just close and strictly increasing / decreasing, if
            'strict' is True).

    Examples
    --------

    """

    # View input as a float array
    if isinstance(y, np.ndarray):
        y = np.asfarray(y)

    # Choose default variant (non-decreasing or non-increasing)
    if increasing is None:
        increasing = y[-1] >= y[0]

    # Compute conventional isotonic regression
    if increasing:
        y_iso = isotonic_regression_increasing(y)
    else:
        y_iso = isotonic_regression_decreasing(y)

    # Convert to strictly monotonic sequence if desired and return result
    if strict:
        return monotonic_to_strict(y, y_iso)
    return y_iso
