"""
Compute the normalized average position of Trues in a Boolean array.
"""
from __future__ import division

import numpy as np


def avg_true_position(flags):
    #TODO: example
    """Compute the normalized average position of Trues in a Boolean array.

    Parameters
    ----------
    flags : bool sequence
        Sequence of booleans.

    Returns
    -------
    float
        Average position of Trues in 'flags'.

    Examples
    --------

    """

    return (1 + 2 * np.mean(np.nonzero(flags))) / (2 * len(flags))
