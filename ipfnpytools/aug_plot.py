#TODO: docstring
"""
Functions to plot time traces and spectrograms of AUG and/or external signals.

Examples
--------

"""

from __future__ import division
from __future__ import absolute_import
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from . import aug_read
from .timing import timing
from .find_first import find_first
from .plot import use_base_style, plot_signals
from .mark_hop import mark_hop
from .save_figure import save_figure
from .fpe_to_ne import fpe_to_ne

#TODO: add function to plot main parameters
#TODO: add function to plot parameters to identify L-H transition (see jupyter)
#TODO: add functions to plot specgrams of interferometry and others (see IDL)

#TODO: add option to plot only specific bands
@timing
def fmcw(
        data=None, shot=0, t_begin=None, t_end=None, round_f_prob=2,
        round_n_e=1, LFS=True, HFS=True, write_f_prob=False, write_n_e=True,
        return_plot=True, read_settings_kw={}, **plot_signals_kw):
    # TODO: docstring
    """Plot signals of FMCW reflectometer in FF."""

    # Read reflectometer data, settings and names
    if data is None:
        data = aug_read.fmcw_data(
            shot=shot, t_begin=t_begin, t_end=t_end, LFS=LFS, HFS=HFS,
            **read_settings_kw)
    (
        LFS_data, HFS_data, shot, diagnostic, _, _, bands_LFS, bands_HFS,
        f_probings_Hz, _, _, _, _, f_sampling_Hz) = data

    # Define labels and filenames
    n_channels_LFS = len(bands_LFS)
    n_channels_HFS = len(bands_HFS)
    bands_LFS = np.array(bands_LFS, dtype=object)
    bands_HFS = np.array(bands_HFS, dtype=object)
    signal_names_LFS = bands_LFS + '-band'
    signal_names_HFS = bands_HFS + '-band'
    if write_f_prob:
        f_probing_strings = [
            ", {} GHz".format(round(f / 1E9, round_f_prob))
            for f in f_probings_Hz]
        signal_names_LFS += f_probing_strings[:n_channels_LFS]
        signal_names_HFS += f_probing_strings[:n_channels_HFS]
    if write_n_e:
        n_e_strings = [
            (
                ", {}x".format(round(fpe_to_ne(f) / 1E19, round_n_e)) +
                "$10^{19}$ $m^{-3}$")
            for f in f_probings_Hz]
        signal_names_LFS += n_e_strings[:n_channels_LFS]
        signal_names_HFS += n_e_strings[:n_channels_HFS]
    title_core = "shot {}, {} FMCW reflectometer ".format(shot, diagnostic)
    func_name = sys._getframe().f_code.co_name

    # Define frequency range to plot
    if diagnostic == "REF":
        f_max = None
    else:
        f_max = 400

    # Plot data
    default_plot_signals_kw = dict(
        t_begins=t_begin, t_ends=t_end, plot_types='specgram', f_maxs=f_max,
        y_title='Frequency (kHz)', z_title='Spectral power (a.u.)',
        write_f_label=False, write_c_label=False,
        specgram_kw=dict(fs=f_sampling_Hz))
    if LFS:
        default_plot_signals_kw.update(dict(
            data=LFS_data, labels=signal_names_LFS,
            title=(title_core + 'LFS'), postfixes=(func_name + '_LFS')))
        default_plot_signals_kw.update(plot_signals_kw)
        LFS_plot = plot_signals(
            return_plot=True, **default_plot_signals_kw)
    else:
        LFS_plot = None
    if HFS:
        default_plot_signals_kw.update(dict(
            data=HFS_data, labels=signal_names_HFS,
            title=(title_core + 'HFS'), postfixes=(func_name + '_HFS')))
        default_plot_signals_kw.update(plot_signals_kw)
        HFS_plot = plot_signals(
            return_plot=True, **default_plot_signals_kw)
    else:
        HFS_plot = None
    if return_plot:
        return LFS_plot, HFS_plot


# TODO: do not write probing frequencies ouside the plot
@timing
def hop(
        shot=0, experiment="AUGD", edition=0, channel=0, t_begin=None,
        t_end=None, data=None, mark="ne", full_steps=False,
        full_stairways=False, crop_invalid_samples=False, relative_phase=True,
        remove_offsets=True, plot_time=True, plot_IQ=False,
        print_settings=False, show=False, close=False, save_fig=False,
        return_plot=True, return_fig_name=True, mark_hop_kw={},
        **plot_signals_kw):
    # TODO: docstring
    # TODO: add features of IDL aug_rfl_hop_plot_data and kg8c_plot_data
    """Plot signals of hopping reflectometer."""

    # Get function name
    func_name = sys._getframe().f_code.co_name

    # Read reflectometer settings and data
    if data is None:
        data = aug_read.hop_data(
            shot=shot, experiment=experiment, edition=edition,
            channel=channel, t_begin=t_begin, t_end=t_end,
            full_steps=full_steps, full_stairways=full_stairways,
            crop_invalid_samples=crop_invalid_samples,
            remove_offsets=remove_offsets, print_settings=print_settings,
            close_shotfile=True, return_all=True)
    (
        shot, diagnostic, experiment, edition, band, time_s, signal_I,
        signal_Q, complex_signal, complex_phase, _, unwrapped_phase,
        amplitude, offsets, t_steps_start_s, t_steps_end_s,
        t_stairways_start_s, t_stairways_end_s, n_steps_stairway,
        n_samples_step, dt_step_s, t_begin_temp, t_end_temp, f_probings_Hz,
        n_e_probed_m03, f_sampling_Hz) = data[:-1]

    # If desired, remove samples in the beginning of each step where the
    # frequency hop has not been completed yet
    if crop_invalid_samples:
        valid_samples = aug_read.hop_valid_samples(
            t_steps_start_s, dt_step_s, time_s)
        time_s = time_s[valid_samples]
        signal_I = signal_I[valid_samples]
        signal_Q = signal_Q[valid_samples]
        complex_signal = complex_signal[valid_samples]
        complex_phase = complex_phase[valid_samples]
        unwrapped_phase = unwrapped_phase[valid_samples]
        amplitude = amplitude[valid_samples]

    # Adjust time range
    if t_begin is None:
        t_begin = t_begin_temp
    if t_end is None:
        t_end = t_end_temp
    if full_stairways:
        t_begin = t_stairways_start_s[max(
            0,
            np.searchsorted(t_stairways_start_s, t_begin, side='right') - 1)]
        t_end = t_stairways_end_s[min(
            len(t_stairways_end_s) - 1,
            np.searchsorted(t_stairways_end_s, t_end, side='left'))]
    elif full_steps:
        t_begin = t_steps_start_s[max(
            0, np.searchsorted(t_steps_start_s, t_begin, side='right') - 1)]
        t_end = t_steps_end_s[min(
            len(t_steps_end_s) - 1,
            np.searchsorted(t_steps_end_s, t_end, side='left'))]

    # Plots as a function of time:
    if plot_time:

        # Plot data
        if relative_phase:
            unwrapped_phase -= unwrapped_phase[find_first(time_s >= t_begin)]
        default_plot_signals_kw = dict(
            diagnostics=diagnostic, shots=shot,
            signals=[
                complex_signal + offsets, complex_phase, unwrapped_phase,
                amplitude],
            times=time_s,
            y_labels=["I+jQ", r"$e^{i \varphi}$", r"$\varphi$", "A"],
            signal_units=["V", "", "rad", "V"],
            labels=[
                "Complex (raw)", "Complex phase", "Unwrapped phase",
                "Amplitude"],
            title="{} hopping signals, shot {}, {}-band".format(
                diagnostic, shot, band),
            postfixes="{}_{}".format(func_name, band),
            specgram_kw=dict(fs=f_sampling_Hz))
        default_plot_signals_kw.update(plot_signals_kw)
        # TODO: phase scale in multiples of pi (couldn't find a robust way that is already done)
        fig, axes, fig_name = plot_signals(
            t_begins=t_begin, t_ends=t_end, return_plot=True,
            return_fig_name=True, save_fig=False,
            **default_plot_signals_kw)

        # Mark hopping steps and probing frequencies
        if mark in ["ne", "f"]:
            if mark == "f":
                units = "GHz"
                values = f_probings_Hz / 1E9
                n_compact_style = 5
                format_values = ".2f"
            elif mark == "ne":
                units = r"x$10^{19}$ m$^{-3}$"
                values = n_e_probed_m03 / 1E19
                n_compact_style = 4
                format_values = ".1f"
            default_mark_hop_kw = {
                "units": units, "pos": "bottom",
                "format_values": format_values,
                "n_compact_style":n_compact_style}
            default_mark_hop_kw.update(mark_hop_kw)
            for ax in axes:
                mark_hop(
                    values, t_steps_start_s, t_steps_end_s, axes=ax,
                    **default_mark_hop_kw)

        # Save figure if desired
        if save_fig:
            save_figure_kw = plot_signals_kw.get("save_figure_kw", {})
            save_figure(fig, fig_name, **save_figure_kw)

        # Return plot if desired
        if return_fig_name:
            time_plot = (fig, axes, fig_name)
        else:
            time_plot = (fig, axes)
        if return_plot and not plot_IQ:
            return time_plot

    # TODO: improve I/Q plot and put it in a separate function that is called here, receiving **kw
    # I/Q plot
    if plot_IQ:

        # Use isotropic style
        directory = os.path.dirname(__file__)
        use_base_style(directory)
        plt.style.use(os.path.join(directory, r'isotropic.mplstyle'))

        # Plot data
        indices = np.logical_and(time_s >= t_begin, time_s <= t_end)
        fig = plt.figure()
        axes = plt.gca()
        default_plot_kw = dict(linestyle='', marker='.', markeredgewidth=0)
        default_plot_kw.update(plot_signals_kw.get("plot_kw", {}))
        plt.plot(signal_I[indices], signal_Q[indices], **default_plot_kw)
        axes.axis('equal')
        plt.title("{} hop, #{}, {:.3f}-{:.3f}s, {}-band".format(
            diagnostic, shot, t_begin, t_end, band))
        "{} hop, #{}, {}-band".format(diagnostic, shot, band)
        plt.xlabel("I (V)")
        plt.ylabel("Q (V)")

        # Show figure
        if show:
            plt.show(block=False)

        # Define filename
        fig_name = "{}_{:.3f}-{:.3f}s_{}_{}_IQ".format(
            shot, t_begin, t_end, func_name, band)

        # Save figure if desired
        if save_fig:
            save_figure_kw = plot_signals_kw.get("save_figure_kw", {})
            save_figure(fig, fig_name, **save_figure_kw)

        # Close figure
        if close:
            plt.close(fig)

        # Return plot if desired
        if return_fig_name:
            IQ_plot = (fig, axes, fig_name)
        else:
            IQ_plot = (fig, axes)
        if return_plot and not plot_time:
            return IQ_plot
    if return_plot:
        return time_plot, IQ_plot


@timing
def mh(
        data_br=None, data_bp=None, shot=0, coils_br=None, coils_bp=None,
        br=True, bp=True, return_plot=True, **plot_signals_kw):
    # TODO: docstring
    # TODO: allow any number of coils to be plotted automatically write poloidal position according to each coil
    # TODO: add Br or Bpol to the title
    """Plot signals of magnetic coils."""

    # Define common variables
    func_name = sys._getframe().f_code.co_name
    default_plot_signals_kw = dict(
        plot_types='specgram', y_title='Frequency (kHz)',
        z_title='Spectral power (a.u.)', write_f_label=False,
        write_c_label=False, write_diagnostics=False)

    # Plot radial magnetic field
    if br:
        if data_br is None:
            data_br = aug_read.mh(shot=shot, coils_br=coils_br, bp=False)
        default_plot_signals_kw.update(dict(
            data=data_br,
            labels=(data_br.names + [' (LFS)', ' (HFS)', ' (top)']),
            postfixes=(func_name + '_Br')))
        default_plot_signals_kw.update(plot_signals_kw)
        br_plot = plot_signals(
            return_plot=True, **default_plot_signals_kw)
        if return_plot:
            if not bp:
                return br_plot

    # Plot poloidal magnetic field
    if bp:
        if data_bp is None:
            data_bp = aug_read.mh(shot=shot, coils_bp=coils_bp, br=False)
        default_plot_signals_kw.update(dict(
            data=data_bp,
            labels=(
                data_bp.names + [' (LFS)', ' (HFS)', ' (top)', ' (bottom)']),
            postfixes=(func_name + '_Bp')))
        default_plot_signals_kw.update(plot_signals_kw)
        bp_plot = plot_signals(
            return_plot=True, **default_plot_signals_kw)
        if return_plot:
            if not br:
                return bp_plot

    # Return plots
    if return_plot:
        return br_plot, bp_plot
