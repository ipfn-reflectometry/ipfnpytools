#TODO: dosctring
"""
Functions for spectral analysis.
"""

from __future__ import division
from __future__ import absolute_import
from builtins import zip
from past.builtins import basestring
import math
import warnings
from itertools import count
import numpy as np
from scipy import signal
from .sliding_window import sliding_window
from .has_even_sampling import has_even_sampling


def round_down(x, base=1):
    # TODO: examples
    """Round a number down to a nearest multiple of another number

    Parameters
    ----------
    x: int or float
        Number to round down.
    base:
        Number whose multiple will be the result of rounding down 'x'.

    Returns
    -------
        Biggest multiple of 'base' smaller than or equal to 'x'.

    """
    return x // base * base


def power_2_ceil(n):
    # TODO: examples
    """Return the smallest power of 2 greater than or equal to a positive
    integer.

    Parameters
    ----------
    n: int
        Positive integer.

    Returns
    -------
        Smallest power of 2 greater than or equal to 'n'.
    """

    return 1 << (n - 1).bit_length()


# TODO: add more windows (see scipy.signal.get_window)
# TODO: add computation when window is an array
def window_fres(window):
    # TODO: examples
    """Return normalized frequency resolution of window (3-dB width of the
    main lobe of the window spectrum).

    Parameters
    ----------
    window: str
        Window name.

    Returns
    -------
        Normalized frequency resolution of the window at -3dB. Returns None if
        'window' is not known or if it is not a string.

    """

    if not isinstance(window, basestring):
        raise ValueError("{} as window type is not supported.".format(
            type(window)))
    try:
        return dict(boxcar=0.9, triang=1.28, hann=1.44)[window]
    except KeyError:
        raise ValueError('Unknown window type "{}".'.format(window))


def coherency(x, y, **csd_kw):
    #TODO: docstring
    """Estimate the magnitude squared coherence, cross phase and power spectra
    of two signals using Welch's method."""

    f, pxx = signal.welch(x, **csd_kw)
    f, pyy = signal.welch(y, **csd_kw)
    f, pxy = signal.csd(x, y, **csd_kw)
    return f, np.abs(pxy) ** 2 / (pxx * pyy), np.angle(pxy), pxx, pyy


# TODO: allow non equally spaced segments (see IDL lg_xspecgram)
def default_specgram_parameters(
        n, t, t_begin, t_end, fmin, fmax, fs, ntpixels, nfpixels,
        nsubsegs_min, window, nperseg, npersubseg, noverlap, nsuboverlap,
        suboverlap, nfft, all_time, check_even_sampling, twosided, 
        **has_even_sampling_kw):
    #TODO: docstring
    """Compute default values of several spectrogram parameters."""

    # Check if data is evenly sampled and define default time range and
    # sampling frequency
    if t is not None:
        if len(t) != n:
            raise ValueError(
                "The time base and signal must have the same length.")
        if t_begin is None:
            t_begin = t[0]
        if t_end is None:
            t_end = t[n - 1]
        if all_time:
            index_real_begin = 0
            index_real_end = n
        else:
            index_real_begin, index_real_end = np.searchsorted(
                t, [t_begin, t_end])
        t_real_begin = t[index_real_begin]
        t_real_end = t[index_real_end - 1]
        if check_even_sampling and not has_even_sampling(
                t[index_real_begin:index_real_end], **has_even_sampling_kw):
            raise ValueError((
                "Data has uneven temporal sampling in the evaluated range" +
                " (indices {}:{}, corresponding to time {}-{}).".format(
                    index_real_begin, index_real_end, t_real_begin, 
                    t_real_end)))
    else:
        if fs is None:
            if t_begin is None:
                t_begin = 0
            if t_end is None:
                t_end = t_begin + n - 1
        else:
            if t_end is None:
                if t_begin is None:
                    t_begin = 0
                t_end = t_begin + (n - 1) / fs
            if t_begin is None:
                t_begin = t_end - (n - 1) / fs
        index_real_begin = 0
        index_real_end = n
        t_real_begin = t_begin
        t_real_end = t_end
    if fs is None:
        fs = (
            (index_real_end - index_real_begin - 1) / 
            (t_real_end - t_real_begin))

    # Compute normalized frequency resolution of window
    try:
        w_fres = window_fres(window)
    except ValueError as ex:
        warnings.warn(
            "not computing frequency resolution due to {}: {!r}".format(
                type(ex).__name__, ex.args),
            stacklevel=3)
        w_fres = np.nan

    # Define default frequency range
    if fmax is None:
        fmax = fs / 2
    if fmin is None:
        fmin = -fmax if twosided else 0

    # Determine visible portion of the signal
    index_visible_begin = (
        index_real_begin + int(math.ceil((t_begin - t_real_begin) * fs)))
    index_visible_end = (
        index_real_begin + 1 + int(math.floor((t_end - t_real_begin) * fs)))
    n_visible = index_visible_end - index_visible_begin

    # Compute default subsegment length for equal t and f resolution
    if npersubseg is None:
        f_factor = 2 * fs / (fmax - fmin) * (
            w_fres if np.isfinite(w_fres) else 1)
        npersubseg = int(round(min(
            np.sqrt(
                n_visible * (nfpixels / ntpixels) * f_factor / nsubsegs_min),
            nfpixels * f_factor)))

    # Determine default subsegment overlap
    if nsuboverlap is None:
        nsuboverlap = int(round(npersubseg * suboverlap))

    # Determine default segment length for the desired number of time pixels
    # (may result in more pixels to not waste data points)
    if nperseg is None:
        nperseg = max(n_visible // ntpixels, npersubseg * nsubsegs_min)
        nperseg = (
            nsuboverlap +
            round_down(nperseg - nsuboverlap, base=(npersubseg - nsuboverlap)))
    elif npersubseg > nperseg:
        npersubseg = nperseg

    # Determine default segment overlap for the desired number of time pixels
    if noverlap is None:
        noverlap = max(
            0,
            min(
                nperseg - 1,
                int(round((ntpixels * nperseg - n_visible) / (ntpixels - 1)))))

    # Determine default FFT length for power of 2 zero padding
    if nfft is None:
        nfft = power_2_ceil(
            max(npersubseg, int(nfpixels * fs / (fmax - fmin))))

    # Compute extended signal index range and initial time such that the
    # segments fully cover the desired time range
    delta_index = max(0, noverlap // 2)
    index_begin = max(0, index_real_begin - delta_index - 1)
    n_final = index_real_end + delta_index + 1 - index_begin
    n_final += -n_final % (nperseg - noverlap)
    index_end = min(n, index_begin + n_final)
    t_offset = t[index_begin] if t is not None else t_real_begin

    # Compute spectrogram shape and mean time of each segment
    nf = nfft if twosided else 1 + nfft // 2
    nstep = nperseg - noverlap
    nsegs = (index_end - index_begin - noverlap) // nstep
    tseg = t_offset + (np.arange(nsegs) * nstep + (nperseg - 1) / 2) / fs

    # Compute time and frequency resolutions
    tres = nperseg / fs
    fres = fs * w_fres / npersubseg

    # Return computed parameters
    return (
        index_begin, index_end, fmin, fmax, fs, nsegs, nf, nstep, nperseg,
        npersubseg, nsuboverlap, nfft, tseg, tres, fres)


def specgram(
        x, t=None, t_begin=None, t_end=None, fmin=None, fmax=None, fs=None,
        ntpixels=480, nfpixels=320, nsubsegs_min=1, window="hann",
        nperseg=None, npersubseg=None, noverlap=None, nsuboverlap=None,
        suboverlap=0.5, nfft=None, sort_f=True, all_time=False,
        check_even_sampling=True, return_all=False, return_onesided=True,
        has_even_sampling_kw={}, **welch_kw):
    # TODO: docstring
    """Compute the spectrogram of a signal using Welch's method."""

    # Determine signal length
    n = len(x)

    # Determine whether two sided spectra will be returned
    twosided = not return_onesided or np.iscomplexobj(x)

    # Compute default values of several spectrogram parameters
    (
        index_begin, index_end, fmin, fmax, fs, nsegs, nf, nstep, nperseg,
        npersubseg, nsuboverlap, nfft, tseg, tres, fres) = \
        default_specgram_parameters(
        n, t, t_begin, t_end, fmin, fmax, fs, ntpixels, nfpixels,
        nsubsegs_min, window, nperseg, npersubseg, noverlap, nsuboverlap,
        suboverlap, nfft, all_time, check_even_sampling, twosided,
        **has_even_sampling_kw)

    # Compute Welch spectrogram for the required portion of the signal
    sxx = np.empty((nf, nsegs))
    for i, x_ in enumerate(sliding_window(
            x[index_begin : index_end], nperseg, nstep, copy=False)):
        f, sxx[:, i] = signal.welch(
            x_, fs=fs, window=window, nperseg=npersubseg,
            noverlap=nsuboverlap, nfft=nfft, return_onesided=(not twosided),
            **welch_kw)

    # Sort frequency if desired
    if sort_f and f[-1] < 0:
        nroll = nf // 2
        f = np.roll(f, nroll)
        sxx = np.roll(sxx, nroll, axis=0)

    # Return spectrogram and other quantities if desired
    if return_all:
        return f, tseg, sxx, tres, fres, fmin, fmax, fs
    return f, tseg, sxx


def xspecgram(
        x, y, t=None, t_begin=None, t_end=None, fmin=None, fmax=None, fs=None,
        ntpixels=480, nfpixels=320, nsubsegs_min=1, window="hann",
        nperseg=None, npersubseg=None, noverlap=None, nsuboverlap=None,
        suboverlap=0.5, nfft=None, sort_f=True, all_time=False,
        check_even_sampling=True, return_all=False, return_onesided=True,
        has_even_sampling_kw={}, **csd_kw):
    # TODO: docstring
    """Compute the cross spectrogram of two signals using Welch's method."""

    # Determine signal length
    n = len(x)
    if len(y) != n:
        raise ValueError("The signals must have the same length.")

    # Determine whether two sided spectra will be returned
    twosided = not return_onesided or np.iscomplexobj((x,y))

    # Compute default values of several spectrogram parameters
    (
        index_begin, index_end, fmin, fmax, fs, nsegs, nf, nstep, nperseg,
        npersubseg, nsuboverlap, nfft, tseg, tres, fres) = \
        default_specgram_parameters(
        n, t, t_begin, t_end, fmin, fmax, fs, ntpixels, nfpixels,
        nsubsegs_min, window, nperseg, npersubseg, noverlap, nsuboverlap,
        suboverlap, nfft, all_time, check_even_sampling, twosided,
        **has_even_sampling_kw)

    # Compute Welch cross spectrogram for the required portion of the signals
    sxy = np.empty((nf, nsegs), dtype=complex)
    for i, x_, y_ in zip(
            count(),
            *[
                sliding_window(
                    a[index_begin : index_end], nperseg, nstep, copy=False)
                for a in [x, y]]):
        f, sxy[:, i] = signal.csd(
            x_, y_, fs=fs, window=window, nperseg=npersubseg,
            noverlap=nsuboverlap, nfft=nfft, return_onesided=(not twosided),
            **csd_kw)

    # Sort frequency if desired
    if sort_f and f[-1] < 0:
        nroll = nf // 2
        f = np.roll(f, nroll)
        sxy = np.roll(sxy, nroll, axis=0)

    # Return spectrogram and other quantities if desired
    if return_all:
        return f, tseg, sxy, tres, fres, fmin, fmax, fs
    return f, tseg, sxy


def coherogram(
        x, y, t=None, t_begin=None, t_end=None, fmin=None, fmax=None, fs=None,
        ntpixels=480, nfpixels=320, nsubsegs_min=10, window="hann",
        nperseg=None, npersubseg=None, noverlap=None, nsuboverlap=None,
        suboverlap=0.5, nfft=None, sort_f=True, all_time=False,
        check_even_sampling=True, return_all=False, return_onesided=True,
        has_even_sampling_kw={}, **csd_kw):
    # TODO: docstring
    """Compute the magnitude squared coherence, cross phase and power spectra
    of two signals as a function of time using Welch's method."""

    # Determine signal length
    n = len(x)
    if len(y) != n:
        raise ValueError("The signals must have the same length.")

    # Determine whether two sided spectra will be returned
    twosided = not return_onesided or np.iscomplexobj((x,y))

    # Compute default values of several spectrogram parameters
    (
        index_begin, index_end, fmin, fmax, fs, nsegs, nf, nstep, nperseg,
        npersubseg, nsuboverlap, nfft, tseg, tres, fres) = \
        default_specgram_parameters(
        n, t, t_begin, t_end, fmin, fmax, fs, ntpixels, nfpixels,
        nsubsegs_min, window, nperseg, npersubseg, noverlap, nsuboverlap,
        suboverlap, nfft, all_time, check_even_sampling, twosided,
        **has_even_sampling_kw)

    # Compute Welch coherogram for the required portion of the signals
    cxy, phaxy, sxx, syy = np.empty((4, nf, nsegs))
    for i, x_, y_ in zip(
            count(),
            *[
                sliding_window(
                    a[index_begin : index_end], nperseg, nstep, copy=False)
                for a in [x, y]]):
        f, cxy[:, i], phaxy[:, i], sxx[:, i], syy[:, i] = coherency(
            x_, y_, fs=fs, window=window, nperseg=npersubseg,
            noverlap=nsuboverlap, nfft=nfft, return_onesided=(not twosided),
            **csd_kw)

    # Sort frequency if desired
    if sort_f and f[-1] < 0:
        f, cxy, phaxy, sxx, syy = (
            np.roll(a, nf // 2, axis=0) for a in [f, cxy, phaxy, sxx, syy])

    # Return spectrogram and other quantities if desired
    if return_all:
        # TODO: Correct the estimate of the significance threshold (last return)
        return (
            f, tseg, cxy, phaxy, sxx, syy, tres, fres, fmin, fmax, fs,
            (nperseg - nsuboverlap) // (npersubseg - nsuboverlap))
    return f, tseg, cxy, phaxy, sxx, syy
