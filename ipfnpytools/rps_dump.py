"""This script is an example of how to extract the RPS information
from the dump files created by the rtrpfproc software"""

import numpy as np
from struct import unpack
import warnings


"""
Dump File logic. The following data are sequentially stored in the dump file.

name      | size (bytes)| position (bytes)                                | format  | description  
----------|-------------|-------------------------------------------------|---------|------------------
npf       | 4           | 0                                               | uint    | number of profiles (in time)
vres      | 4           | 4                                               | uint    | number of frequencies(densities)
nisos     | 4           | 8                                               | uint    | number of iso-densities
time      | 8*npf       | 12                                              | *double | time instants
fp        | 4*vres      | 12+8*npf                                        | *float  | probed frequencies
ne        | 4*vres      | 12+8*npf+4*vres                                 | *float  | probed densities
ne_iso    | 4*nisos     | 12+8*npf+8*vres                                 | *float  | iso-density values
gd_HFS    | 4*npf*vres  | 12+8*npf+8*vres+4*nisos                         | **float | high field side group delay
gd_LFS    | 4*npf*vres  | 12+8*npf+8*vres+4*nisos+4*npf*vres              | **float | low field side group delay
R_HFS     | 4*npf*vres  | 12+8*npf+8*vres+4*nisos+8*npf*vres              | **float | radius (HFS)
R_LFS     | 4*npf*vres  | 12+8*npf+8*vres+4*nisos+12*npf*vres             | **float | radius (LFS)
R_iso_HFS | 4*npf*nisos | 12+8*npf+8*vres+4*nisos+16*npf*vres             | **float | iso-density radius (HFS)
R_iso_LFS | 4*npf*nisos | 12+8*npf+8*vres+4*nisos+16*npf*vres+4*npf*nisos | **float | iso-density radius (LFS)
"""


class AreaBase:
    """Clone of the `dd.areaBase` class

    Parameters
    ----------
    data: ndarray
        Two-dimensional array representing some coordinate of a two-dimensional dataset.
    """

    def __init__(self, data=None):
        self.data = data


class SignalGroup:
    """Clone of the `dd.signalGroup` class

    Parameters
    ----------
    time: ndarray
        1-d array of the time coordinates of `data`.
    data: ndarray
        1-d or 2-d array of data. If 2-d, `time` is the coordinate of each row in `data`.
    area: AreaBase, optional
        Should only be provided if `data` is a 2-d array. Defaults to `None`.
    """

    def __init__(self, time=None, data=None, area=None):
        self.time = time
        self.data = data
        self.area = area


class DataChunk:
    """Properties of a dataset inside the dump file produced by the rtrpfproc software

    Parameters
    ----------
    format_char: str
        Format character corresponding to the data type according to the struct package.
        e.g. 'd' for 64-bit double, 'I' for 32-bit unsigned int, 'f' 32-bit float
    size: int
        Size in bytes of the data type
    position: int
        Position in bytes of the data in the dump file
    """

    def __init__(self, format_char, size, position):
        self.format_char = format_char
        self.size = size
        self.position = position


class ShotFile:
    """Clone of the `dd.shotfile` class. Implements only a subset of the methods.
    Reads the RPS diagnostic data from a binary dump file as if it were from a regular shotfile.

    Parameters
    ----------
    path: str
        Path to the dump file.

    """
    def __init__(self, path):

        self.path = path
        self._fo = None

        with open(self.path, "rb") as fo:
            self.npf = unpack("I", fo.read(4))[0]
            self.vres = unpack("I", fo.read(4))[0]
            self.nisos = unpack("I", fo.read(4))[0]

            self.t0 = unpack("d", fo.read(8))[0]
            self.t1 = unpack("d", fo.read(8))[0]
            self.dt = self.t1 - self.t0

        # Position in bytes of each data set inside the dump file
        self.time = DataChunk("d", 8, 12)
        self.fp = DataChunk("f", 4, 12+8*self.npf)
        self.ne = DataChunk("f", 4, 12+8*self.npf+4*self.vres)
        self.ne_iso = DataChunk("f", 4, 12+8*self.npf+8*self.vres)
        self.gd_hfs = DataChunk("f", 4, 12+8*self.npf+8*self.vres+4*self.nisos)
        self.gd_lfs = DataChunk("f", 4, 12+8*self.npf+8*self.vres+4*self.nisos+4*self.npf*self.vres)
        self.r_hfs = DataChunk("f", 4, 12+8*self.npf+8*self.vres+4*self.nisos+8*self.npf*self.vres)
        self.r_lfs = DataChunk("f", 4, 12+8*self.npf+8*self.vres+4*self.nisos+12*self.npf*self.vres)

    def __call__(self, name, dtype=None, tBegin=None, tEnd=None, calibrated=True):
        """Read a certain signal from the RPS diagnostic.
        If specified, `tBegin` and `tEnd` will trim the signal in time.

        Parameters
        ----------
        name: str
            Signal string identifier. Possible signals are
            ['RB_HFS', 'RB_LFS', 'FP_HFS', 'FP_LFS', 'TIME', 'neb_HFS', 'neb_LFS', 'GD_HFS', 'GD_LFS'].
        tBegin: float, optional
            If specified, the returned signal will begin at the first time sample larger than `tBegin`.
            Optional, defaults to None
        tEnd: float, optional
            If specified, the returned signal will end at the last time sample smaller than `tEnd`.
            Optional, defaults to None.
        calibrated: bool, optional
            If `True`, the signal is returned in SI units.
            Otherwise, the values are provided as stored in the dump file. Defaults to `True`.

        Returns
        -------
        signal: Union[SignalGroup, ndarray, AreaBase]
            Requested signal

        Raises
        ------
        ValueError:
            If the interval formed by [`tBegin`, `tEnd`] does not intercept the signal's own time interval.
            If `name` does not exist.
        """

        if tBegin is None:
            i0 = 0
        else:
            i0 = max((int(np.ceil((tBegin - self.t0) / self.dt)), 0))

        if tEnd is None:
            i1 = self.npf
        else:
            i1 = min((int((tEnd - self.t0) / self.dt), self.npf))

        if (i0 > self.npf) or (i1 < 0):
            raise ValueError("Time limits out of bounds")

        s = i1 - i0

        with open(self.path, "rb") as self._fo:

            if name == 'RB_HFS':
                r_hfs = self._load_r_hfs(i0, s)
                return AreaBase(r_hfs)

            if name == 'RB_LFS':
                r_lfs = self._load_r_lfs(i0, s)
                return AreaBase(r_lfs)

            if name == 'FP_HFS':
                fp = self._load_fp(i0, s)
                return AreaBase(fp)

            if name == 'FP_LFS':
                fp = self._load_fp(i0, s)
                return AreaBase(fp)

            time = self._load_data_chunk(self._fo, self.time, i0, s, np.float64)

            if name == 'TIME':
                return time

            if name == 'neb_HFS':
                ne = self._load_ne(i0, s)
                if calibrated:
                    ne *= 1e19
                r_hfs = self._load_r_hfs(i0, s)
                return SignalGroup(time=time, data=ne, area=AreaBase(r_hfs))

            if name == 'neb_LFS':
                ne = self._load_ne(i0, s)
                if calibrated:
                    ne *= 1e19
                r_lfs = self._load_r_lfs(i0, s)
                return SignalGroup(time=time, data=ne, area=AreaBase(r_lfs))

            if name == 'GD_HFS':
                gd_hfs = self._load_gd_hfs(i0, s)
                fp = self._load_fp(i0, s)
                return SignalGroup(time=time, data=gd_hfs, area=AreaBase(fp))

            if name == 'GD_LFS':
                gd_lfs = self._load_gd_lfs(i0, s)
                fp = self._load_fp(i0, s)
                return SignalGroup(time=time, data=gd_lfs, area=AreaBase(fp))

            raise ValueError("'{}' is not a valid name".format(name))

    @staticmethod
    def _load_data_chunk(fo, data, offset, elements, dtype=np.float32):
        fo.seek(data.position + offset * data.size)
        return np.array(unpack(str(elements) + data.format_char, fo.read(data.size * elements)), dtype=dtype)

    def _load_ne(self, i0, s):
        ne = self._load_data_chunk(self._fo, self.ne, 0, self.vres)
        ne = np.array([ne for _ in range(s)], dtype=np.float32)
        return ne

    def _load_r_hfs(self, i0, s):
        r_hfs = self._load_data_chunk(self._fo, self.r_hfs, i0 * self.vres, s * self.vres)
        r_hfs = r_hfs.reshape((s, self.vres))
        return r_hfs

    def _load_r_lfs(self, i0, s):
        r_lfs = self._load_data_chunk(self._fo, self.r_lfs, i0 * self.vres, s * self.vres)
        r_lfs = r_lfs.reshape((s, self.vres))
        return r_lfs

    def _load_fp(self, i0, s):
        fp = self._load_data_chunk(self._fo, self.fp, 0, self.vres)
        fp = np.array([fp for _ in range(s)], dtype=np.float32)
        return fp

    def _load_gd_hfs(self, i0, s):
        gd_hfs = self._load_data_chunk(self._fo, self.gd_hfs, i0 * self.vres, s * self.vres)
        gd_hfs = gd_hfs.reshape((s, self.vres))
        return gd_hfs

    def _load_gd_lfs(self, i0, s):
        gd_lfs = self._load_data_chunk(self._fo, self.gd_lfs, i0 * self.vres, s * self.vres)
        gd_lfs = gd_lfs.reshape((s, self.vres))
        return gd_lfs

    @staticmethod
    def close():
        warnings.warn("This dummy version of `ShotFile.close()` does nothing. "
                      "The shotfile is opened and closed automatically at each call of the `ShotFile` object")
        pass


def write_dump(time, frequency, density, rb_hfs, rb_lfs, gd_hfs, gd_lfs, path):
    """Write an RPS dump file from reflectometry data.
    
    Parameters
    ----------
    time: ndarray
        1D array of time instants of shape (t,)
    frequency: ndarray
        1D array of probing frequencies of shape (f,)
    density: ndarray
        1D array of corresponding probing densities of shape (f,)
    rb_hfs, rb_lfs, gd_hfs, gd_lfs: ndarray
        2D array with radius/group-delay for the HFS/LFS of shape (t, f,)
    path: str
        Path to write the dump file
    
    Note: 
        Dump file is written without any isodensities. All units SI.
    """
    
    with open(path, "wb") as f:
        np.array((rb_hfs.shape[0]), dtype=np.uint32).tofile(f)
        np.array((rb_hfs.shape[1]), dtype=np.uint32).tofile(f)
        
        np.array((10), dtype=np.uint32).tofile(f)
        
        np.array(time, dtype=np.float64).tofile(f)
        np.array(frequency, dtype=np.float32).tofile(f)
        np.array(density*1e-19, dtype=np.float32).tofile(f)
        
        np.array(np.zeros(10), dtype=np.float32).tofile(f)
        
        np.array(gd_hfs, dtype=np.float32).tofile(f)
        np.array(gd_lfs, dtype=np.float32).tofile(f)
        np.array(rb_hfs, dtype=np.float32).tofile(f)
        np.array(rb_lfs, dtype=np.float32).tofile(f)
        
        np.array(np.zeros((rb_hfs.shape[0], 10)), dtype=np.float32).tofile(f)
        np.array(np.zeros((rb_hfs.shape[0], 10)), dtype=np.float32).tofile(f)
        
        
"""
Dump File logic. The following data are sequentially stored in the dump file.

name      | size (bytes)| position (bytes)                                | format  | description  
----------|-------------|-------------------------------------------------|---------|------------------
npf       | 4           | 0                                               | uint    | number of profiles (in time)
vres      | 4           | 4                                               | uint    | number of frequencies(densities)
nisos     | 4           | 8                                               | uint    | number of iso-densities
time      | 8*npf       | 12                                              | *double | time instants
fp        | 4*vres      | 12+8*npf                                        | *float  | probed frequencies
ne        | 4*vres      | 12+8*npf+4*vres                                 | *float  | probed densities
ne_iso    | 4*nisos     | 12+8*npf+8*vres                                 | *float  | iso-density values
gd_HFS    | 4*npf*vres  | 12+8*npf+8*vres+4*nisos                         | **float | high field side group delay
gd_LFS    | 4*npf*vres  | 12+8*npf+8*vres+4*nisos+4*npf*vres              | **float | low field side group delay
R_HFS     | 4*npf*vres  | 12+8*npf+8*vres+4*nisos+8*npf*vres              | **float | radius (HFS)
R_LFS     | 4*npf*vres  | 12+8*npf+8*vres+4*nisos+12*npf*vres             | **float | radius (LFS)
R_iso_HFS | 4*npf*nisos | 12+8*npf+8*vres+4*nisos+16*npf*vres             | **float | iso-density radius (HFS)
R_iso_LFS | 4*npf*nisos | 12+8*npf+8*vres+4*nisos+16*npf*vres+4*npf*nisos | **float | iso-density radius (LFS)
"""
        