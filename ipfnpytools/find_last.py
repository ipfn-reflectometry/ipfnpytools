"""
Find the flattened indices of the last non-zero elements of an array.
"""

import numpy as np


def find_last(x, n=1):
    # TODO: example
    """Find the flattened indices of the last non-zero elements of an array.

    Parameters
    ----------
    x : ndarray
        Input array.
    n : int, optional
        Number of indexes to find.

    Returns
    -------
    int array
        Array containing the indices of the last 'n' non-zero elements of 'x'.

    Examples
    --------

    """

    return np.flatnonzero(x)[: -n - 1 : -1]
