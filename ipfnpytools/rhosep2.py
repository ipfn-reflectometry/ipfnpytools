from __future__ import division
from builtins import zip
from builtins import range
import warnings
from .getsig import getsig
import numpy as np


def rhosep2(shotnr, times=None, equil='GQH'):
    """
    Rho poloidal coordinate of the secondary separatrix
    
    Parameters
    ----------
    shotnr: int
        Shot number
    times: ndarray, optional
        If provided, specifies the time instants when the secondary separatrix is computed.
        If left unspecified, rhosep2 will return both an array of time instants and an array of rho poloidal coordinates.
    equil: str, optional
        Equillibrium shot file. Choose 'IDG' for the IDE equillibrium, or 'GQH' if IDE is not available. Default is 'GQH'.
        
    Returns
    -------
    rhosep2: ndarray or tuple of two ndarray
        1D array with the rho poloidal coordinates of the secondary separatrix, computed at the specified time instants, or a tuple with two 1D arrays where the first array contains the time instants stored in the equillibrium shotfile. 
    """
    
    dPsi_AxisSep1 = getsig(shotnr, equil, 'fax-bnd')
    dPsi_Sep1Sep2 = getsig(shotnr, equil, 'XPfdif')
    equilibrium_time = getsig(shotnr, equil, 'TIMEF')
    
    # GQH provides an 'XPfdif' always positive, while IDG provides both positive and negative values
    dPsi_Sep1Sep2 = np.abs(dPsi_Sep1Sep2)
    
    # To work in reversed current, take the module of fax-bnd
    dPsi_AxisSep1 = np.abs(dPsi_AxisSep1)
    
    if times is not None:
        dPsi_AxisSep1 = np.interp(times, equilibrium_time, dPsi_AxisSep1)
        dPsi_Sep1Sep2 = np.interp(times, equilibrium_time, dPsi_Sep1Sep2)
#         
        rho_sep2 = np.sqrt((dPsi_AxisSep1 + dPsi_Sep1Sep2)/dPsi_AxisSep1)
        return rho_sep2
    else:
        rho_sep2 = np.sqrt((dPsi_AxisSep1 + dPsi_Sep1Sep2)/dPsi_AxisSep1)
        return equilibrium_time, rho_sep2

