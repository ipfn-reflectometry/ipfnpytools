import numpy as np
import aug_sfutils as sf
from scipy.interpolate import interpn, RegularGridInterpolator


def trho_to_rz(time, rho, r, z, shot=None, equilibrium='EQH', eq_shotfile=None, max_iterations=3):
    """Convert time & rho poloidal to R and z coordinates along a specified line.
    
    Parameters
    ----------
    time: float, ndarray
        Time instants.
    rho: float, ndarray
        Rho poloidal coordinates.
    r: ndarray
        Array of radial coordinates defining the line.
    z: ndarray
        Array of height coordinates defining the line.
    shot: int
        Shot number.
    equilibrium: str {'EQH', 'IDE'}
        Equilibrium shot file name, optional. If `shot` is provided, load the corresponding equilibrium.
    eq_shotfile: dd.Shotfile
        Equilibrium shot file with prefilled scalar fields, optional.
    max_iterations: int, optional
        Maximum number of iterations to compute the intersection.
        
    Returns
    -------
    r, z: ndarray
        1D array of size N with the r (major radius) an z (height) of the point on the line with the specified rho.
    """
    

    # Check the validity of the arguments ------------------------------------------
    try:
        [time, rho] = np.broadcast_arrays(time, rho)
    except ValueError:
        raise ValueError("time and rho must be scalars or have equal shapes")

    try:
        [r, z] = np.broadcast_arrays(r, z)
    except ValueError:
        raise ValueError("r and z must be scalars or have equal shapes")

    shape = time.shape

    # Check what equilibrium parameters the user provided --------------------------
    if (eq_shotfile is None) == (shot is None):
        raise ValueError("User must provide `shot` or `eq_shotfile`, but not both")

    # Check what equilibrium parameters the user provided --------------------------
    if eq_shotfile is not None:
        eq = eq_shotfile
    else:
        try:
            eq = sf.EQU(shot, diag=equilibrium)
        except TypeError:
            raise ValueError("You must provide either `shot` or `eq_shotfile`")

    # Algorithm definitions -------------------------------------------------------

    # Number of points on the line used for sucessive aproximation. MUST BE EVEN.
    resolution = 10

    # Data type, float32 will typically be faster
    data_type = np.float32

    # Array to hold the solutions to the mapping problem
    answer = np.full((time.size, 2), np.nan)

    # Produce arrays for interpolation ---------------------------------------------
    time_grid, r_grid = np.meshgrid(time.flatten(), r, indexing='ij')
    time_grid, z_grid = np.meshgrid(time.flatten(), z, indexing='ij')

    # # Begin the iterative algorithm ------------------------------------------------
    for iteration in range(max_iterations):

        # Define the interpolator, only on the first loop
        try:
            interpolator

        except NameError:

            interpolator = RegularGridInterpolator(
                points=( 
                    np.array(eq.Rmesh, dtype=data_type), 
                    np.array(eq.Zmesh, dtype=data_type),
                    np.array(eq.time, dtype=data_type),
                ), 
                values=np.array(np.sqrt((eq.pfm - eq.psi0)/(eq.psix-eq.psi0)), dtype=data_type), 
                bounds_error=False,
                fill_value=np.nan,
            )

        # Interpolate the grid points (t, Z, R) mapping them to rho
        rho_rzt = interpolator(
            np.array([
                r_grid.flatten(),
                z_grid.flatten(),
                time_grid.flatten(),
            ]).astype(data_type).T
        )

        # Difference between rho(t, r, z) and target rho 
        stage1 = np.reshape(rho_rzt, (len(time), r_grid.shape[-1])) - rho[:, None]

        # Roll this difference and multiply by it self
        stage2 = np.roll(stage1, shift=-1, axis=-1)
        # stage3 will be negative when the rho(t, r, z) field crosses the target rho
        stage3 = stage2 * stage1

        # Find the points on the grid adjacent to the desired flux surfaces
        before_crossing = (stage3 < 0)
        # Prevent wrap-around errors
        before_crossing[:, -1] = False

        # Determine which time instants had one and only one solution and void the other solutions
        valid_solutions = np.sum(before_crossing, axis=-1) == 1
        before_crossing[~valid_solutions] = False

        # For the time instants that had zero or more than one solutions, 
        # pretend there was only one solution at the beginning for the sole purpose of redefining the search grid
        before_crossing[~valid_solutions, 0] = True 

        # Find the points after the crossing
        after_crossing = np.roll(before_crossing, shift=1, axis=-1)

        # Substitute in answer
        answer[valid_solutions, 0] = (r_grid[valid_solutions][before_crossing[valid_solutions]] + r_grid[valid_solutions][after_crossing[valid_solutions]]) / 2
        answer[valid_solutions, 1] = (z_grid[valid_solutions][before_crossing[valid_solutions]] + z_grid[valid_solutions][after_crossing[valid_solutions]]) / 2

        if iteration == (max_iterations - 1):
            break

        else:  # I know this else is irrelevant, it's just good practice

            # Redefine the search grid
            r_grid = np.linspace(r_grid[before_crossing], r_grid[after_crossing], resolution).T
            z_grid = np.linspace(z_grid[before_crossing], z_grid[after_crossing], resolution).T
            time_grid, _ = np.meshgrid(time.flatten(), np.arange(resolution), indexing='ij')


    return answer