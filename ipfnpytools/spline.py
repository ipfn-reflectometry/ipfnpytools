"""
One-dimensional smoothing spline fit to a given set of data points, with
optional constraints on derivatives, knot optimization and other features.

Initially adapted from
https://stackoverflow.com/questions/32046582/spline-with-constraints-at-border
"""

from __future__ import division
from builtins import zip
import warnings
import numpy as np
from scipy.interpolate import UnivariateSpline, splev, splrep
from scipy.optimize import minimize, minimize_scalar
from .arguments import extend_args


def quadratic_spline_roots(spl, x_begin=None, x_end=None):
    # TODO: docstring
    """Compute the roots of a quadratic spline. Adapted from:
    https://stackoverflow.com/questions/50371298/find-maximum-minimum-of-a-1d-interpolated-function"""

    # Get spline knots in the desired range
    knots = spl.get_knots()
    if x_begin is None:
        x_begin = knots[0]
    if x_end is None:
        x_end = knots[-1]
    knots = np.concatenate([
        [x_begin],
        knots[(knots > x_begin) & (knots < x_end)],
        [x_end]])

    # Compute spline roots
    roots = []
    for a, b in zip(knots[:-1], knots[1:]):
        u, v, w = spl(a), spl((a+b)/2), spl(b)
        t = np.roots([u+w-2*v, w-u, 2*v])
        t = t[np.isreal(t) & (np.abs(t) <= 1)]
        roots.extend(t*(b-a)/2 + (b+a)/2)
    return np.array(roots)


def total_y_squared_distance(c, x, y, t, k, w=None):
    """Error function to minimize when optimizing spline coefficients."""
    distances = y - splev(x, (t, c, k))
    if w is not None:
        distances *= w
    return np.einsum('...i,...i', distances, distances)


def split_tc(tc, x, k):
    """Split the combined tc array (with non-redundant t) into separate t
    (redundant) and c arrays."""
    l = k + 1
    t, c = np.split(tc, [-(int(tc.size / 2) + l)])
    t = np.concatenate(([x[0]] * l, t, [x[-1]] * l))
    return t, c


def total_y_squared_distance_tc(tc, x, y, k, w=None):
    """Error function to minimize when optimizing spline knots and
    coefficients."""
    t, c = split_tc(tc, x, k)
    return total_y_squared_distance(c, x, y, t, k, w=w)


def total_y_distance(c, x, y, t, k, w=None):
    """Error function to minimize when optimizing spline coefficients for
    minimum vertical distance: sum of the weighted vertical distances between
    the spline and the points."""
    distances = np.abs(y - splev(x, (t, c, k)))
    if w is not None:
        distances *= w
    return np.sum(distances)


def squared_distance_to_spline(x_spline, x, y, t, c, k, w, wx, wy):
    """Squared weighted orthogonal distance between spline at 'x_spline' and
    point ('x','y')."""
    return (
        (w * wx * (x - x_spline)) ** 2 +
        (w * wy * (y - splev(x_spline, (t, c, k)))) ** 2)


def distance_to_spline(x_spline, x, y, t, c, k, w, wx, wy):
    """Weighted orthogonal distance between spline at 'x_spline' and point
    ('x','y')."""
    return np.sqrt(squared_distance_to_spline(
        x_spline, x, y, t, c, k, w, wx, wy))


# TODO: optimize computation of minimum distances (add inital guess? scipy.odr?)
def total_squared_distance(c, x, y, t, k, w, wx, wy):
    """Error function to minimize when optimizing spline coefficients for
    minimum orthogonal squared distance: sum of the squared weighted
    orthogonal distances between the spline and the points."""
    total = 0
    for x_i, y_i, w_i, wx_i, wy_i in zip(x, y, w, wx, wy):
        total += minimize_scalar(
            squared_distance_to_spline,
            args=(x_i, y_i, t, c, k, w_i, wx_i, wy_i)).fun
    return total


def total_distance(c, x, y, t, k, w, wx, wy):
    """Error function to minimize when optimizing spline coefficients for
    minimum orthogonal distance: sum of the weighted orthogonal distances
    between the spline and the points."""
    total = 0
    for x_i, y_i, w_i, wx_i, wy_i in zip(x, y, w, wx, wy):
        total += minimize_scalar(
            distance_to_spline, args=(x_i, y_i, t, c, k, w_i, wx_i, wy_i)).fun
    return total


_mono_options_inc = ['inc', 'increasing']
_mono_options_dec = ['dec', 'decreasing']


# TODO: add option for positive (negative) spline (aproveitar a parte dos extremos da monotonicidade, mas sem as diferencas)
# TODO: add option for positive (negative) derivatives of arbitrary order across whole domain or specific regions
def constrained_spline(
        x, y, w=None, wx=None, wy=None, s_factor=1, s=None, n_knots=None, 
        knot_dist='quantile', t=None, min_knot_sep=0, bc_type=None, 
        x_no_deriv=None, x_no_2nd_deriv=None, x_neg_2nd_deriv=None, 
        x_pos_2nd_deriv=None, deriv_constraints=None, x_begin=None, x_end=None, 
        mono=None, ext=0, squared_distance=True, odr=None, 
        optimize_knots=False, minimize_kw=None, **splrep_kw):
    #TODO: docstring
    """Find the B-spline representation of a 1-D curve with optional 
    constraints on derivatives, knot optimization and other features.

    Given the set of data points ``(x[i], y[i])`` determine a smooth spline
    approximation of degree ``k`` (default 3).
    
    Smoothness of spline can be controlled in several ways:
    - defining ``s`` or ``s_factor``: measure of the squared error between spline and data points 
    (knots are placed until condition below is satisfied) 
    ``sum((w[i] * (y[i]-spl(x[i])))**2, axis=0) <= s``.
    ``s`` and ``s_factor`` relate as follows ``s=s_factor*len(x)``.
    - defining ``n_knots`` (number of internal knots in spline) and 
      ``knot_dist`` (knot distribution: either linearly spaced or by quantiles (default)).
    - Providing the exact position of each internal knot using array ``t``.
    
    If given both ``s_factor`` (or ``s``) and ``n_knots`` (or ``t``), spline is constructed
    using set of knots ``t`` and s parameter is neglected.

    Parameters
    ----------
        x : array_like 
            The data points defining a curve y = f(x). 1-D array of independent input data. 
            Must be increasing; must be strictly increasing if `s` is 0.
        y : array_like
            The data points defining a curve y = f(x).
        w : array_like, optional
            Strictly positive rank-1 array of weights the same length as x and y.
            The weights are used in computing the weighted least-squares spline
            fit. If the errors in the y values have standard deviation given by the
            vector d, then w should be 1/d. Default is ones(len(x)).
        wx, wy : array_like, optional
            If using orthogonal distance regression-like methods, 
            w*wx and w*wy are the weights in the x and y direction, respectively.
            Default is np.ones_like(y).
        s_factor : float, optional
            s=s_factor*len(x). See s description. Defaults to 1.
        s : float, optional
            A smoothing condition. The amount of smoothness is determined by satisfying 
            the condition: sum((w * (y - g))**2,axis=0) <= s where g(x) is the smoothed 
            interpolation of (x,y). The user can use s to control the tradeoff between 
            closeness and smoothness of fit. Larger s means more smoothing while smaller 
            values of s indicate less smoothing. Recommended values of s depend on the
            weights, w. If the weights represent the inverse of the standard deviation 
            of y, then a good s value should be found in the range (m-sqrt(2*m),m+sqrt(2*m))
            where m is the number of datapoints in x, y, and w. Overrides s_factor. 
            Defaults to s_factor*len(x).
        n_knots : int, optional
            Number of internal knots to consider in spline. This overrides the s parameters.
            By default, knots are distributed by quantiles. 
            If set to None, smoothing condition applies. Defaults to None.
        knot_dist : str: 'linear' or 'quantile', optional)
            Knots distribution: if set to linear, knots are linearly
            distributed through the x data (i.e. equally spaced);
            If set to 'quantile', knots are distributed in quantiles 
            (i.e. the number of data point between each two consecutive 
            knots is the same). Defaults to 'quantile'.
        t : array_like, optional
            Specify knot positions to use in spline. This overrides n_knots. Defaults to None.
        min_knot_sep : float, optional
            Use to impose minimal distance between consecutive knots.
            Usefull to respect Schoenberg-Whitney conditions when optimizing knot positions. 
            Defaults to 0.
        bc_type : {'clamped', 'natural', 'linear', list), optional
            Specify boundary conditions.
            ``clamped``: first derivative of spline is set to 0 at the endpoint(s).
            ``natural``: second derivative of spline is set to 0 at the endpoint(s). (only for k>1)
            ``linear``: second derivative of spline is set to 0 between the endpoint(s) 
                        and closest knot(s). (only implemented for k=3 at the moment)
            Defaults to None.
        x_no_deriv : array_like, optional
            Array of points where to impose spline's derivative equal to 0.
            Defaults to None.
        x_no_2nd_deriv : array_like, optional 
            Array of points where to impose spline's second derivative equal to 0. (only for k>1)
            Defaults to None.
        x_neg_2nd_deriv : array_like, optional
            Array of points where to impose spline's second derivative smaller than or equal to 0. (only for k>1)
            Defaults to None.
        x_pos_2nd_deriv : array_like, optional
            Array of points where to impose spline's second derivative larger than or equal to 0. (only for k>1)
            Defaults to None.
        deriv_constraints : list, optional
            Additional constraints on the spline values and/or derivatives at arbitrary points (syntax to be explained). Defaults to None.
        x_begin : float, optional
            x position where to start fitting the spline.
            Defaults to x[0].
        x_end : float, optional
            x position where to stop fitting the spline.
            Defaults to x[-1].
        mono : str {'dec', 'decreasing', 'inc', 'increasing'}, optional 
            Forces the spline to be monotonic (only implemented for k<4 at the moment):
                ``dec`` or ``decreasing``: first derivative of spline smaller than or equal to 0 throughout.
                ``inc`` or ``increasing``: first derivative of spline greater than or equal to 0 throughout.
            Defaults to None.
        ext : int or str, optional
            Controls the extrapolation mode for elements not in the interval 
            defined by the knot sequence.
                if ext=0 or ''extrapolate'', return the extrapolated value.
                if ext=1 or ''zeros'', return 0
                if ext=2 or ''raise'', raise a ValueError
                if ext=3 or ''const'', return the boundary value.
            Defaults to 0.
        squared_distance : bool, optional
            Whether to minimize the squared distance when fitting the spline. 
            If set to False, minimizes the absolute distance instead. Defaults to True.
        odr : bool, optional
            Whether to perform orthogonal distance regression. 
            If set to False, performs a conventional fit using distances only along the y axis instead.
            Defaults to False if both wx and wy are None and to True otherwise.
        optimize_knots : bool, optional
            Whether to optimize knot positions numerically. Defaults to False.
        minimize_kw : dict, optional
            Keyword arguments passed to optimizing routine ``scipy.optimize.minimize``.
            Defaults to None.

    Raises:
        ValueError: If `mono` is not a valid option.

    Returns:
        UnivariateSpline object: See https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.UnivariateSpline.html
    """


    # Print all warnings, even if repeated
    warnings.simplefilter('always', UserWarning)

    # Check for keyword errors
    if mono is not None and mono not in _mono_options_inc + _mono_options_dec:
        raise ValueError("{} is not a valid mono option.".format(mono))

    # Define default keywords
    if x_begin is None:
        x_begin = np.min(x)
    if x_end is None:
        x_end = np.max(x)
    bc_type, = extend_args(bc_type, n=2)
    if x_no_deriv is None:
        x_no_deriv = []
    if x_no_2nd_deriv is None:
        x_no_2nd_deriv = []
    if x_neg_2nd_deriv is None:
        x_neg_2nd_deriv = []
    if x_pos_2nd_deriv is None:
        x_pos_2nd_deriv = []
    x_no_deriv, x_no_2nd_deriv, x_neg_2nd_deriv, x_pos_2nd_deriv = (
        np.atleast_1d(
            x_no_deriv, x_no_2nd_deriv, x_neg_2nd_deriv, x_pos_2nd_deriv))
    if deriv_constraints is None:
        deriv_constraints = []
    if odr is None:
        odr = wx is not None or wy is not None
    if minimize_kw is None:
        minimize_kw = {}

    # Select data
    mask = (x >= x_begin) & (x <= x_end)
    x, y = (a[mask] for a in (x, y))
    if w is not None:
        w = w[mask]
    if wx is not None:
        wx = wx[mask]
    if wy is not None:
        wy = wy[mask]

    # TODO: choose knots optimally by splrep
    # Set default smoothing and knots
    if s is None:
        s = s_factor * len(x)
    if t is None and n_knots is not None:
        if knot_dist == 'quantile':
            t = np.quantile(x, np.linspace(0, 1, n_knots + 2))[1:-1]
        elif knot_dist == 'linear':
            t = np.linspace(x[0], x[-1], n_knots + 1, endpoint=False)[1:]
        else:
            raise ValueError("{} is not a valid knot_dist option.".format(knot_dist))

    # Set default weights
    if odr is not None:
        if w is None:
            w = np.ones_like(y)
        if wx is None:
            wx = np.ones_like(y)
        if wy is None:
            wy = np.ones_like(y)

    # TODO: add 'periodic' boundary conditions
    # Set boundary conditions
    for i, (bc, x_bc) in enumerate(zip(bc_type, [x[0], x[-1]])):
        if bc == 'clamped':
            x_no_deriv = np.append(x_no_deriv, x_bc)
        elif bc == 'natural':
            x_no_2nd_deriv = np.append(x_no_2nd_deriv, x_bc)
        elif bc == 'linear':
            x_no_2nd_deriv = np.append(x_no_2nd_deriv, (x_bc, x_bc + i - 0.5))
        elif bc is not None:
            deriv_constraints.append((bc[0], x_bc, bc[1], bc[2]))

    # Set derivative constraints
    for order, xx, relation, yy in zip(
            [1] + 3 * [2],
            [x_no_deriv, x_no_2nd_deriv, x_neg_2nd_deriv, x_pos_2nd_deriv],
            2 * ['='] + ['<', '>'],
            4 * [0]):
        if xx.size > 0:
            deriv_constraints.append((order, xx, relation, yy))

    # Compute spline without constraints
    t, c, k = splrep(x, y, w=w, s=s, t=t, **splrep_kw)

    # Force spline to satisfy constraints and/or minimize orthogonal distance
    if (
            mono is not None or (not squared_distance) or odr
            or optimize_knots or len(deriv_constraints) > 0):
        constraints = []

        # Add derivative constraints
        for constraint in deriv_constraints:
            def constraint_function(tc):
                t_, c = split_tc(tc, x, k) if optimize_knots else (t, tc)
                return (
                    (-1 if constraint[2] == '<' else 1) *
                    (
                        splev(constraint[1], (t_, c, k), der=constraint[0])
                        - constraint[3]))
            constraints.append(dict(
                type={'=': 'eq', '>': 'ineq', '<': 'ineq'}[constraint[2]],
                fun=constraint_function))

        # TODO: make monotonicity work with k!=3
        # Add monotonicity constraints
        if mono is not None:
            def constraint_function(tc):
                t_, c = split_tc(tc, x, k) if optimize_knots else (t, tc)
                x_extrema = np.sort(quadratic_spline_roots(
                    UnivariateSpline._from_tck((t_, c, k)).derivative(),
                    x_begin=x_begin, x_end=x_end))
                x_extrema = np.concatenate([
                    [x_begin],
                    x_extrema[(x_extrema > x_begin) & (x_extrema < x_end)],
                    [x_end]])
                diff = np.diff(splev(x_extrema, (t_, c, k)))
                mask = diff > 0 if mono in _mono_options_dec else diff < 0
                return np.sum(diff[mask])
            constraints.append(dict(type='eq', fun=constraint_function))

        # Optimize knots and coefficients
        if optimize_knots:

            # TODO: enforce Schoenberg-Whitney conditions
            # Ensure knots are ordered and have a minimum separation
            l = k + 1
            n_t = t.size - 2 * l
            if n_t > 1:
                def constraint_function(tc):
                    return np.nextafter(
                        (
                            np.diff(np.concatenate((x[:1], tc[:n_t], x[-1:])))
                            - min_knot_sep),
                        -np.inf)
                constraints.append(dict(type='ineq', fun=constraint_function))

            # TODO: add other distance functions (e.g. odr)
            # Define distance function and arguments
            func = total_y_squared_distance_tc
            args = (x, y, k, w)

            # Find optimal spline
            tc = np.concatenate((t[l:-l], c))
            res = minimize(
                func, tc, args, constraints=constraints, **minimize_kw)
            tc = res.x
            t, c = split_tc(tc, x, k)

        # Optimize only coefficients
        else:

            # Define distance function and arguments
            if odr:
                func = (
                    total_squared_distance if squared_distance
                    else total_distance)
                args = (x, y, t, k, w, wx, wy)
            else:
                func = (
                    total_y_squared_distance if squared_distance
                    else total_y_distance)
                args = (x, y, t, k, w)

            # Find optimal spline
            res = minimize(
                func, c, args, constraints=constraints, **minimize_kw)
            c = res.x

        # Print warning if optimal spline is not found
        if not res.success:
            warnings.warn(
                "Optimal spline not found - {}\n{}".format(
                    res.message, res))

    # Construct and return spline
    return UnivariateSpline._from_tck((t, c, k), ext=ext)
