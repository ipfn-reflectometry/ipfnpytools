"""
Display an image with a color bar.
"""

import warnings
import numpy as np
import matplotlib.pyplot as plt
from .add_colorbar import add_colorbar
from .default_color_normalization import default_color_normalization


def default_half_pixel_size(a, s):
    #TODO: doctstring
    """Compute the default size of half a pixel in a given direction"""

    if a.size == 1:
        warnings.warn(
            (
                "Assuming pixel size of 1 in {} direction because {} has " + 
                "only 1 element").format(s, s),
            stacklevel=3)
        return 0.5
    else:
        return (a[-1] - a[0]) / (a.size - 1) / 2


# TODO: add options for other norms, like symlog, logit, etc. (https://matplotlib.org/stable/tutorials/colors/colormapnorms.html)
# TODO: add data keyword
# TODO: make x and y arguments optional
# TODO: check if data is equally spaced in each direction (see spec.specgram)
# TODO: test with RGB(A) input
def image_show(
        x, y, c, alpha=None, axes=None, log=False, clip_low=0, clip_high=0,
        portion=0.15, xmin=None, xmax=None, ymin=None, ymax=None, cmap=None, 
        norm=None, vmin=None, vmax=None, x_relevant_min=None, 
        x_relevant_max=None, y_relevant_min=None, y_relevant_max=None, 
        normalize_all=False, normalize_along_x=False, normalize_along_y=False, 
        color_bar=True, color_bar_label=None, add_colorbar_kw=None, 
        **imshow_kw):
    # TODO: examples
    """Display an image with an optional color bar.

    This is basically the matplotlib.axes.Axes.imshow with added features and
    automation.

    The input 'c' may either be actual RGB(A) data, or 2D scalar data, which 
    will be rendered as a pseudocolor image. For displaying a grayscale image 
    set up the colormapping using the parameters 
    ``cmap='gray', vmin=0, vmax=255``.

    The number of pixels used to render an image is set by the Axes size and 
    the *dpi* of the figure. This can lead to aliasing artifacts when the image 
    is resampled because the displayed image size will usually not match the 
    size of *c* (see 
    https://matplotlib.org/stable/gallery/images_contours_and_fields/image_antialiasing.html).
    The resampling can be controlled via the *interpolation* parameter and/or 
    matplotlib.rcParams["image.interpolation"].
    
    Note: Use only for equally spaced data in each direction (x and y). For
    arbitrarily spaced data use ipfnpytools.plot_color_mesh.plot_color_mesh 
    instead.

    Parameters
    ----------
    x, y : 1-D float array
        x / y coordinates of the pixel centers (used to determine the 'extent'
        for the imshow() call - only length, first and last values are used).
    c : array_like
        The image data. Supported array shapes are:
        - (M, N): an image with scalar data. The values are mapped to
            colors using normalization and a colormap. See parameters *norm*,
            *cmap*, *vmin*, *vmax*.
        - (M, N, 3): an image with RGB values (0-1 float or 0-255 int).
        - (M, N, 4): an image with RGBA values (0-1 float or 0-255 int),
            i.e. including transparency.
        The first two dimensions (M, N) define the rows and columns of
        the image.
        Out-of-range RGB(A) values are clipped.
    alpha : float or array-like, optional
        The alpha blending value, between 0 (transparent) and 1 (opaque).
        If *alpha* is an array, the alpha blending values are applied pixel
        by pixel, and *alpha* must have the same shape as *c*.
    axes : matplotlib.axes.Axes instance, optional
        An Axes instance where the image is displayed. If None, defaults to 
        matplotlib.pyplot.gca().
    log : bool, optional
        If True, use a logarithmic color scale. Defaults to False.
    clip_low, clip_high : float, optional
        Fraction of low / high values to clip, assuming 'portion'==0. Actual 
        clipped fraction is lower if 'portion'>0. Defaults to 0. (currently 
        only implemented for "linear" and "log" 'norm')
    portion : float, optional
        Extra portion of the color scale that is not clipped. Reduces the
        effect of 'clip_low' and 'clip_high'. Defaults to 0.15. (currently only 
        implemented for "linear" and "log" 'norm')
    xmin, xmax, ymin, ymax : float, optional
        minimum / maximum  x / y coordinates to display.
    x_relevant_min, x_relevant_max, y_relevant_min, y_relevant_max : float, optional
        minimum / maximum  x / y coordinates used to determine the default 
        color normalization. If None, defaults to xmin / xmax / ymin / ymax.
    cmap : str or matplotlib.colors.Colormap instance, optional
        The Colormap instance or registered colormap name used to map scalar 
        data to colors. If None, defaults to plt.get_cmap().
    norm : str or matplotlib.colors.Normalize instance, optional
        The normalization method used to scale scalar data to the [0, 1] range 
        before mapping to colors using 'cmap'.
        If given, this can be one of the following:
        - An instance of matplotlib.colors.Normalize or one of its subclasses 
        (see https://matplotlib.org/stable/tutorials/colors/colormapnorms.html)
        - A scale name, i.e. one of "linear", "log", "symlog", "logit", etc. 
        For a list of available scales, call 
        matplotlib.scale.get_scale_names(). In that case, a suitable 
        matplotlib.colors.Normalize subclass is dynamically generated and 
        instantiated.
        Overrides 'log'. If None, defaults to "linear" if log is False and to 
        "log" if 'log' is True.
    vmin, vmax : float, optional
        When using scalar data and no explicit 'norm', vmin and vmax define the
        minimum / maximum 'c' values to display. It is an error to use 
        vmin / vmax when a 'norm' instance is given (but using an str 'norm' 
        name together with vmin / vmax is fine). If None, defaults to a range 
        computed based on 'clip_low', 'clip_high', and 'portion' if 'norm' is 
        "linear" or "log", and otherwise to covering the full value range of 
        'c'.
    normalize_all: bool, optional
        If True, normalize 'c' to its maximum value. Defaults to True. 
        (currently only implemented for scalar data)
    normalize_along_x, normalize_along_y: bool, optional
        If True, normalize 'c' to its maximum values along the x / y direction
        (after centering). Defaults to False. (currently only implemented for 
        scalar data)
    color_bar : bool, optional
        If True, draw color bar. Defaults to True.
    color_bar_label : str, optional
        Color bar label. If None, defaults to no label.
    add_colorbar_kw : dict, optional
        Dict with keywords passed to the ipfnpytools.add_colorbar() call used 
        to create the color bar. If None, defaults to {}.
    **imshow_kw
        All additional keyword arguments are passed to the 
        matplotlib.axes.Axes.imshow() call.

    Returns
    -------
    matplotlib.image.AxesImage instance
        Image object.

    Examples
    --------

    """

    # View inputs as numpy arrays of appropriate dimensions
    x, y = np.atleast_1d(x, y)
    c = np.atleast_2d(c)

    # Get default axes and define other optional keywords
    if axes is None:
        axes = plt.gca()
    if norm is None:
        norm = "log" if log else "linear"
    if add_colorbar_kw is None:
        add_colorbar_kw = {}

    # Define default visualization range
    dx = default_half_pixel_size(x, 'x')
    dy = default_half_pixel_size(y, 'y')
    if xmin is None:
        xmin = x[0] - dx
    if xmax is None:
        xmax = x[-1] + dx
    if ymin is None:
        ymin = y[0] - dy
    if ymax is None:
        ymax = y[-1] + dy
    if x_relevant_min is None:
        x_relevant_min = xmin
    if x_relevant_max is None:
        x_relevant_max = xmax
    if y_relevant_min is None:
        y_relevant_min = ymin
    if y_relevant_max is None:
        y_relevant_max = ymax

    # Check input shapes
    if c.shape[:2] != (y.size, x.size):
        raise TypeError(
            (
                'Dimensions of c {} are incompatible with x ({}) and/or ' + 
                'y ({}).').format(c.shape, x.size, y.size))

    # Normalize data if desired
    #TODO: make it work with c.ndim > 2? (RGB and RGBA values)
    if c.ndim == 2:
        mask_relevant = np.logical_and(*np.broadcast_arrays(
            ((y >= y_relevant_min) & (y <= y_relevant_max))[:,np.newaxis],
            (x >= x_relevant_min) & (x <= x_relevant_max)))
        if normalize_all:
            c /= np.nanmax(c[mask_relevant])
        #TODO: use masked array in more places?
        c_masked = np.ma.array(c, mask=~mask_relevant)
        if normalize_along_x:
            c /= np.nanmax(c_masked, axis=1)[:, np.newaxis]
        if normalize_along_y:
            c /= np.nanmax(c_masked, axis=0)
    
        # Define default color normalization range
        if norm in ["linear", "log"]:
            vmin, vmax = default_color_normalization(
                c[mask_relevant], clip_low, clip_high, portion, norm, vmin, 
                vmax)

    # Display image
    default_imshow_kw = dict(
        norm=norm, cmap=cmap, alpha=alpha, vmin=vmin, vmax=vmax, aspect="auto", 
        interpolation="none", 
        extent=(x[0] - dx, x[-1] + dx, y[0] - dy, y[-1] + dy), origin="lower")
    default_imshow_kw.update(imshow_kw)
    img = axes.imshow(c, **default_imshow_kw)
    axes.set_xlim(xmin, xmax)
    axes.set_ylim(ymin, ymax)

    # Draw color bar
    if color_bar:
        default_add_colorbar_kw = dict(
            mappable=img, ax=axes, label=color_bar_label)
        default_add_colorbar_kw.update(add_colorbar_kw)
        add_colorbar(**default_add_colorbar_kw)
    return img
