"""
Compute poloidal flux radius (rho_pol) corresponding to given time (t),
major radius (R) and height (z).
"""


import numpy as np
import aug_sfutils as sf
from scipy.interpolate import interpn

def trz_to_rhop(*args, **kwargs):
    return fast_trz_to_rhop(*args, **kwargs)

def fast_trz_to_rhop(time, r, z, shot=None, equilibrium='EQH', eq_shotfile=None):
    """Convert time, rho poloidal, and z coordinates to R.
    
    Parameters
    ----------
    time: float, ndarray
        Time instants. 
    r: float, ndarray
        Major radius (R) coordinates.
    z: float, ndarray
        Height (z) coordinate.
    shot: int
        Shot number.
    equilibrium: str, optional
        Equilibrium shot file.
        
    Returns
    -------
    rho_trz: ndarray
        Rho poloidal coordinates.
        
    """
    
    # Check what equilibrium parameters the user provided --------------------------
    if (eq_shotfile is None) == (shot is None):
        raise ValueError("User must provide `shot` or `eq_shotfile`, but not both")
        
    if eq_shotfile is None:
        eq = sf.EQU(shot, diag=equilibrium)
    else:
        eq = eq_shotfile
            
            
    try:
        [time, r, z] = np.broadcast_arrays(time, r, z)
    except ValueError:
        raise ValueError("time, r, and z must be scalars or have equal shapes")
    shape = time.shape
    
   
    
    rho_trz = interpn(
        points=(
            np.array(eq.Rmesh, dtype=np.float32), 
            np.array(eq.Zmesh, dtype=np.float32),
            np.array(eq.time, dtype=np.float32),
        ), 
        values=np.array(np.sqrt((eq.pfm - eq.psi0)/(eq.psix-eq.psi0)), dtype=np.float32), 
        xi=np.array([r.flatten(), z.flatten(), time.flatten()], dtype=np.float32).T,
        bounds_error=False,
        fill_value=np.nan,
    )
    
    return rho_trz[0] if shape == (1,) else rho_trz.reshape(shape)
