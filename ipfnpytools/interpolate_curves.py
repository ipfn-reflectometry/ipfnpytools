"""
Interpolate a set of curves along their path in the xy plane.
"""

from __future__ import division
from builtins import zip
from itertools import count
import numpy as np
from scipy.interpolate import splprep, splev


def total_span(iterable):
    #TODO: examples
    """Compute the difference between the maximum and the minimum value in a
    list of arrays.

    Parameters
    ----------
    iterable : list
        List of arrays.

    Returns
    -------
    float
        Difference between the maximum and the minimum value of all the arrays
        in 'iterable'.

    """
    return (
        max([np.amax(x) for x in iterable]) -
        min([np.amin(x) for x in iterable]))


#TODO: make it work with a list of arrays
#TODO: include options of interpolating with given points or fixed spacing in y or z
#TODO: allow monotonic interpolation (PchipInterpolator)
def interpolate_curves(
        ay, az, n=None, max_degree=3, check_finite=True, normalize_span=True,
        **splprep_kw):
    #TODO: docstring
    """Interpolate a set of curves along their path in the yz plane."""

    # Remove NaNs and infinite values if desired
    if check_finite:
        y_good = []
        z_good = []
        for y, z in zip(*np.broadcast_arrays(ay, az)):
            good_indices = np.logical_and(np.isfinite(y), np.isfinite(z))
            y_good.append(y[good_indices])
            z_good.append(z[good_indices])
    else:
        y_good = ay
        z_good = az

    # Define number of interpolation points
    if n is None:
        n = int(round(np.mean([len(y) for y in y_good])))

    # Define normalization of x and y dimensions
    if normalize_span:
        dy = total_span(y_good)
        dz = total_span(z_good)
    else:
        dy = 1
        dz = 1

    # Interpolate each curve to the desired number of points
    default_splprep_kw = dict(s=0)
    default_splprep_kw.update(**splprep_kw)
    y_interp = np.empty((np.broadcast(ay, az).shape[0], n), dtype=ay.dtype)
    z_interp = np.empty_like(y_interp)
    auto_choose_k = 'k' not in default_splprep_kw
    for index, y, z in zip(count(), y_good, z_good):
        if auto_choose_k:
            default_splprep_kw['k'] = min(len(y) - 1, max_degree)
        tck, u = splprep([y / dy, z / dz], **default_splprep_kw)
        #TODO: instead of equally spaced u, use the average u spacing of the input curves
        u_new = np.linspace(0, 1, n)
        y_new, z_new = splev(u_new, tck)
        y_interp[index] = y_new * dy
        z_interp[index] = z_new * dz
    return y_interp, z_interp
