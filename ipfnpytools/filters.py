#TODO: docstring
"""
Filters with an implementation that is robust to numerical errors.
"""

from __future__ import division
import numpy as np
from scipy import signal, interpolate


def no_filter(n, output):
    # TODO: docstring
    """

    Parameters
    ----------
    n
    output

    Returns
    -------

    """

    # TODO: add returns for "ba" and "zpk" outputs
    if output == "sos":
        sos = np.zeros((-(-n // 2), 6))
        sos[:, (0, 3)] = 1
        return sos


def iir_filter(fc, n=6, btype=None, fs=1, output='sos', **iirfilter_kw):
    # TODO: docstring
    """

    Parameters
    ----------
    fc
    n
    btype
    fs
    iirfilter_kw

    Returns
    -------

    """

    # Set default critical frequencies and normalize to the Nyquist frequency
    fnyq = fs / 2
    fc = np.atleast_1d(fc)
    indices = np.equal(fc, None)
    fc[indices] = np.array([0, fnyq])[indices]
    fc = fc / fnyq

    # Define default type of filter
    if btype is None:
        if len(fc) == 1 or fc[0] <= 0:
            btype = "lowpass"
        elif fc[-1] >= 1:
            btype = "highpass"
        else:
            btype = "bandpass"

    # Adjust critical frequency(ies) to the type of filter and compute filter
    if btype == "lowpass":
        fc = fc[-1]
        if fc >= 1:
            return no_filter(n, output)
    elif btype == "highpass":
        fc = fc[0]
        if fc <= 0:
            return no_filter(n, output)
    elif btype == "bandpass":
        if fc[0] <= 0 and fc[-1] >= 1:
            return no_filter(n, output)
    default_iirfilter_kw = dict(N=n, Wn=fc, btype=btype, output=output)
    default_iirfilter_kw.update(iirfilter_kw)
    return signal.iirfilter(**default_iirfilter_kw)


def butter_lowpass_filter(data, cutoff, fs, half_order=5, axis=-1, handle_nans=False, keep_nans=True):
    """This function applies a linear digital filter twice, once forward and once backwards. 
    The combined filter has zero phase and a filter order twice that of *half_order*.
    
    Parameters
    ----------
    data: ndarray
        Array of data to filter
    cutoff: float
        Cutoff frequency in Hz.
    fs: float
        Sampling frequency in Hz.
    half_order: int
        Half of the filter order.
    axis: int, optional
        The axis of x to which the filter is applied. Default is -1.
    handle_nans: bool, optional
        If True, NaN values are inter- and/or extrapolated before filtering. Defaults to False.
    keep_nans: bool, optional
        If True, values in the output are returned to NaN in the same locations as *data*. 
        
    Returns
    -------
    y: ndarray
        Filtered array
        
    Note: If *data* contains NaNs and *handle_nans*=False, the returned array will be all-NaNs.
    """
    
    y = np.array(data)
    
    if handle_nans:
        nan_mask = np.isnan(data)       
        if nan_mask.any():
            time = np.arange(len(data))
            y[nan_mask] = interpolate.interp1d(
                time[~nan_mask], data[~nan_mask], 
                assume_sorted=True, bounds_error=False, fill_value='extrapolate',
            )(time[nan_mask])
            
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    sos = signal.butter(half_order, normal_cutoff, btype='low', output='sos')
    y = signal.sosfiltfilt(sos, y, axis)
    
    if handle_nans and keep_nans:
        y[nan_mask] = np.nan
        
    return y


def butter_highpass_filter(data, cutoff, fs, half_order=5, axis=-1, handle_nans=False, keep_nans=True):
    """This function applies a linear digital filter twice, once forward and once backwards. 
    The combined filter has zero phase and a filter order twice that of *half_order*.
    
    Parameters
    ----------
    data: ndarray
        Array of data to filter
    cutoff: float
        Cutoff frequency in Hz.
    fs: float
        Sampling frequency in Hz.
    half_order: int
        Half of the filter order.
    axis: int, optional
        The axis of x to which the filter is applied. Default is -1.
    handle_nans: bool, optional
        If True, NaN values are inter- and/or extrapolated before filtering. Defaults to False.
    keep_nans: bool, optional
        If True, values in the output are returned to NaN in the same locations as *data*. 
        
    Returns
    -------
    y: ndarray
        Filtered array
        
    Note: If *data* contains NaNs and *handle_nans*=False, the returned array will be all-NaNs.
    """
    
    y = np.array(data)
    
    if handle_nans:
        nan_mask = np.isnan(data)       
        if nan_mask.any():
            time = np.arange(len(data))
            y[nan_mask] = interpolate.interp1d(
                time[~nan_mask], data[~nan_mask], 
                assume_sorted=True, bounds_error=False, fill_value='extrapolate',
            )(time[nan_mask])
            
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    sos = signal.butter(half_order, normal_cutoff, btype='high', output='sos')
    y = signal.sosfiltfilt(sos, y, axis)
    
    if handle_nans and keep_nans:
        y[nan_mask] = np.nan
        
    return y

def moving_average(x, N, mode):
    
    return np.convolve(x, np.ones(N)/N, mode=mode)
