import numpy as np


def all_equal(x, **all_kw):
    # TODO: docstring
    """Check if all elements of an array are equal."""

    x = np.atleast_1d(x)
    return np.all(x == x[0], **all_kw)
