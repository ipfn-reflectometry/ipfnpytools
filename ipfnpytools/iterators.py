"""
Useful iterators.
"""

from builtins import zip
from builtins import map
from builtins import range
from itertools import count
from functools import partial


def chunks(sequence, n):
    #TODO: docstring
    """Collect data into fixed-length chunks or blocks.
        chunks('ABCDEFG', 3) --> 'ABC' 'DEF'"""

    for i in range(0, len(sequence) + 1 - n, n):
        yield sequence[i:i + n]


def izipchunks(*args, **kw):
    # TODO: docstring
    """Collect, count and zip data into fixed-length chunks.
        izipchunks('ABCDE', [5,6,7,8,9], n=2)
        --> (0, 'AB', [5, 6]) (1, 'CD', [7, 8])"""

    return zip(count(), *map(partial(chunks, n=kw.get('n', 1)), args))
