"""
Compute theta poloidal angle for a given (time, radius, height) triplet.
"""

import numpy as np
import aug_sfutils as sf
from scipy.interpolate import interp1d
from .getsig import getsig


def trz_to_theta(time, r, z, shot, equilibrium='EQH'):
    """Convert time, radius (r), and height (z) coordinates to poloidal angle (theta).
    
    Parameters
    ----------
    time: float, ndarray
        Time instants. 
    r: float, ndarray
        Major radius (R) coordinates.
    z: float, ndarray
        Height (z) coordinate.
    shot: int
        Shot number.
    equilibrium: str, optional
        Equilibrium shot file. Defaults to the 'EQH' family 
        
    Returns
    -------
    theta: ndarray
        Theta poloidal coordinates.
    """
    
            
    # Validate user input ----------------------------------------------------------
            
    try:
        [time, r, z] = np.broadcast_arrays(time, r, z)
    except ValueError:
        raise ValueError("time, r, and z must be scalars or have equal shapes")
    shape = time.shape
    
    
    
    # Get the magnetic axis from the provided shotfile -----------------------------
    shotfile = {'IDE': 'IDG', 'EQH': 'GQH'}[equilibrium]
    
    t_mag = getsig(shot, shotfile, 'TIMEF')
    r_mag = getsig(shot, shotfile, 'Rmag')
    z_mag = getsig(shot, shotfile, 'Zmag')
    
    
    
    r_mag_t = np.interp(
        x=time,
        xp=t_mag,
        fp=r_mag,
        left=np.nan,
        right=np.nan,
    )
    
    z_mag_t = np.interp(
        x=time,
        xp=t_mag,
        fp=z_mag,
        left=np.nan,
        right=np.nan,
    )
    
    
    theta = np.arccos((r - r_mag_t) / ((r - r_mag_t)**2 + (z - z_mag_t)**2)**0.5)
    
    return theta[0] if shape == (1,) else theta

