from .getsig import getsig
from .filters import butter_lowpass_filter
import numpy as np
from scipy.signal import find_peaks
from scipy.interpolate import interp1d


def get_elms(shot: int = None, cutoff: float = 1.0e3, prominence: float = 1600, min_width: float = 2e-4, max_width: float = 5e-3, rel_height: float = 0.8, lead: float = 0, tail: float = 0, elm_signal: np.array = None, wlen=None):
    """Get ELMs info for a given shot. ELM detection uses scipy.find_peaks after a low-pass filter on the outer divertor current data.
    
    Parameters
    ----------
    shot: int
        Shot number
    cutoff:
        Frequency cutoff for the low-pass filter applied to the divertor current data (in Hz). Optinal, default value is 1kHz.
    prominence: float, optional
        Required prominence of divertor current peaks (in Ampere). Optional, default value is 1.6kA.
    min_width: float, optional
        Minimum ELM width in seconds. Optional, defaults to 200us.
    max_width: float, optional
        Maximum ELM width in seconds. Optional, defaults to 10ms.
    rel_height: float, optional
        Relative height to compute the width. Optional, defaults to 0.8 or 80%.
    lead: float, optional
        If provided, will be subtracted from the left base of each ELM peak. Giving a time buffer before the ELM peak starts. Defaults to zero.
    tail: float, optional
        If provided, will be added to the right base of each ELM peak. Giving a time buffer after the ELM peak ends. Defaults to zero.
    elm_signal: ndarray, optional
        Provide a custom ELM signal for detection. Array of shape (2, N,). The first row is assumed to be the time-axis for the ELM signal, sorted. The second row is assumed to be the signal intensity.
        
        

    
        
    Returns
    -------
    peaks : ndarray
        Time instances that correspond to the ELM peak
    properties : dict
        A dictionary containing properties of the returned peaks.
        * 'prominences'
              Height difference between the peak and its base.
        * 'left', 'right'
              Left and right edges of each peak.
    """
    
    # Get signal for ELM detection - divertor shunt current --------------------------------------------------
    if (elm_signal is None) == (shot is None):
        raise ValueError("Provide either *shot* or *elm_signal* but not both.")
    
    if elm_signal is None:
        data =  - getsig(shot, 'MAC', 'Ipolsola')
        time = getsig(shot, 'MAC', 'TLab')
    else:
        data = elm_signal[1]
        time = elm_signal[0]
    
    # Low-pass zero-phase filter ------------------------------------------------------------------------------
    fs = 1 / (time[1] - time[0])
    custom_filter = lambda x: butter_lowpass_filter(x, cutoff=cutoff, fs=fs)
    
    # Peak detection using scipy.find_peaks ------------------------------------------------------------------
#     prominence = 1600  # Minimum height difference between the peak and its base in Ampere (1.6kA)
    
#     min_width = 2e-4  # Minimum ELM width in seconds (200us)
#     max_width = 1e-2  # Maximum ELM width in seconds (10ms)
    
#     rel_height = 0.8  # Relative height to compute the width
    
    
    peaks, properties = find_peaks(
        custom_filter(data), 
        width=(int(min_width*fs), int(max_width*fs)), 
        rel_height=rel_height, prominence=prominence,
        wlen=wlen,
    )
    
    # Convert from samples to time  -----------------------------------------------------------------
    time_interpolator = interp1d(np.arange(len(time)), time)
    left = time_interpolator(properties['left_ips'])
    right = time_interpolator(properties['right_ips'])
    
    
    return time[peaks], {'prominences': properties['prominences'], 'left': left - lead, 'right': right + tail}


def create_elm_mask(time: np.ndarray, **kwargs) -> np.ndarray:
    """Create a mask that is True for values of `time` inside an ELM crash
    
    Parameters
    ----------
    time: ndarray
        Array of time values
    kwargs: dictionary
        Keyword arguments passed to ipfnpytools.elms.get_elms()
        
    Returns
    -------
    elm_mask: ndarray
        Bool array with same shape as `time`. True for values of `time` inside an ELM crash
    """
    
    _, properties = get_elms(**kwargs)
    
    left = properties['left']
    right = properties['right']
    
#     print(time.shape)
#     print(left.shape)
#     print(right.shape)
    
#     print("Last value", right[-1])
#     print("Last time values", time[[-2, -1]]) 
    
    # Assuming left, right, and time are already defined and sorted numpy arrays
    left_indices = np.searchsorted(time, left, side='left')
    right_indices = np.searchsorted(time, right, side='right')
    
#     print("Last indices", right_indices[[-2, -1]])

    # Create an array of zeros with the same length as `time`
    count_changes = np.zeros(len(time), dtype=int)

    # Add 1 at positions corresponding to the left_indices
    np.add.at(count_changes, left_indices[left_indices < len(count_changes)], 1)

    # Subtract 1 at positions corresponding to the right_indices
    np.subtract.at(count_changes, right_indices[right_indices < len(count_changes)], 1)

    # Calculate the cumulative sum and convert to boolean
    mask = np.cumsum(count_changes).astype(bool)
    
#     return np.sum((time >= properties['left'][:, None]) & (time <= properties['right'][:, None]), axis=0).astype(bool)
    return mask