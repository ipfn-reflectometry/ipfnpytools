#TODO: docstring
"""
Functions to make different types of plots with good quality in a convenient
way.

Examples
--------

"""

import sys
import os
import warnings
from itertools import count
from future.moves.itertools import zip_longest
import numpy as np
import matplotlib as mpl
import matplotlib.patheffects as patheffects
import matplotlib.pyplot as plt
from scipy.stats import gmean#, hmean
from .timing import timing
from . import arguments as args
from . import si_units
from .subplotting import subplotting
from .find_first import find_first
from .find_last import find_last
from .spec import specgram, coherogram
from .image_show import image_show
from .profilogram import profilogram
from .has_even_sampling import has_even_sampling
from .all_equal import all_equal
from .default_color_normalization import default_color_normalization
from .add_colorbar import add_colorbar
from .plot_color_mesh import plot_color_mesh
from .save_figure import save_figure
from .color_maps import cyclic_bmryb_cmap, average_brightness


def use_base_style(
        directory=None, style="base", new="base_new", old="base_old",
        old_mpl_version=1):
    # TODO: docstring
    """Use matplotlib style sheets for good looking plots."""

    if directory is None:
        directory = os.path.dirname(__file__)
    ext = ".mplstyle"
    plt.style.use(os.path.join(directory, style + ext))
    if int(mpl.__version__[0]) > old_mpl_version:
        plt.style.use(os.path.join(directory, new + ext))
    else:
        plt.style.use(os.path.join(directory, old + ext))


def calc_smart_linewidth(x, y, ymin=None, ymax=None, p_width=None):
    #TODO: doctstring
    """Define linewidth based on the data variation."""

    # Define default keywords
    if ymin is None:
        ymin = min(y)
    if ymax is None:
        ymax = max(y)
    if p_width is None:
        p_width = (80, 0.8, 20, 0.44)

    # Compute linewidth
    y = y.clip(ymin, ymax)
    y_diff = np.abs(np.diff(y))
    y_diff_upper = 0 if y_diff.size == 0 else np.percentile(y_diff, p_width[0])
    linewidth_factor = 1 if y_diff_upper == 0 else min(
        1,
        (
            p_width[1] * max(ymax - ymin, p_width[2] * y_diff_upper) *
            len(x) ** p_width[3] / y_diff.sum()))
    return plt.rcParams['lines.linewidth'] * linewidth_factor


# TODO: add features of IDL 'lg_plot_signals'
# TODO: refactor code in common with plot_errorbars and plot_scatter
def plot_traces(
        x, y, axes=None, labels=None, labels_real=None, labels_imag=None,
        suffix_real='real', suffix_imag='imag', xmin=None, xmax=None,
        ymin=None, ymax=None, x_relevant_min=None, x_relevant_max=None,
        p_width=None, all_x=False, smart_linewidth=True, same_color=False,
        **plot_kw):
    #TODO: docstring
    """Plot y versus x in a given range with smart linewidth."""

    # View inputs as numpy arrays of appropriate dimensions
    #TODO: broadcast arrays similarly to plot_scatter to be more consistent and allow a wider range of input dimensions
    x, y, labels, labels_real, labels_imag = np.atleast_1d(
        x, y, labels, labels_real, labels_imag)

    # Define default keywords
    if axes is None:
        axes = plt.gca()
    if xmin is None:
        xmin = x.min()
    if xmax is None:
        xmax = x.max()
    if x_relevant_min is None:
        x_relevant_min = xmin
    if x_relevant_max is None:
        x_relevant_max = xmax

    # Determine visible portion of the data
    # TODO: add points just before and after range to mask even if no points are in range
    # TODO: use x range to define linewidth correctly even when all_x==True
    if not all_x:
        mask = np.logical_and(x >= xmin, x <= xmax)
        if mask.ndim > 1:
            mask = mask.any(axis=-1)
        first_index = find_first(mask)
        if first_index.size > 0:
            mask[max(first_index - 1, 0)] = True
            mask[min(find_last(mask) + 1, mask.size - 1)] = True
        x = x[mask]
        y = y[mask]

    # Determine relevant portion of the data
    mask = np.logical_and(x >= x_relevant_min, x <= x_relevant_max)
    if mask.ndim > 1:
        mask = mask.any(axis=-1)
    first_index = find_first(mask)
    if first_index.size > 0:
        mask[max(first_index - 1, 0)] = True
        mask[min(find_last(mask) + 1, mask.size - 1)] = True
    y_relevant = y[mask]

    # Define default y range
    if ymin is None:
        ymin = (
            np.nanmin((np.nanmin(y_relevant.real), np.nanmin(y_relevant.imag)))
            if np.iscomplexobj(y_relevant) else np.nanmin(y_relevant))
    if ymax is None:
        ymax = (
            np.nanmax((np.nanmax(y_relevant.real), np.nanmax(y_relevant.imag)))
            if np.iscomplexobj(y_relevant) else np.nanmax(y_relevant))

    # Define default linewidth based on the data variation
    default_plot_kw = {}
    if smart_linewidth:
        default_plot_kw['linewidth'] = calc_smart_linewidth(
            x, y, ymin=ymin, ymax=ymax, p_width=p_width)

    # Use the same color for all lines if desired
    if same_color:
        default_plot_kw['color'] = next(axes._get_lines.prop_cycler)['color']

    # Plot trace (real and imag if data is complex)
    default_plot_kw.update(plot_kw)
    if np.iscomplexobj(y):
        lines_real = axes.plot(x, y.real, **default_plot_kw)
        if same_color:
            default_plot_kw['color'] = next(
                axes._get_lines.prop_cycler)['color']
            default_plot_kw.update(plot_kw)
        lines_imag = axes.plot(x, y.imag, **default_plot_kw)
        for i, (label_real, label_imag, label) in zip(count(), zip_longest(
                labels_real, labels_imag, labels)):
            if label_real is None:
                label_real = label
            if label_real is None:
                label_real = suffix_real
            elif suffix_real is not None and suffix_real != '':
                label_real += ' ({})'.format(suffix_real)
            if label_imag is None:
                label_imag = label
            if label_imag is None:
                label_imag = suffix_imag
            elif suffix_imag is not None and suffix_imag != '':
                label_imag += ' ({})'.format(suffix_imag)
            lines_real[i].set_label(label_real)
            lines_imag[i].set_label(label_imag)
        return lines_real + lines_imag, ymin, ymax
    else:
        lines = axes.plot(x, y, **default_plot_kw)
        for line, label in zip(lines, labels):
            line.set_label(label)
        return lines, ymin, ymax


def ga_mean(a, axis=None):
    # TODO: docstring
    """Compute the arithmetic mean of the geometric and arithmetic means."""
    return np.mean((gmean(a, axis=axis), np.mean(a, axis=axis)))


#TODO: adjust markersize after the plot to avoid guessing ymin and ymax
def calc_smart_markersize(
        x, y, xerr, yerr, xmax, xmin, ymax, ymin, axes, base_markersize=None,
        check_finite=True):
    #TODO: doctstring
    """Define markersize based on the data spacing and errorbars."""

    # Define default keywords
    if base_markersize is None:
        base_markersize = plt.rcParams['lines.markersize']

    # Determine visible points
    mask_visible = (x >= xmin) & (x <= xmax)
    if check_finite:
        mask_visible = mask_visible & np.isfinite(y)
    x = x[mask_visible]
    y = y[mask_visible]
    if xerr is not None and xerr.ndim > 0:
        xerr = xerr[..., mask_visible]
    if yerr is not None and yerr.ndim > 0:
        yerr = yerr[..., mask_visible]

    # Define default y range
    if ymin is None:
        if yerr is not None:
            if yerr.ndim == 2:
                y_low = y - yerr[0]
            else:
                y_low = y - yerr
        else:
            y_low = y
        ymin = np.min(y_low)
    if ymax is None:
        if yerr is not None:
            if yerr.ndim == 2:
                y_high = y + yerr[1]
            else:
                y_high = y + yerr
        else:
            y_high = y
        ymax = np.max(y_high)

    # Compute markersize
    y = y.clip(ymin, ymax)
    points_per_pixel = 72 / axes.figure._dpi
    bbox = axes.get_window_extent()
    x_factor = (bbox.x1 - bbox.x0) / (xmax - xmin)
    y_factor = (bbox.y1 - bbox.y0) / (ymax - ymin)
    x_pixels, x_min_pixels, x_max_pixels = (
        x_factor * a for a in (x, np.min(x), np.max(x)))
    y_pixels, y_min_pixels, y_max_pixels = (
        y_factor * a for a in (y, np.min(y), np.max(y)))
    delta_x = np.abs(x_pixels[1:] - x_pixels[:-1])
    delta_y = np.abs(y_pixels[1:] - y_pixels[:-1])
    x_range = x_max_pixels - x_min_pixels
    y_range = y_max_pixels - y_min_pixels
    # several averages tried in each step: hmean, gmean, ga_mean, mean
    marker_spacing = ga_mean((delta_x, delta_y))
    # marker_spacing = np.mean(np.sqrt(delta_x * delta_y)) # also tried np.sqrt(np.mean(
    marker_small_spacing = np.mean((x_range, y_range)) / delta_x.size
    # marker_small_spacing = (np.sqrt(x_range*y_range / x_visible.size) / 2)
    marker_spacing_points = (
        points_per_pixel * np.mean((marker_spacing, marker_small_spacing)))
    err_pixels = []
    if xerr is not None:
        err_pixels.append(ga_mean(np.atleast_1d(x_factor * xerr)))
    if yerr is not None:
        err_pixels.append(ga_mean(np.atleast_1d(y_factor * yerr)))
    markersize = (
        (points_per_pixel * ga_mean(err_pixels)) if len(err_pixels) > 0
        else marker_spacing_points)
    max_markersize = min(base_markersize, marker_spacing_points)
    min_markersize = min(0.5 * base_markersize, marker_spacing_points)
    return np.clip(markersize, min_markersize, max_markersize)


#TODO: allow upper and lower error
#TODO: automatic linewidth
def plot_errorbars(
        x, y, yerr=None, xerr=None, axes=None, xmin=None, xmax=None,
        ymin=None, ymax=None, x_relevant_min=None, x_relevant_max=None,
        marker=None, base_markersize=None, markersize=None, label=None,
        linestyle=None, color=None, all_x=False, smart_markersize=True,
        check_finite=True, same_color=False, **errorbar_kw):
    #TODO: docstring
    """Plot y versus x in a given range with errorbars."""

    # View inputs as numpy arrays of appropriate dimensions
    #TODO: broadcast arrays similarly to plot_scatter to be more consistent and allow a wider range of input dimensions
    x, y = np.atleast_1d(x, y)
    if x.ndim != 2:
        x = x[:, np.newaxis]
    if y.ndim != 2:
        y = y[:, np.newaxis]
    x, y = np.broadcast_arrays(x, y)
    xerr_was_given, yerr_was_given = (a is not None for a in (xerr, yerr))
    if xerr_was_given:
        xerr = np.atleast_1d(xerr)
        if xerr.ndim < 2:
            xerr = xerr[:, np.newaxis]
        xerr = np.broadcast_to(xerr, x.shape)
    if yerr_was_given:
        yerr = np.atleast_1d(yerr)
        if yerr.ndim < 2:
            yerr = yerr[:, np.newaxis]
        yerr = np.broadcast_to(yerr, y.shape)

    # Compute horizontal error bar ends
    if xerr_was_given:
        x_high = x + xerr
        x_low = x - xerr
    else:
        x_high = x_low = x

    # Define default keywords
    if axes is None:
        axes = plt.gca()
    if xmin is None:
        xmin = np.min(x_low)
    if xmax is None:
        xmax = np.max(x_high)
    if x_relevant_min is None:
        x_relevant_min = xmin
    if x_relevant_max is None:
        x_relevant_max = xmax
    if marker is None:
        marker = 'x'
    if linestyle is None:
        linestyle = ''

    # Select data
    # TODO: add points just before and after range to mask even if no points are in range
    if not all_x:
        mask = ((x_high >= xmin) & (x_low <= xmax)).any(axis=-1)
        if mask.size > 0:
            first_index = find_first(mask)
            if first_index.size > 0:
                mask[max(first_index - 1, 0)] = True
                mask[min(find_last(mask) + 1, mask.size - 1)] = True
        x, y = (a[mask] for a in (x, y))
        if xerr_was_given:
            xerr = xerr[..., mask, :]
        if yerr_was_given:
            yerr = yerr[..., mask, :]

    # Define default y range
    mask = (x >= x_relevant_min) & (x <= x_relevant_max)
    if check_finite:
        mask = mask & np.isfinite(y)
    if ymin is None:
        if mask.size > 0:
            ymin = (y[mask] - yerr[mask] if yerr_was_given else y[mask]).min()
        else:
            ymin = np.nan
    if ymax is None:
        if mask.size > 0:
            ymax = (y[mask] + yerr[mask] if yerr_was_given else y[mask]).max()
        else:
            ymax = np.nan

    # Define default markersize based on data spacing and errorbars
    #TODO: make it work well when points are not ordered (and also use the median or percentiles as averages?)
    if markersize is None and smart_markersize:
        markersize = calc_smart_markersize(
            x, y, xerr, yerr, xmax, xmin, ymax, ymin, axes,
            base_markersize=base_markersize, check_finite=check_finite)

    # Use the same color for all errorbars if desired
    if color is None and same_color:
        color = next(axes._get_lines.prop_cycler)['color']

    # Plot errorbars
    errorbars = []
    for i in range(x.shape[-1]):
        xerr_i = xerr[..., i] if xerr_was_given else None
        yerr_i = yerr[..., i] if yerr_was_given else None
        try:
            errorbars.append((
                xerr_i, yerr_i,
                axes.errorbar(
                    x[:, i], y[:, i], yerr=yerr_i, xerr=xerr_i, marker=marker,
                    markersize=markersize, label=label, linestyle=linestyle,
                    color=color, **errorbar_kw)))
        except:
            #TODO: include original traceback in the raise
            raise
        else:
            label = None
    return errorbars, ymin, ymax


# Shadow effects for labels over color plots
shadow = [patheffects.withStroke(foreground='black', alpha=0.75, linewidth=2)]
white_shadow = [patheffects.withStroke(
    foreground='white', alpha=0.75, linewidth=2)]


# Default keywords for image_show calls to plot spectrograms
default_specgram_image_show_kw = dict(
    log=True, clip_low = 0.03, clip_high = 0.025)


def specgram_c_label(c_label, units, f_units):
    #TODO: docstring
    """Define the color bar label for a spectrogram and add units to it."""

    if units is None:
        units=''
    if c_label is None:
        if units != '':
            c_label = 'P'
        else:
            c_label = 'Spectral power'
    # TODO: make ^2 work correctly with units with divisions (ex. T/s)
    units_len = len(units.split())
    if units_len == 1:
        c_label += " ({}$^2$/{})".format(units, f_units)
    elif units_len > 1:
        c_label += " (({})$^2$/{})".format(units, f_units)
    return c_label


def write_ftres_label(
        fres, tres, f_units, tres_units, axes=None, text=None, write_fres=True, 
        write_tres=True, **annotate_kw):
    #TODO: docstring
    """Write a label with time and frequency resolutions."""

    if axes is None:
        axes = plt.gca()
    # TODO: reduce space between signal legend and time and frequency resolutions (use legend()?)
    text = text if text is not None else ''
    if write_fres and np.isfinite(fres):
        if text != "":
            text += "\n"
        text += r"$\Delta f_{{3dB}}$ = {:.2g} {}".format(fres, f_units)
    if write_tres:
        if text != "":
            text += "\n"
        text += r"$\Delta t$ = {:.2g} {}".format(tres, tres_units)
    default_annotate_kw = dict(
        text=text, xy=(0, 1), xycoords="axes fraction", xytext=(3, -3),
        textcoords="offset points", ha="left", va="top", color="white",
        path_effects=shadow)
    default_annotate_kw.update(annotate_kw)
    axes.annotate(**default_annotate_kw)


# TODO: add features of IDL 'lg_plot_specgram'
def plot_specgram(
        x, t=None, axes=None, units="", f_units="", tres_units="",
        f_multiplier=1, t_multiplier=1, tres_multiplier=1, label=None,
        c_label=None, t_begin=None, t_end=None, fmin=None, fmax=None,
        t_relevant_begin=None, t_relevant_end=None, f_relevant_min=None,
        f_relevant_max=None, f_offset=0, t_offset=0, write_c_label=True,
        write_fres=True, write_tres=True, all_time=False, image_show_kw={},
        annotate_kw={}, **specgram_kw):
    #TODO: docstring
    """Plot spectrogram."""

    # Define color bar label and add units
    if write_c_label:
        c_label = specgram_c_label(c_label, units, f_units)

    # Unapply multipliers to time and frequency ranges
    if t_begin is not None:
        t_begin /= t_multiplier
    if t_end is not None:
        t_end /= t_multiplier
    if fmin is not None:
        fmin /= f_multiplier
    if fmax is not None:
        fmax /= f_multiplier

    # Compute spectrogram
    bbox = axes.get_window_extent()
    default_specgram_kw = dict(
        ntpixels=int(round(bbox.x1 - bbox.x0)),
        nfpixels=int(round(bbox.y1 - bbox.y0)), all_time=all_time)
    default_specgram_kw.update(specgram_kw)
    f, t, sxx, tres, fres, fmin, fmax, fs = specgram(
        x, t=(t + t_offset / t_multiplier), t_begin=t_begin, t_end=t_end,
        fmin=fmin, fmax=fmax, return_all=True, **default_specgram_kw)

    # Reapply multiplier to time range
    if t_begin is not None:
        t_begin *= t_multiplier
    if t_end is not None:
        t_end *= t_multiplier

    # Plot spectrogram
    default_image_show_kw = dict(
        axes=axes, xmin=t_begin, xmax=t_end, x_relevant_min=t_relevant_begin,
        x_relevant_max=t_relevant_end, ymin=(fmin * f_multiplier),
        ymax=(fmax * f_multiplier), y_relevant_min=f_relevant_min,
        y_relevant_max=f_relevant_max, color_bar_label=c_label)
    default_image_show_kw.update(default_specgram_image_show_kw)
    default_image_show_kw.update(image_show_kw)
    image_show(
        t * t_multiplier, f * f_multiplier - f_offset, sxx / f_multiplier,
        **default_image_show_kw)

    # Write legend and time and frequency resolutions
    write_ftres_label(
        fres * f_multiplier, tres * tres_multiplier, f_units, tres_units,
        axes=axes, text=label, write_fres=write_fres, write_tres=write_tres,
        **annotate_kw)


# TODO: refactor code in common with plot_scatter
# TODO: add automatic number of pixels choice
def plot_profilogram(
        x, y, c, axes=None, units="", text=None, c_label=None, xmin=None,
        xmax=None, ymin=None, ymax=None, x_relevant_min=None,
        x_relevant_max=None, y_relevant_min=None, y_relevant_max=None,
        write_c_label=True, all_x=False, transpose=True,
        check_even_sampling=True, use_image_show=None,
        plot_color_mesh_or_image_show_kw={}, plot_color_mesh_kw={},
        image_show_kw={}, annotate_kw={}, **profilogram_kw):
    #TODO: docstring
    """Plot profilogram."""

    # Compute profilogram
    x, y, c = profilogram(
        x, y, c, xmin=xmin, xmax=xmax, all_x=all_x, **profilogram_kw)
    c = np.atleast_2d(c)

    # Decide whether to use image_show or plot_color_mesh
    #TODO: use image_show even when ndim>1, if input is evenly sampled
    if use_image_show is None:
        if x.ndim <= 1 and y.ndim <= 1:
            if check_even_sampling:
                use_image_show = (
                    (x.size == 1 or has_even_sampling(x)) and 
                    (y.size == 1 or has_even_sampling(y)))
            else:
                use_image_show = True
        else:
            use_image_show = False

    # Transpose signals if desired
    if transpose:
        x, y, c = (a.T for a in (x, y, c))

    # Add units to color bar label
    if (
            write_c_label and c_label is not None and units is not None and
            units != ""):
        c_label += " ({})".format(units)

    # TODO: add antialiased=True as a default keyword?
    # Plot profilogram
    default_plot_color_mesh_or_image_show_kw = dict(
        axes=axes, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
        x_relevant_min=x_relevant_min, x_relevant_max=x_relevant_max,
        y_relevant_min=y_relevant_min, y_relevant_max=y_relevant_max,
        color_bar_label=c_label)
    default_plot_color_mesh_or_image_show_kw.update(
        plot_color_mesh_or_image_show_kw)
    if use_image_show:
        default_image_show_kw = dict()
        default_image_show_kw.update(default_plot_color_mesh_or_image_show_kw)
        default_image_show_kw.update(image_show_kw)
        image_show(x, y, c, **default_image_show_kw)
    else:
        default_plot_color_mesh_kw = dict(rasterized=True)
        default_plot_color_mesh_kw.update(
            default_plot_color_mesh_or_image_show_kw)
        default_plot_color_mesh_kw.update(plot_color_mesh_kw)
        plot_color_mesh(x, y, c, **default_plot_color_mesh_kw)

    # Write legend
    # TODO: add time and area resolutions (use legend()?)
    if text is not None:
        default_annotate_kw = dict(
            text=text, xy=(0, 1), xycoords="axes fraction", xytext=(3, -3),
            textcoords="offset points", ha="left", va="top")
        if (
                average_brightness(plot_color_mesh_kw.get(
                    'cmap', plt.get_cmap(plt.rcParams['image.cmap'])))
                < 0.5):
            default_annotate_kw.update(color="white", path_effects=shadow)
        else:
            default_annotate_kw.update(
                color="black", path_effects=white_shadow)
        default_annotate_kw.update(annotate_kw)
        axes.annotate(**default_annotate_kw)


# TODO: refactor code in common with plot_traces, plot_errorbars, plot_profilogram
# TODO: add features from image_show and plot_color_mesh
# TODO: Define default markersize based on the data and x/y ranges
# TODO: add possibility of averaging points
# TODO: add possibility of adding lines connecting points and/or errorbars (make a call to scatter and another one to plot or errorbar?)
def plot_scatter(
        x, y, c=None, label=None, axes=None, units="", c_label=None, log=False, 
        clip_low=0, clip_high=0, portion=0.15, xmin=None, xmax=None, ymin=None, 
        ymax=None, cmap=None, norm=None, vmin=None, vmax=None, 
        x_relevant_min=None, x_relevant_max=None, y_relevant_min=None, 
        y_relevant_max=None, normalize_all=False, normalize_along_x=False, 
        normalize_along_y=False, color_bar=None, write_c_label=True, 
        all_x=False, all_y=False, transpose=False, n_threshold_rasterized=100, 
        rasterized=None, add_colorbar_kw=None, **scatter_kw):
    #TODO: docstring
    """Plot scatter."""

    # View inputs as numpy arrays of appropriate dimensions
    x, y = np.atleast_1d(x, y)
    c_was_given = c is not None
    if c_was_given:
        c = np.atleast_1d(c)

    # Get default axes and define other optional keywords
    if axes is None:
        axes = plt.gca()
    if norm is None:
        norm = "log" if log else "linear"
    if add_colorbar_kw is None:
        add_colorbar_kw = {}

    # Transpose signals if desired
    if transpose:
        x, y = (a.T for a in (x, y))
        if c_was_given:
            c = c.T

    # Check input shapes and broadcast if needed
    error_message = (
        'Dimensions of c {} are incompatible with x ({}) and/or y ({}).'
        ).format(c.shape, x.shape, y.shape)
    if any(a.ndim > 1 for a in (x, y)) or (c_was_given and c.ndim > 1):
        if x.ndim == 1:
            x = x[:, np.newaxis]
        if y.ndim == 1:
            y = y[np.newaxis, :]
    if c_was_given:
        try:
            xy_shape = np.broadcast_shapes(x.shape, y.shape, c.shape[:2])
        except ValueError:
            #TODO: improve error message (try an example with x 1D, y 0D, z 2D to see the problem)
            raise TypeError(error_message)
        x, y = (np.broadcast_to(a, shape=xy_shape) for a in (x, y))
        c = np.broadcast_to(c, shape=(xy_shape + c.shape[2:]))
    else:
        x, y = np.broadcast_arrays(x, y)
    
    # Determine relevant data ranges for normalization and visualization
    if x_relevant_min is None:
        x_relevant_min = xmin if xmin is not None else np.nanmin(x)
    if x_relevant_max is None:
        x_relevant_max = xmax if xmax is not None else np.nanmax(x)
    if y_relevant_min is None:
        y_relevant_min = ymin if ymin is not None else np.nanmin(y)
    if y_relevant_max is None:
        y_relevant_max = ymax if ymax is not None else np.nanmax(y)
    mask_relevant = (
        (x >= x_relevant_min) & (x <= x_relevant_max) &
        (y >= y_relevant_min) & (y <= y_relevant_max))
    
    # Normalize data if desired
    #TODO: make it work with c.ndim > 2? (RGB and RGBA values)
    if c_was_given and c.ndim <= 2:
        if normalize_all:
            c /= np.nanmax(c[mask_relevant])
        #TODO: use masked array in more places?
        c_masked = np.ma.array(c, mask=~mask_relevant)
        if normalize_along_x:
            c /= np.nanmax(c_masked, axis=0)
        if normalize_along_y:
            c /= np.nanmax(c_masked, axis=1)[:, np.newaxis]

        # Define default color normalization range
        if norm in ["linear", "log"]:
            vmin, vmax = default_color_normalization(
                c[mask_relevant], clip_low, clip_high, portion, norm, vmin, 
                vmax)

    # Select data to draw
    x_relevant, y_relevant = (a[mask_relevant] for a in (x, y))
    if xmin is None:
        xmin = np.nanmin(x_relevant)
    if xmax is None:
        xmax = np.nanmax(x_relevant)
    if ymin is None:
        ymin = np.nanmin(y_relevant)
    if ymax is None:
        ymax = np.nanmax(y_relevant)
    mask_to_draw = np.full_like(x, True, dtype=bool)
    if not all_x:
        mask_to_draw = mask_to_draw & (x >= xmin) & (x <= xmax)
    if not all_y:
        mask_to_draw = mask_to_draw & (y >= ymin) & (y <= ymax)
    x, y = (a[mask_to_draw] for a in (x, y))
    if c_was_given:
        c = c[mask_to_draw]

    # Draw scatter
    if rasterized is None:
        rasterized = x.size > n_threshold_rasterized
    # TODO: add antialiased=True as a default keyword?
    default_scatter_kw = dict(
        c=c, norm=norm, cmap=cmap, vmin=vmin, vmax=vmax, label=label, 
        rasterized=True)
    default_scatter_kw.update(scatter_kw)
    path_collection = axes.scatter(x, y, **default_scatter_kw)

    # Draw color bar and add units to label if desired
    if color_bar is None:
        color_bar = c_was_given
    if color_bar:
        if (
                write_c_label and c_label is not None and units is not None and 
                units != ""):
            c_label += " ({})".format(units)
        default_add_colorbar_kw = dict(
            mappable=path_collection, ax=axes, label=c_label)
        default_add_colorbar_kw.update(add_colorbar_kw)
        add_colorbar(**default_add_colorbar_kw)

    # # Write legend
    # TODO: use legend()?
    # TODO: allow list of labels for 2D input with several labels at once
    # labels = np.atleast_1d(labels)
    # for path, label in zip(path_collection, labels):
    #     path.set_label(label)
    return path_collection, ymin, ymax


# TODO: add option to share color bars
# TODO: example
def plots(
    # dataset arguments
        x, y, z=None,
        ex=None, ey=None, #ez=None,
        labels=None,
        x_multipliers=1, y_multipliers=1, z_multipliers=1,
        x_offsets=0, y_offsets=0, z_offsets=0,
        x_units=None, y_units=None, z_units=None,
    # TODO: include effect of z_mins and z_maxs
    # TODO: allow specifying limit values for x_mins, y_mins, etc., like cview (e.g. y_maxs="<100") or with other keywords (e.g. y_max_limits=100)
        x_mins=None, y_mins=None, #z_mins=None,
        x_maxs=None, y_maxs=None, #z_maxs=None,
        x_relevant_mins=None, y_relevant_mins=None, #z_relevant_mins=None,
        x_relevant_maxs=None, y_relevant_maxs=None, #z_relevant_maxs=None,
        linestyles=None, linewidths=None, markers=None, markersizes=None,
        elinewidths=None, all_x=None, all_y=None, 
        plot_types=None,
        plot_flags=True,
        boxes=None,
    # box arguments
        n_boxes=None, indices_sort_boxes=None,
        x_labels='', y_labels='', z_labels='',
    # TODO: add labels for rows or columns (e.g. x_labels_cols, y_labels_rows)
    # TODO: add box multipliers
        x_units_boxes=None, y_units_boxes=None, z_units_boxes=None,
    # TODO: include effect of z_mins_boxes and z_maxs_boxes
        x_mins_boxes=None, y_mins_boxes=None, #z_mins_boxes=None,
        x_maxs_boxes=None, y_maxs_boxes=None, #z_maxs_boxes=None,
    # TODO: add limits for rows or columns (e.g. x_mins_cols, y_mins_rows, etc.)
        x_title_indices=None, y_title_indices=None, z_title_indices=None,
        x_title_flags=True, y_title_flags=True, z_title_flags=None,
        write_legends=True,
    # other arguments (array-like)
        plot_indices=None,
    # other arguments (scalar)
        x_title=None, y_title=None, z_title=None,
        x_title_x_pos=None, y_title_x_pos=None, z_title_x_pos=None,
        x_title_y_pos=None, y_title_y_pos=None, z_title_y_pos=None,
        title=None, title_x_pos=None, title_y_pos=None,
        order='column',
        maintain_colors=False, use_custom_style=True, load_base_style=True, 
        show=False, save_fig=False, close=False, fig_name=None, 
        return_plot=True, return_indices_sort_boxes=False,
    # dataset dict arguments
        plot_kw=None, profilogram_kw=None,
        plot_color_mesh_or_image_show_kw={}, plot_color_mesh_kw={},
        image_show_kw={}, legend_annotate_kw=None,
    # box dict arguments
        legend_kw=None,
    # other dict arguments
        save_figure_kw=None,
        **subplotting_kw):
    """
    Plot one or multiple datasets in one or multiple axes, with one or
    multiple types of plots (e.g. trace, scatter, errorbar, color mesh).

    For simplicity, many input parameters can be given either as a single
    value or as a list/tuple/array of values. The number of datasets to plot is
    determined from the parameter with the most values and the parameters
    given as single values are then extended to match the total number of
    datasets.

    Optional parameters given as lists can include None elements for default
    behavior.

    Parameters
    ----------
    x, y : array-like or list of array-like
        The horizontal / vertical coordinates of the data points. These
        parameters can be 1D arrays, or lists of 1D arrays (in that case, each
        list element represents a different dataset).
    z : array-like or list of array-like, optional
        2-D Array of scalar values to color-map against x and y. If `z` is
        provided, `x` and `y` will become the horizontal and vertical
        coordinates on a color map.
    ex, ey : float, array-like, or list of array-like, shape(N,), optional
        The errorbar sizes:
            - scalar: Symmetric +/- values for all data points.
            - shape(N,): Symmetric +/-values for each data point.
            - *None*: No errorbar.
        All values must be >= 0.
    labels : str or list/array of str, optional
        Label for each data set. The labels are placed automatically in a
        legend.
    x_multipliers, y_multipliers, z_multipliers : float or list/array of
            float, optional
        Multiply the x/y/z coordinates of each dataset by a constant.
    x_offsets, y_offsets, z_offsets : float or list/array of float, optional
        Add a constant to offset the x/y/z coordinates of each dataset.
    x_units, y_units, z_units : str or list/array of str, optional
        A string specifying the units of each dataset. The units will be used
        to automatically create labels for the axes.
    x_mins, y_mins, x_maxs, y_maxs : float or list/array of float, optional
        Trim the datasets in x and y before plotting and adjust the view
        limits and color scales accordingly. For 2D traces, the trimmed data
        includes the first data point outside the boundary.
        Defaults to 'x/y_mins/maxs_boxes' of the box corresponding to each
        dataset.
        The parameter `all_x`/`all_y` can be set to True to prevent trimming 
        and instead adjust only the viewing limits.
    x_relevant_mins, y_relevant_mins, x_relevant_maxs, y_relevant_maxs : float
            or list/array of float, optional
        Specify coordinate range for default color normalization and default y
        range without affecting displayed x range nor triming data.
    linestyles : str or list/array of str, optional
            ({'-', '--', '-.', ':', ''})
        ===============================   =================
        Linestyle                         Description
        ===============================   =================
        ``'-'`` or ``'solid'``            solid line
        ``'--'`` or  ``'dashed'``         dashed line
        ``'-.'`` or  ``'dashdot'``        dash-dotted line
        ``':'`` or ``'dotted'``           dotted line
        ``'None'`` or ``' '`` or ``''``   draw nothing
        ===============================   =================
    linewidths : float or list/array of float, optional
        Line width, in points.
    markers : str or list/array of str, optional
        Marker style. See all available markers here
        https://matplotlib.org/stable/api/markers_api.html
    markersizes : float or list/array of float, optional
        Marker size, in points.
    elinewidths : float or list/array of float, optional
        Error bar line width, in points.
    all_x, all_y: bool or list/array of bool, optional
        Used in conjunction with *x_mins, y_mins, x_maxs, y_maxs*.
        If True, plots all data points and then adjusts the viewing limits. If
        False, points outside the min/max boundary are not plotted (except the
        first point outside the boundary for traces and profilograms).
    plot_types : str or list/array of str, optional
            ({'trace', 'scatter', 'errorbar', 'profilogram'})
        Specify the type of plot. If not provided or ``None`` the plot type is
        determined automatically based on each dataset.
    plot_flags : bool or list/array of bool, optional
        Mask to show (True) / hide (False) certain datasets.
    boxes : int or list/array of int, optional
        Specify in which axes (box) to plot each dataset.
        For example,``box = [0, 1, 1]`` will plot the first dataset in the
        first box, and the second and third data sets in the second box.
    n_boxes : int, optional
        Number of boxes to draw in. Useful if you want to create more boxes
        than the ones you are drawing in with the function call.
    indices_sort_boxes : list/array of int, optional
        Indices to reorder boxes, e.g., [0, 3, 2, 1] will swap the second and
        fourth boxes.
    x_labels, y_labels, z_labels : str or list/array of str, optional
        Labels for the x/y/z axes. If there are units, the displayed label for
        each x/y/z axis "i" is constructed as
        "{x/y/z_label[i]} ({x/y/z_units_boxes[i]})".
    x_units_boxes, y_units_boxes, z_units_boxes : str or list/array of str,
            optional
        Specify the units for each box. Defaults to 'x/y/z_units' in each box
        if all datasets in that box have the same units. The units are used to
        automatically create labels for the axes.
    x_mins_boxes, y_mins_boxes, x_maxs_boxes, y_maxs_boxes : float or
            list/array of float, optional
        Specify the data limits similarly to 'x/y_mins/maxs' but for each box
        instead of for individual datasets. Defaults to the minimum/maximum
        value of 'x/y_mins/maxs' of the datasets in each box.
    x_title_indices, y_title_indices, z_title_indices : int or list/array of
            int, optional
        Indices of the boxes to which the 'x/y/z_title_x/y_pos' default values
        refer to. Used to automatically align the axes titles with the
        corresponding boxes. Defaults to the indices of the True elements in
        'x/y/z_title_flags'.
    x_title_flags, y_title_flags, z_title_flags : bool or list/array of bool,
            optional
        Same as 'x/y/z_title_indices' but using a boolean mask. It is
        overwritten by 'x/y/z_title_indices'.
    write_legends : bool or list/array of bool, optional
        Mask to show (True) / hide (False) the legends for each box based on
        the `labels` of the corresponding datasets.
    plot_indices : int or list/array of int, optional
        Show or hide certain datasets, similarly to 'plot_flags', but
        specifying the indices of the datasets to be shown instead of a
        boolean mask. Overwrites 'plot_flags'.
    x_title, y_title, z_title : str
        Common title for the x/y/z axes of multiple boxes, positioned based on
        'x/y/z_title_x/y_pos'.
    x_title_x_pos, y_title_x_pos, z_title_x_pos, x_title_y_pos, y_title_y_pos,
            z_title_y_pos : float
        Specify the x/y position of the x/y/z common axis title as a fraction
        of the figure size. Defaults to positions that center-align the titles
        with the boxes given by 'x/y/z_title_indices/flags'.
    title : str
        Figure title. By default this title is placed on the top and centered.
    title_x_pos, title_y_pos : float
        Specify the x/y position of the figure title as a fraction of the
        figure size. Defaults to the top center of the figure.
    order : {'column', 'row'}
        Order the boxes by column or by row, for example when placing the
        subplot (a), (b), (c), etc. subcaptions.
    maintain_colors : bool, optional
        If True, skip the color in the current colorwheel of each dataset that
        is not plotted.
    use_custom_style : bool, optional
        If true, adapt the style to the plot types.
    load_base_style : bool, optional
        If true, use the custom *ipfnpytools* style as the base.
    show : bool
        If True, invoke plt.show() after drawing.
    save_fig : bool, optional
        If true, save the figure to a file with save_figure.
    close : bool, optional
        If True, close the figure after plotting with plt.close. Useful if you
        just want to save the figure to a file but not display it.
    fig_name : str, optional
        Specify a file name to save the figure. Defaults to "plots".
    return_plot : bool, optional
        If True, return the figure object, the plotted axes, and possibly the
        indices to sort the boxes (depending on 'return_indices_sort_boxes').
        If False, return None.
    return_indices_sort_boxes : bool, optional
        If True and 'return_plot' is also true, return the sorted boxes
        indices in addition to the figure and axes.
    plot_kw : dict or list/array of dict, optional
        Additional keyword arguments passed to ipfnpytools.plot.plot_traces,
        ipfnpytools.plot.plot_errorbars, or matplotlib.axes.Axes.scatter for
        each datased.
    profilogram_kw : dict or list/array of dict, optional
        Additional keyword arguments passed to
        ipfnpytools.plot.plot_profilogram for each dataset.
    plot_color_mesh_or_image_show_kw : dict or list/array of dict, optional
        Additional keyword arguments passed to
        ipfnpytools.image_show.image_show or
        ipfnpytools.plot_color_mesh.plot_color_mesh for each dataset,
        depending on what ipfnpytools.plot_profilogram uses, which by default
        depends on whether the x and y data points are equally spaced.
    plot_color_mesh_kw : dict, optional
        Additional keyword arguments passed to
        ipfnpytools.plot_color_mesh.plot_color_mesh for each dataset.
    image_show_kw : dict or list/array of dict, optional
        Additional keyword arguments passed to
        ipfnpytools.image_show.image_show for each dataset.
    legend_annotate_kw : dict or list/array of dict, optional
        Additional keyword arguments passed to matplotlib.axes.Axes.annotate
        for each dataset when writing the legend of a color plot.
    legend_kw : dict or list/array of dict, optional
        Additional keyword arguments passed to matplotlib.axes.Axes.legend for
        each box.
    save_figure_kw : dict, optional
        Additional keyword arguments passed to
        ipfnpytools.save_figure.save_figure.
    **subplotting_kw : kwargs
        Additional keyword arguments passed down to
        ipfnpytools.subplotting.subplotting.

    Returns
    -------
        fig : matplotlib.figure.Figure object, optional
            Figure that contains all boxes.
            Only returned if 'return_plot' is True.
        ax : matplotlib.axes.Axes object or array of Axes, optional
            Axes corresponding to each box.
            Only returned if 'return_plot' is True.
            *ax* can be either a single `matplotlib.axes.Axes` object or an
            array of Axes objects if more than one subplot was created. The
            dimensions of the resulting array can be controlled with
            **subplotting_kw keywords, see the
            ipfnpytools.subplotting.subplotting documentation.
        indices_sort_boxes : array of int, optional
            Sorted boxes indices.
            Only returned if 'return_plot' and 'return_indices_sort_boxes' are
            both True.

    See Also
    --------
    plot_signals

    Examples
    --------

    """

    # Print all warnings, even if repeated
    warnings.simplefilter('always', UserWarning)

    # Convert data to object arrays
    x, y, z, ex, ey = (args.special_object_array(a) for a in (x, y, z, ex, ey))

    # Define arguments that can be replicated, use them to determine the
    # number of datasets and replicate them to this number as numpy arrays
    args_rep = [
        x, y, z, ex, ey, x_multipliers, y_multipliers, z_multipliers, x_units,
        y_units, z_units, x_offsets, y_offsets, z_offsets, x_mins, y_mins, #z_mins,
        x_maxs, y_maxs, #z_maxs,
        x_relevant_mins, y_relevant_mins, #z_relevant_mins,
        x_relevant_maxs, y_relevant_maxs, #z_relevant_maxs,
        labels, linestyles, linewidths, markers, markersizes, elinewidths,
        all_x, all_y, plot_types, plot_flags, boxes, plot_kw, profilogram_kw,
        plot_color_mesh_or_image_show_kw, plot_color_mesh_kw, image_show_kw,
        legend_annotate_kw]
    args.listify(args_rep)
    n_datasets = max(map(len, args_rep))
    args.replicate_elements(args_rep, n_datasets)
    args.arrayfy(args_rep)
    (
        x, y, z, ex, ey, x_multipliers, y_multipliers, z_multipliers, x_units,
        y_units, z_units, x_offsets, y_offsets, z_offsets, x_mins, y_mins, #z_mins,
        x_maxs, y_maxs, #z_maxs,
        x_relevant_mins, y_relevant_mins, #z_relevant_mins,
        x_relevant_maxs, y_relevant_maxs, #z_relevant_maxs,
        labels, linestyles, linewidths, markers, markersizes, elinewidths,
        all_x, all_y, plot_types, plot_flags, boxes, plot_kw, profilogram_kw,
        plot_color_mesh_or_image_show_kw, plot_color_mesh_kw, image_show_kw,
        legend_annotate_kw) = args_rep

    # Define default dict arguments
    args.set_default(plot_kw, {})
    args.set_default(profilogram_kw, {})
    args.set_default(plot_color_mesh_or_image_show_kw, {})
    args.set_default(plot_color_mesh_kw, {})
    args.set_default(image_show_kw, {})
    args.set_default(legend_annotate_kw, {})

    # Define which datasets to plot
    if plot_indices is not None:
        plot_flags = np.full(n_datasets, False, dtype=bool)
        plot_flags[plot_indices] = True

    # Define default plot types
    z_ndim = np.array([a.ndim if a is not None else 0 for a in z])
    args.set_default(
        plot_types,
        np.select(
            [
                z_ndim == 1, z_ndim == 2, 
                [ex_i is None and ey_i is None for ex_i, ey_i in zip(ex, ey)]],
            ['scatter', 'profilogram', 'trace'],
            default='errorbar'))

    # Use custom matplotlib style sheets for good looking plots if desired
    if use_custom_style:
        original_style = dict(mpl.rcParams.copy())
        directory = os.path.dirname(__file__)
        if load_base_style:
            use_base_style(directory)
        # TODO: use the correct style in each plot, even if there are color plot and traces in the same figure
        if np.in1d(['scatter', 'profilogram'], plot_types).any():
            plt.style.use(os.path.join(directory, r'color_bar.mplstyle'))

    # Define default boxes and identify which datasets belong to each box
    args.set_default(boxes, np.arange(n_datasets))
    boxes = boxes.astype(int)
    if n_boxes is None:
        n_boxes = boxes.max() + 1
    unique_boxes, box_1st_signal_indices = np.unique(boxes, return_index=True)
    box_masks = boxes == unique_boxes[:, np.newaxis]
    is_not_none = [y_i is not None for y_i in y]
    is_trace_box_masks = np.all(
        np.broadcast_arrays(box_masks, plot_types == 'trace', is_not_none),
        axis=0)
    is_errorbar_box_masks = np.all(
        np.broadcast_arrays(box_masks, plot_types == 'errorbar', is_not_none),
        axis=0)
    box_plot_types = [plot_types[box_mask] for box_mask in box_masks]
    color_boxes = unique_boxes[np.array(
        [
            np.in1d(['scatter', 'profilogram'], types).any()
            for types in box_plot_types])]

    # Define default values for box variables
    if indices_sort_boxes is None:
        indices_sort_boxes = np.arange(n_boxes)
    else:
        indices_sort_boxes = np.array(indices_sort_boxes)
    (
        x_labels, y_labels, z_labels, x_units_boxes, y_units_boxes,
        z_units_boxes, x_mins_boxes, y_mins_boxes, #z_mins_boxes,
        x_maxs_boxes, y_maxs_boxes, #z_maxs_boxes,
        x_title_flags, y_title_flags, z_title_flags,
        write_legends, legend_kw) = args.extend_args(
        x_labels, y_labels, z_labels, x_units_boxes, y_units_boxes,
        z_units_boxes, x_mins_boxes, y_mins_boxes, #z_mins_boxes,
        x_maxs_boxes, y_maxs_boxes, #z_maxs_boxes,
        x_title_flags, y_title_flags, z_title_flags,
        write_legends, legend_kw, n=n_boxes)
    args.set_default(legend_kw, {})
    label_color = np.full(n_boxes, "black", dtype=object)
    annotate_kw = np.full(n_boxes, {}, dtype=object)
    for i, box_mask in zip(color_boxes, box_masks[color_boxes]):
        brightness = []
        for kw in plot_color_mesh_or_image_show_kw[box_mask]:
            brightness.append(average_brightness(kw.get(
                'cmap', plt.get_cmap(plt.rcParams['image.cmap']))))
        if np.mean(brightness) < 0.5:
            label_color[i] = "white"
            annotate_kw[i] = dict(path_effects=shadow)
        else:
            label_color[i] = "black"
            annotate_kw[i] = dict(path_effects=white_shadow)
    for lims_boxes, lims, ext in zip(
            [x_mins_boxes, y_mins_boxes, x_maxs_boxes, y_maxs_boxes],
            [x_mins, y_mins, x_maxs, y_maxs],
            [np.min, np.min, np.max, np.max]):
        args.set_default(
            lims_boxes,
            [
                ext(lims[mask]) if np.any(mask) else None
                for mask in box_masks & (lims != None)])
    args.set_default(
        x_units_boxes,
        [
            x_units[box_mask][0] if all_equal(x_units[box_mask]) else None
            for box_mask in box_masks])
    args.set_default(
        y_units_boxes,
        [
            y_units[box_mask][0] if all_equal(y_units[box_mask]) else None
            for box_mask in box_masks])
    args.set_default(
        z_units_boxes,
        [
            z_units[box_mask][0] if all_equal(z_units[box_mask]) else None
            for box_mask in box_masks])

    # Define default values for dataset variables which depend on the boxes
    args.set_default(x_mins, x_mins_boxes[boxes])
    args.set_default(y_mins, y_mins_boxes[boxes])
    # args.set_default(z_mins, z_mins_boxes[boxes])
    args.set_default(x_maxs, x_maxs_boxes[boxes])
    args.set_default(y_maxs, y_maxs_boxes[boxes])
    # args.set_default(z_maxs, z_maxs_boxes[boxes])
    # args.set_default(x_relevant_mins, x_mins)
    # args.set_default(y_relevant_mins, y_mins)
    # #    args.set_default(z_relevant_mins, z_mins)
    # args.set_default(x_relevant_maxs, x_maxs)
    # args.set_default(y_relevant_maxs, y_maxs)
    # #    args.set_default(z_relevant_maxs, z_maxs)
    args.set_default(x_units, x_units_boxes[boxes])
    args.set_default(y_units, y_units_boxes[boxes])
    args.set_default(z_units, z_units_boxes[boxes])

    # Add units to labels
    indices = np.all(
        (
            np.not_equal(labels, None), np.not_equal(y_units, None),
            y_units != '', y_units != y_units_boxes[boxes]),
        axis=0)
    labels[indices] += [" ({})".format(units) for units in y_units[indices]]

    # Define default title flags and indices
    if x_title_indices is None:
        x_title_indices = np.flatnonzero(x_title_flags)
    x_title_indices = np.array(x_title_indices)
    if y_title_indices is None:
        y_title_indices = np.flatnonzero(y_title_flags)
    y_title_indices = np.array(y_title_indices)
    default_z_title_flags = np.full(n_boxes, False, dtype=bool)
    default_z_title_flags[y_title_indices] = True
    args.set_default(z_title_flags, default_z_title_flags)
    if z_title_indices is None:
        z_title_indices = np.flatnonzero(z_title_flags)
    z_title_indices = np.array(z_title_indices)

    # Create subplots
    # TODO: Adjust wspace according to the number of columns with color plots
    default_subplotting_kw = dict(
        label_color=label_color[np.argsort(indices_sort_boxes)],
        annotate_kw=annotate_kw[np.argsort(indices_sort_boxes)], order=order)
    default_subplotting_kw.update(subplotting_kw)
    fig, axes = subplotting(nplots=n_boxes, **default_subplotting_kw)
    axes_flat = (
        np.array([axes]).flatten({"column":"F", "row":"C"}[order])
        [indices_sort_boxes])
    nrows, ncols = fig.axes[0].get_subplotspec().get_gridspec().get_geometry()

    # Define default title positions
    figwidth_factor = plt.rcParams['figure.figsize'][0] / fig.get_figwidth()
    figheight_factor = plt.rcParams['figure.figsize'][1] / fig.get_figheight()
    wspan_in_fig = fig.subplotpars.right - fig.subplotpars.left
    hspan_in_fig = fig.subplotpars.top - fig.subplotpars.bottom
    wspan_in_axes = ncols + fig.subplotpars.wspace * (ncols - 1)
    hspan_in_axes = nrows + fig.subplotpars.hspace * (nrows - 1)
    wstep_in_axes = 1 + fig.subplotpars.wspace
    hstep_in_axes = 1 + fig.subplotpars.hspace
    wstep_in_fig = wstep_in_axes * wspan_in_fig / wspan_in_axes
    hstep_in_fig = hstep_in_axes * hspan_in_fig / hspan_in_axes
    if title_x_pos is None:
        title_x_pos = 0.5
    if title_y_pos is None:
        title_y_pos = 1 - 0.045 * figheight_factor
    if x_title_x_pos is None:
        pos = (
            (0.5 + np.mean(x_title_indices // nrows) * wstep_in_axes) /
            wspan_in_axes)
        x_title_x_pos = fig.subplotpars.left + pos * wspan_in_fig
    if x_title_y_pos is None:
        x_tick_space = (
            0.0167 if plt.rcParams['xtick.direction'] in ['out', 'inout']
            else 0)
        x_title_y_pos = (
            fig.subplotpars.bottom -
            (x_tick_space + 0.11) * figheight_factor +
            (nrows - 1 - (x_title_indices % nrows).max()) * hstep_in_fig)
    if y_title_x_pos is None:
        y_tick_space = (
            0.0167 if plt.rcParams['ytick.direction'] in ['out', 'inout']
            else 0)
        y_title_x_pos = (
            fig.subplotpars.left - (y_tick_space + 0.117) * figwidth_factor +
            (y_title_indices // nrows).min() * wstep_in_fig)
    if y_title_y_pos is None:
        pos = 1 - (
            (0.5 + np.mean(y_title_indices % nrows) * hstep_in_axes) /
            hspan_in_axes)
        y_title_y_pos = fig.subplotpars.bottom + pos * hspan_in_fig
    if z_title_x_pos is None:
        z_title_x_pos = (
            fig.subplotpars.right + 0.172 * figwidth_factor -
            (ncols - 1 - (z_title_indices // nrows).max()) * wstep_in_fig)
    if z_title_y_pos is None:
        pos = 1 - (
            (0.5 + np.mean(z_title_indices % nrows) * hstep_in_axes) /
            hspan_in_axes)
        z_title_y_pos = fig.subplotpars.bottom + pos * hspan_in_fig

    # Draw titles
    if title is not None:
        fig.text(
            title_x_pos, title_y_pos, title, ha='center', va='center',
            size=plt.rcParams['figure.titlesize'])
    if x_title is not None:
        fig.text(
            x_title_x_pos, x_title_y_pos, x_title, ha='center', va='center',
            size=plt.rcParams['axes.labelsize'])
    if y_title is not None:
        fig.text(
            y_title_x_pos, y_title_y_pos, y_title, ha='center', va='center',
            rotation='vertical', size=plt.rcParams['axes.labelsize'])
    if z_title is not None:
        fig.text(
            z_title_x_pos, z_title_y_pos, z_title, ha='center', va='center',
            rotation='vertical', size=plt.rcParams['axes.labelsize'])

    # Plot each dataset in the desired way
    lines, errorbars, path_collection = (
        np.empty(n_datasets, dtype=object) for _ in range(3))
    y_mins_f, y_maxs_f = (np.full(n_datasets, np.nan) for _ in range(2))
    for i, flag in enumerate(plot_flags):
        ax = axes_flat[boxes[i]]
        if flag:
            try:

                # Apply multipliers and offsets
                x_i = x[i] * x_multipliers[i] + x_offsets[i]
                y_i = y[i] * y_multipliers[i] + y_offsets[i]
                z_i = (
                    z[i] * z_multipliers[i] + z_offsets[i] if z[i] is not None
                    else None)
                ex_i = ex[i] * x_multipliers[i] if ex[i] is not None else None
                ey_i = ey[i] * y_multipliers[i] if ey[i] is not None else None

                # Trace:
                #TODO: implement all_y
                if plot_types[i] == 'trace':
                    lines[i], y_mins_f[i], y_maxs_f[i] = plot_traces(
                        x_i, y_i, axes=ax, labels=labels[i], xmin=x_mins[i],
                        xmax=x_maxs[i], ymin=y_mins[i], ymax=y_maxs[i],
                        x_relevant_min=x_relevant_mins[i],
                        x_relevant_max=x_relevant_maxs[i],
                        linestyle=linestyles[i], marker=markers[i],
                        markersize=markersizes[i], all_x=all_x[i],
                        smart_linewidth=False, **plot_kw[i])

                # Error bar:
                #TODO: implement all_y
                elif plot_types[i] == 'errorbar':
                    errorbars[i], y_mins_f[i], y_maxs_f[i] = plot_errorbars(
                        x_i, y_i, xerr=ex_i, yerr=ey_i, axes=ax,
                        label=labels[i], xmin=x_mins[i], xmax=x_maxs[i],
                        ymin=y_mins[i], ymax=y_maxs[i],
                        x_relevant_min=x_relevant_mins[i],
                        x_relevant_max=x_relevant_maxs[i],
                        linestyle=linestyles[i], elinewidth=elinewidths[i],
                        marker=markers[i], markersize=markersizes[i],
                        all_x=all_x[i], smart_markersize=False, **plot_kw[i])

                # Scatter:
                #TODO: correctly implement scatter plots
                #TODO: decide which arguments should be available in the mother function
                #TODO: pass **scatter_kw instead of **plot_kw (and/or make a common one?)
                #TODO: refactor arguments in common with profilogram
                elif plot_types[i] == 'scatter':
                    path_collection[i], y_mins_f[i], y_maxs_f[i] = plot_scatter(
                        x_i, y_i, c=z_i, axes=ax, units=z_units[i], 
                        label=labels[i], c_label=z_labels[boxes[i]], 
                        xmin=x_mins[i], xmax=x_maxs[i],
                        ymin=y_mins[i], ymax=y_maxs[i],
                        x_relevant_min=x_relevant_mins[i],
                        x_relevant_max=x_relevant_maxs[i],
                        y_relevant_min=y_relevant_mins[i],
                        y_relevant_max=y_relevant_maxs[i],
                        write_c_label=(not z_title_flags[boxes[i]]),
                        marker=markers[i], s=markersizes[i], 
                        all_x=all_x[i], all_y=all_y[i], **plot_kw[i])

                # Profilogram:
                #TODO: decide which arguments should be available in the mother function
                #TODO: implement all_y
                elif plot_types[i] == 'profilogram':
                    plot_profilogram(
                        x_i, y_i, z_i, axes=ax, units=z_units[i],
                        text=labels[i], c_label=z_labels[boxes[i]],
                        xmin=x_mins[i], xmax=x_maxs[i],
                        ymin=y_mins[i], ymax=y_maxs[i],
                        x_relevant_min=x_relevant_mins[i],
                        x_relevant_max=x_relevant_maxs[i],
                        y_relevant_min=y_relevant_mins[i],
                        y_relevant_max=y_relevant_maxs[i],
                        write_c_label=(not z_title_flags[boxes[i]]),
                        all_x=all_x[i],
                        plot_color_mesh_or_image_show_kw=(
                            plot_color_mesh_or_image_show_kw[i]),
                        plot_color_mesh_kw=plot_color_mesh_kw[i],
                        image_show_kw=image_show_kw[i],
                        annotate_kw=legend_annotate_kw[i], **profilogram_kw[i])

                # Raise error in case of invalid plot type
                else:
                    raise ValueError(
                        "'{}' is not a valid plot type.".format(plot_types[i]))

            # In case of error print warning message and advance color if desired
            except Exception as e:
                #TODO: add original traceback to warning message
                warnings.warn(
                    "not plotting dataset {} due to {}: {!r}".format(
                        i, type(e).__name__, e.args),
                    stacklevel=3)
                if plot_types[i] == 'trace' and maintain_colors:
                    next(ax._get_lines.prop_cycler)['color']

        # If desired advance color even if dataset is skipped
        else:
            if plot_types[i] == 'trace' and maintain_colors:
                next(ax._get_lines.prop_cycler)['color']

    # For each box
    for i, box, in zip(count(), unique_boxes):
        ax = axes_flat[box]

        # Adjust x range and get x limits
        #TODO: treat x limits the same way as y limits
        ax.set_xlim(x_mins_boxes[i], x_maxs_boxes[i])
        x_min, x_max = ax.get_xlim()

        # Adjust y range and get y limits
        box_mask = box_masks[box]
        with warnings.catch_warnings():
            warnings.filterwarnings(
                action='ignore', message='All-NaN slice encountered')
            y_min = np.nanmin(y_mins_f[box_mask])
            y_max = np.nanmax(y_maxs_f[box_mask])
        if np.all(np.isfinite((x_min, y_min, x_max, y_max))):
            ax.dataLim.update_from_data_xy(((x_min, y_min), (x_max, y_max)))
            ax.autoscale(axis='y')
        ax.set_ylim(y_mins_boxes[i], y_maxs_boxes[i])
        y_min, y_max = ax.get_ylim()

        # Adjust linewidths of trace plots
        trace_mask = is_trace_box_masks[box]
        for line_list, linewidth, plotkw in zip(
                lines[trace_mask], linewidths[trace_mask],
                plot_kw[trace_mask]):
            if line_list is not None:
                for line in line_list:
                    if linewidth is None:
                        plt.setp(
                            line,
                            linewidth=calc_smart_linewidth(
                                line._x, line._y, ymin=y_min, ymax=y_max,
                                p_width=plotkw.get("p_width")))
                    else:
                        plt.setp(line, linewidth=linewidth)

        # Adjust markersizes and linewidths of errorbar plots
        errorbar_mask = is_errorbar_box_masks[box]
        for errorbar_list, markersize, linestyle, linewidth, plotkw in zip(
                errorbars[errorbar_mask], markersizes[errorbar_mask],
                linestyles[errorbar_mask], linewidths[errorbar_mask],
                plot_kw[errorbar_mask]):
            if errorbar_list is not None:
                x_joined = np.concatenate([a[2][0]._x for a in errorbar_list])
                if x_joined.size > 0:
                    y_joined = np.concatenate(
                        [a[2][0]._y for a in errorbar_list])
                    if markersize is None:
                        xerr_joined = (
                            np.concatenate([a[0] for a in errorbar_list])
                            if errorbar_list[0][0] is not None else None)
                        yerr_joined = (
                            np.concatenate([a[1] for a in errorbar_list])
                            if errorbar_list[0][1] is not None else None)
                        markersize = calc_smart_markersize(
                            x=x_joined, y=y_joined, xerr=xerr_joined,
                            yerr=yerr_joined, xmax=x_max, xmin=x_min,
                            ymax=y_max, ymin=y_min, axes=ax,
                            base_markersize=None)
                    for errorbar in errorbar_list:
                        plt.setp(errorbar[2][0], markersize=markersize)
                    if linestyle is not None and linestyle != '':
                        if linewidth is None:
                            linewidth = calc_smart_linewidth(
                                x_joined, y_joined, ymin=y_min, ymax=y_max,
                                p_width=plotkw.get("p_width"))
                        for errorbar in errorbar_list:
                            plt.setp(errorbar[2][0], linewidth=linewidth)

        # Show legend with custom linewidths and markersizes if desired
        if (write_legends[box] and len(ax.get_legend_handles_labels()[0]) > 0):
            default_legend_kw = dict(
                loc=("upper left" if n_boxes > 1 else None), borderpad=0.1,
                labelspacing=0.1, handletextpad=0.3, borderaxespad=0.3,
                edgecolor='none', markerscale=0.8)
            default_legend_kw.update(legend_kw[i])
            with warnings.catch_warnings():
                warnings.filterwarnings("ignore", message=(
                    "No labelled objects found. Use label='...' kwarg on " +
                    "individual plots."))
                legend_ = ax.legend(**default_legend_kw)
                if legend_ is not None:
                    plt.setp(
                        legend_.get_lines(),
                        linewidth=plt.rcParams['lines.linewidth'])
                    for legend_handle in legend_.legend_handles:
                        legmarker = getattr(legend_handle, '_legmarker', None)
                        if legmarker is not None:
                            legmarker.set_markersize(np.clip(
                                legmarker.get_markersize(),
                                0.5*plt.rcParams['lines.markersize'],
                                plt.rcParams['lines.markersize']))

        # Add units to labels and write them
        if x_labels[i] is not None:
            if x_units_boxes[i] is not None and x_units_boxes[i] != '':
                x_labels[i] += " ({})".format(x_units_boxes[i])
            # Explicit size required because of matplotlib bug:
            # https://github.com/matplotlib/matplotlib/issues/25298/
            ax.set_xlabel(x_labels[i], size=mpl.rcParams['axes.labelsize'])
        if y_labels[i] is not None:
            if y_units_boxes[i] is not None and y_units_boxes[i] != '':
                y_labels[i] += " ({})".format(y_units_boxes[i])
            # Explicit size required because of matplotlib bug:
            # https://github.com/matplotlib/matplotlib/issues/25298/
            ax.set_ylabel(y_labels[i], size=mpl.rcParams['axes.labelsize'])

    # Show figure if desired
    if show:
        plt.show(block=False)

    # Save figure if desired
    if save_fig:
        if fig_name is None:
            fig_name = sys._getframe().f_code.co_name
        if save_figure_kw is None:
            save_figure_kw = {}
        default_save_figure_kw = {"fig": fig, "name": fig_name}
        default_save_figure_kw.update(save_figure_kw)
        save_figure(**default_save_figure_kw)

    # Close figure if desired
    if close:
        plt.close(fig)

    # Load back the original style if custom styles were used
    if use_custom_style:
        mpl.rcParams.update(original_style)

    # Return figure and axes if desired
    if return_plot:
        if return_indices_sort_boxes:
            return fig, axes, indices_sort_boxes
        return fig, axes

    #TODO: look for improvements in JET codes
    #TODO: add relative_change option in which the value of the first plotted sample is subtracted to the signal and use it when the plot_hop() 'relative_phase' option is used
    #TODO: adjust axes limits well and automatically? (subplot_kw xlim and ylim?, matplotlibrc axes.autolimit_mode, axes.xmargin, axes.ymargin, http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.autoscale, http://matplotlib.org/api/_as_gen/matplotlib.axes.Axes.autoscale.html#matplotlib.axes.Axes.autoscale, http://matplotlib.org/api/_as_gen/matplotlib.axes.Axes.margins.html#matplotlib.axes.Axes.margins)
    #TODO: make default value of the units depend on multipliers (use unit prefixes: p, n, \mu, m, k, M, G, etc.)
    #TODO: add option to share color bars (and update functions which call this one to also allow it there)


#TODO: add areas_err
#TODO: read units from data object
# TODO: add option to average signals (e.g. like IDL's nsum)
@timing
def plot_signals(
        times=None, signals=None, areas=None, times_err=None, signals_err=None,
        shots=None, diagnostics=None, names=None, data=None, labels=None, 
        labels_prefixes=None, labels_postfixes=None, t_multiplier=1, 
        t_offsets=0, t_units='s', t_begins=None, t_ends=None, 
        t_relevant_begins=None, t_relevant_ends=None, all_time=False,
        signal_multipliers=1, signal_units=None, area_multipliers=1, 
        area_units=None, area_mins=None, area_maxs=None, 
        area_relevant_mins=None, area_relevant_maxs=None, f_multipliers=1E-3, 
        f_offsets=0, f_units=None, f_mins=None, f_maxs=None, 
        f_relevant_mins=None, f_relevant_maxs=None, plot_types=None, 
        plot_flags=True, boxes=None, t_title='Time', title=None, 
        title_prefix=None, title_postfix=None, order='column', 
        write_f_label=True, write_c_label=True, write_shots=True, 
        write_diagnostics=True, write_names=True, postfixes=None, prefix="", 
        endfix=None, use_custom_style=True, load_base_style=True, 
        return_plot=True, return_fig_name=True, 
        plot_color_mesh_or_image_show_kw={}, image_show_kw=None, 
        specgram_kw=None, **plots_kw):
    # TODO: finish docstring
    """ Plot signals as a function of time in different ways (e.g. time trace,
    spectrogram, profilogram).

    For simplicity, many input parameters can be given either as a single
    value or as a list/tuple/array of values. The number of signals to plot is
    determined from the parameter with the most values and the parameters
    given as single values are then extended to match the total number of
    signals.

    Parameters
    ----------
    times : array or array list, optional
        Time base of each signal.
    signals : array or array list, optional
        Values of each signal.
    areas : array or array list, optional
        Area base of each signal.
    times_err : array or array list, optional
        Uncertainty in the time base of each signal.
    signals_err : array or array list, optional
        Uncertainty in the values of each signal.
    shots : int, optional
        Shot number.
    diagnostics : str or str list/tuple/array, optional
        Names of each diagnostic.
    names : str or str list/tuple/array, optional
        Names of each signal.
    data : object with all the data from the signals, optional
        Instead of specifying the signals, time base, area base and other
        parameters in different arguments, this argument can be used to input
        an object which contains all the necessary data in the following
        indices:
            shots
            diagnostics
            names
            times
            signals
            areas
    ...

    Returns
    -------
    If return_plot == True:
        fig :
            matplotlib.figure.Figure object
        axes :
            Axes object or array of Axes objects.
        If return_fig_name == True:
            fig_name: str
                Name of the figure.

    Examples
    --------

    """

    # Print all warnings, even if repeated
    warnings.simplefilter('always', UserWarning)

    # Convert signals, time and area bases to object arrays
    signals, times, areas, signals_err, times_err = (
        args.special_object_array(a)
        for a in (signals, times, areas, signals_err, times_err))

    # Define arguments that can be replicated, use them to determine the
    # number of signals and replicate them to this number as numpy arrays
    args_rep = [
        times, signals, areas, times_err, signals_err, shots, diagnostics,
        names, labels, labels_prefixes, labels_postfixes, plot_types,
        plot_flags, boxes, t_offsets, t_begins, t_ends, t_relevant_begins,
        t_relevant_ends, signal_multipliers, signal_units, area_multipliers,
        area_units, f_multipliers, f_offsets, f_units, f_mins, f_maxs,
        f_relevant_mins, f_relevant_maxs, area_mins, area_maxs,
        area_relevant_mins, area_relevant_maxs, all_time, write_f_label,
        write_c_label, write_shots, write_diagnostics, write_names,
        plot_color_mesh_or_image_show_kw, image_show_kw, specgram_kw]
    args.listify(args_rep)
    n_signals = max(map(len, args_rep + [getattr(data, 'signals', [])]))
    args.replicate_elements(args_rep, n_signals)
    args.arrayfy(args_rep)
    (
        times, signals, areas, times_err, signals_err, shots, diagnostics,
        names, labels, labels_prefixes, labels_postfixes, plot_types,
        plot_flags, boxes, t_offsets, t_begins, t_ends, t_relevant_begins,
        t_relevant_ends, signal_multipliers, signal_units, area_multipliers,
        area_units, f_multipliers, f_offsets, f_units, f_mins, f_maxs,
        f_relevant_mins, f_relevant_maxs, area_mins, area_maxs,
        area_relevant_mins, area_relevant_maxs, all_time, write_f_label,
        write_c_label, write_shots, write_diagnostics, write_names,
        plot_color_mesh_or_image_show_kw, image_show_kw,
        specgram_kw) = args_rep

    # Use data object if provided
    if data is not None:
        #TODO: add time and signal uncertainties to data object
        args.set_default(shots, data.shots)
        args.set_default(diagnostics, data.diagnostics)
        args.set_default(names, data.names)
        args.set_default(times, args.special_object_array(data.times))
        args.set_default(signals, args.special_object_array(data.signals))
        args.set_default(areas, args.special_object_array(data.areas))

    # Define default dict arguments
    args.set_default(plot_color_mesh_or_image_show_kw, {})
    args.set_default(image_show_kw, {})
    args.set_default(specgram_kw, {})

    # Define default boxes
    args.set_default(boxes, np.arange(n_signals))
    boxes = boxes.astype(int)

    # Define default values for variables which depend on area bases
    args.set_default(
        plot_types,
        np.where(
            [area is not None for area in areas], 
            np.where(
                [
                    signal is not None and signal.ndim == 1 
                    for signal in signals], 
                'scatter', 
                'profilogram'),
            np.where(
                [
                    time_err is None and signal_err is None
                    for time_err, signal_err in zip(times_err, signals_err)],
                'trace', 'errorbar')))
    colorbar_mask = np.in1d(plot_types, ['profilogram', 'scatter'])
    y = np.where(colorbar_mask, areas, signals)
    z = np.where(colorbar_mask, signals, None)
    ey = np.where(colorbar_mask, None, signals_err)
    y_mins = np.where(colorbar_mask, area_mins, None)
    y_maxs = np.where(colorbar_mask, area_maxs, None)
    y_relevant_mins = np.where(colorbar_mask, area_relevant_mins, None)
    y_relevant_maxs = np.where(colorbar_mask, area_relevant_maxs, None)
    y_units = np.where(colorbar_mask, area_units, signal_units)
    z_units = np.where(colorbar_mask, signal_units, None)
    y_multipliers = np.where(
        colorbar_mask, area_multipliers, signal_multipliers)

    # Do not plot spectrograms with the standard plotting function
    # (use a special function afterwards)
    specgram_flags = plot_types == 'specgram'
    plot_flags[specgram_flags] = False
    plot_types[specgram_flags] = 'profilogram'

    # Define default labels, title and figure name
    if prefix is not None:
        default_title_prefix = prefix
        fig_name = prefix
    else:
        default_title_prefix = ''
        fig_name = ''
    default_labels_prefixes = np.full(n_signals, '', dtype=object)
    args.set_default(diagnostics, '')
    args.set_default(names, '')
    diagnostic_in_title = False
    if write_shots.any():
        shots_strings = np.where(np.equal(shots, None), '', shots.astype(str))
        if write_shots.all() and all_equal(shots_strings):
            if shots_strings[0] != '':
                default_title_prefix += 'shot {}'.format(shots_strings[0])
                if fig_name != '':
                    fig_name += '_'
                fig_name += '{}'.format(shots_strings[0])
        else:
            default_labels_prefixes[write_shots] += shots_strings[write_shots]
    if fig_name != '':
        fig_name += '_'
    fig_name += '{:.3f}-{:.3f}{}'
    if write_diagnostics.all() and all_equal(diagnostics):
        if diagnostics[0] != '':
            diagnostic_in_title = True
            if default_title_prefix != '':
                default_title_prefix += ', '
            default_title_prefix += diagnostics[0]
            if fig_name != '':
                fig_name += '_'
            fig_name += diagnostics[0]
    else:
        mask = np.logical_and.reduce((
            write_diagnostics, default_labels_prefixes != '',
            diagnostics != ''))
        default_labels_prefixes[mask] += ', '
        default_labels_prefixes[write_diagnostics] += \
            diagnostics[write_diagnostics]
    if write_names.all() and all_equal(names):
        if names[0] != '':
            if default_title_prefix != '':
                default_title_prefix += '/' if diagnostic_in_title else ', '
            default_title_prefix += names[0]
            if fig_name != '':
                fig_name += '_'
            fig_name += names[0]
    else:
        mask = np.logical_and.reduce((
            write_names, default_labels_prefixes != '', names != ''))
        if diagnostic_in_title:
            separators = ', '
        else:
            separators = np.where(diagnostics != '', '/', ', ')[mask]
        default_labels_prefixes[mask] += separators
        default_labels_prefixes[write_names] += names[write_names]
    if title_prefix is None:
        title_prefix = default_title_prefix
    if title_postfix is None:
        title_postfix = ''
    if title is None:
        title = title_prefix + title_postfix
    args.set_default(labels_prefixes, default_labels_prefixes)
    args.set_default(labels_postfixes, '')
    args.set_default(labels, labels_prefixes + labels_postfixes)

    # Define default x title
    if t_units is not None:
        t_title += ' ({})'.format(t_units)

    # Define default frequency units
    if t_units == 's' and t_multiplier == 1:
        default_f_units = np.array(
            [si_units.prefixes.get(mult) for mult in 1 / f_multipliers],
            dtype=object)
        default_f_units[np.not_equal(default_f_units, None)] += 'Hz'
        args.set_default(default_f_units, "")
        args.set_default(f_units, default_f_units)

    # Plot signals
    default_plots_kw = dict(
        plot_types=plot_types, plot_flags=plot_flags, boxes=boxes,
        labels=labels, title=title, x_title=t_title,
        y_mins=y_mins, y_maxs=y_maxs, y_relevant_mins=y_relevant_mins,
        y_relevant_maxs=y_relevant_maxs, y_units=y_units, z_units=z_units,
        sharex=True, x_mins=t_begins, x_maxs=t_ends,
        x_relevant_mins=t_relevant_begins, x_relevant_maxs=t_relevant_ends,
        x_multipliers=t_multiplier, x_offsets=t_offsets, all_x=all_time,
        y_multipliers=y_multipliers, z_multipliers=signal_multipliers,
        use_custom_style=use_custom_style, load_base_style=load_base_style, 
        plot_color_mesh_or_image_show_kw=plot_color_mesh_or_image_show_kw,
        image_show_kw=image_show_kw)
    default_plots_kw.update(plots_kw)
    fig, axes, indices_sort_boxes = plots(
        x=times, y=y, z=z, ex=times_err, ey=ey, order=order,
        return_indices_sort_boxes=True, **default_plots_kw)
    axes_flat = (
        np.array([axes]).flatten({"column":"F", "row":"C"}[order])
        [indices_sort_boxes])

    # Use custom matplotlib style sheets for spectrograms if desired
    remember_style = use_custom_style and specgram_flags.any()
    if remember_style:
        original_style = dict(mpl.rcParams.copy())
        directory = os.path.dirname(__file__)        
        if load_base_style:
            use_base_style(directory)
        plt.style.use(os.path.join(directory, r'color_bar.mplstyle'))

    # Plot spectrograms:
    #TODO: decide which arguments should be available in the mother function
    for i, box, in zip(np.flatnonzero(specgram_flags), boxes[specgram_flags]):
        ax = axes_flat[box]
        try:
            default_image_show_kw = dict(plot_color_mesh_or_image_show_kw[i])
            default_image_show_kw.update(image_show_kw[i])
            plot_specgram(
                signals[i] * signal_multipliers[i], t=times[i], axes=ax,
                units=signal_units[i], f_units=f_units[i], tres_units='ms',
                f_multiplier=f_multipliers[i], t_multiplier=t_multiplier,
                tres_multiplier=1E3, label=labels[i],
                #c_label=c_labels[box],
                t_begin=t_begins[i], t_end=t_ends[i],
                t_relevant_begin=t_relevant_begins[i],
                t_relevant_end=t_relevant_ends[i], t_offset=t_offsets[i],
                fmin=f_mins[i], fmax=f_maxs[i],
                f_relevant_min=f_relevant_mins[i],
                f_relevant_max=f_relevant_maxs[i], f_offset=f_offsets[i],
                write_c_label=write_c_label[i],
                all_time=all_time[i],
                image_show_kw=default_image_show_kw,
                #annotate_kw=legend_annotate_kw[i],
                **specgram_kw[i])
        except Exception as ex:
            #TODO: add original traceback to warning message
            warnings.warn(
                "not plotting signal {} due to {}: {!r}".format(
                    i, type(ex).__name__, ex.args),
                stacklevel=3)
        if write_f_label[i]:
            y_label = "Frequency"
            if f_units[i] is not None and f_units[i] != '':
                y_label += " ({})".format(f_units[i])
            # Explicit size required because of matplotlib bug:
            # https://github.com/matplotlib/matplotlib/issues/25298/
            ax.set_ylabel(y_label, size=mpl.rcParams['axes.labelsize'])
    
    # Load back the original style if custom styles were used
    if remember_style:
        mpl.rcParams.update(original_style)

    # Return figure, axes and figure name if desired
    if return_plot:
        if return_fig_name:
            x_lims = [ax.get_xlim() for ax in axes_flat]
            t_begin_fig_name = np.min(x_lims)
            t_end_fig_name = np.max(x_lims)
            if t_units is None:
                t_units = ''
            fig_name = fig_name.format(
                t_begin_fig_name, t_end_fig_name, t_units)
            if postfixes is None:
                postfixes = sys._getframe().f_code.co_name
            if not np.array_equiv(postfixes, ''):
                fig_name += "_{}".format(
                    "_".join(np.atleast_1d(postfixes)))
            if endfix is None and np.array_equiv(plot_types, 'specgram'):
                endfix = "specgram"
                if n_signals > 1:
                    endfix += "s"
            if endfix is not None and endfix != "":
                fig_name += "_{}".format(endfix)
            return fig, axes, fig_name
        return fig, axes

#TODO: allow a different time base for each signal
#TODO: allow different shots
#TODO: clean code
def plot_coherogram(
        x=None, y=None, t=None, shot=None, diagnostics=None, names=None,
        data=None, t_multiplier=1, t_units=None, t_begin=None, t_end=None,
        c_labels=None, signal_multipliers=1, signal_units='',
        signal_names=None, signal_legends=None, f_multiplier=None,
        f_units=None, fmin=None, fmax=None, tres_multiplier=None,
        tres_units=None, confidence=0.95, cmax=None, threshold=None,
        min_alpha=0.05, slope=None, use_alpha=True, all_time=False,
        phase_colorbar=True, coherogram_kw=None, annotate_kw=None,
        plot_color_mesh_kw=None, **plot_signals_kw):
    #TODO: doctring
    """Plot the magnitude squared coherence, cross phase and power spectra of
    two signals as a function of time."""

    # Use data object if provided
    if data is not None:
        if np.all(data.shots == data.shots[0]):
            shot = data.shots[0]
        else:
            shot = None
        diagnostics = data.diagnostics
        names = data.names
        t = data.times[0]
        x, y = data.signals

    # Extend arguments to both signals and fill the rest with None
    n_signals = 2
    n_all_signals = 4
    vars = np.atleast_1d(
        diagnostics, names, c_labels, signal_multipliers, signal_units,
        signal_names, signal_legends, plot_color_mesh_kw)
    for i, var in enumerate(vars):
        if len(var) != n_all_signals:
            vars[i] = np.concatenate((
                args.extend_args(var, n=n_signals)[0],
                (n_all_signals - n_signals) * [None]))
    (
        diagnostics, names, c_labels, signal_multipliers, signal_units,
        signal_names, signal_legends, plot_color_mesh_kw) = vars

    # Define default variables
    args.set_default(signal_multipliers, 1)
    args.set_default(signal_units, 'rad', indices=3)
    args.set_default(signal_units, '')
    args.set_default(plot_color_mesh_kw, {}, copy=True)
    if coherogram_kw is None: 
        coherogram_kw = {}
    if annotate_kw is None:
        annotate_kw = {}
    if t_units is None and t is not None:
        t_units = 's'
    if t_units == 's' and t_multiplier == 1:
        if f_multiplier is None:
            f_multiplier = 1E-3
            if fmin is not None:
                fmin *= f_multiplier
            if fmax is not None:
                fmax *= f_multiplier
        if f_units is None:
            f_units = si_units.prefixes.get(1 / f_multiplier)
            if f_units is not None:
                f_units += 'Hz'
        if tres_multiplier is None:
            tres_multiplier = 1E3
        if tres_units is None:
            tres_units = si_units.prefixes.get(1 / tres_multiplier)
            if tres_units is not None:
                tres_units += t_units
    if f_multiplier is None:
        f_multiplier = 1
    y_title_common = 'Frequency'
    if f_units is not None:
        y_title_common += ' ({})'.format(f_units)
    c_label_x, c_label_y = (
        specgram_c_label(None, units, f_units) for units in signal_units[:2])
    if c_label_x == c_label_y:
        c_title_common = c_label_x
        c_title_common_indices = [0,1]
        c_label_x = c_label_y = None
    else:
        c_title_common = None
        c_title_common_indices = None
    args.set_default(
        c_labels,
        [
            c_label_x, c_label_y, r'$\left | Coherence \right |^{2}$',
            'Cross-phase'])

    # Unapply multipliers to time and frequency ranges
    if t_begin is not None:
        t_begin /= t_multiplier
    if t_end is not None:
        t_end /= t_multiplier
    if fmin is not None:
        fmin /= f_multiplier
    if fmax is not None:
        fmax /= f_multiplier

    # Compute coherogram
    default_coherogram_kw = dict(
        ntpixels=355, nfpixels=142, t_begin=t_begin, t_end=t_end, fmin=fmin,
        fmax=fmax, all_time=all_time)
    default_coherogram_kw.update(coherogram_kw)
    f, tseg, cxy, phaxy, sxx, syy, tres, fres, fmin, fmax, fs, k = coherogram(
        x, y, t, return_all=True, **default_coherogram_kw)

    # Reapply multipliers to time and frequency ranges
    if t_begin is not None:
        t_begin *= t_multiplier
    if t_end is not None:
        t_end *= t_multiplier
    fmin *= f_multiplier
    fmax *= f_multiplier

    # Compute significance threshold and transparency
    if threshold is None:
        threshold = 1 - (1 - confidence) ** (1 / (k - 1))
    if slope is None:
        slope = np.log(1 / min_alpha - 1) / (0.5 - np.abs(0.5 - threshold))
    alpha = None if not use_alpha else (
        1 / (1 + np.exp(slope * (threshold - cxy))))

    # Define maximum value of coherence color scale
    if cmax is None:
        cxymax = cxy.max()
        cmax = 1 if cxymax > 0.9 else cxymax

    # Define default color mesh plot settings
    default_plot_color_mesh_kw = (
        2 * [default_specgram_image_show_kw] +
        [dict(vmin=0, vmax=cmax), dict(alpha=alpha)])
    for d, kw in zip(plot_color_mesh_kw, default_plot_color_mesh_kw):
        d.update(kw)
    if phase_colorbar:
        plot_color_mesh_kw[3].update(dict(
            cmap=cyclic_bmryb_cmap, vmin=-np.pi, vmax=np.pi,
            colorbar_kw=dict(ticks=[-np.pi, -np.pi/2, 0, np.pi/2, np.pi])))

    # Plot coherogram
    default_plot_signals_kw = dict(
        times=tseg,
        signals=[sxx.T / f_multiplier, syy.T / f_multiplier, cxy.T, phaxy.T],
        areas=f, shots=shot, diagnostics=diagnostics, names=names,
        t_multiplier=t_multiplier, t_units=t_units, t_begins=t_begin,
        t_ends=t_end, postfixes=sys._getframe().f_code.co_name,
        signal_multipliers=signal_multipliers, signal_units=signal_units,
        labels=signal_names, area_multipliers=f_multiplier,
        area_mins=fmin, area_maxs=fmax,
        z_labels=c_labels,
        y_title=y_title_common,
        z_title=c_title_common,
        z_title_indices=c_title_common_indices,
        #area_units=f_units,
        # c_labels=c_labels,
        # y_title_common=y_title_common,
        # c_title_common=c_title_common,
        # c_title_common_indices=c_title_common_indices,
        all_time=all_time,
        sharey=True, plot_color_mesh_or_image_show_kw=plot_color_mesh_kw)
    default_plot_signals_kw.update(plot_signals_kw)
    result = plot_signals(**default_plot_signals_kw)

    # Write phase colorbar tick labels as fractions of pi
    if phase_colorbar:
        result[0].axes[-1].set_yticklabels(
            [r'$-\pi$', r'$-\pi$/2', '$0$', r'$\pi$/2', r'$\pi$'])

    # Mark signficance threshold on the coherence colorbar
    coherence_colorbar = result[0].axes[-2]
    color = 'cyan'
    threshold_norm = threshold / cmax
    coherence_colorbar.axhline(
        y=threshold_norm, zorder=3, clip_on=False, xmin=-0.3,
        xmax=1.3, color=color, linewidth=2)
    coherence_colorbar.annotate(
        text=int(confidence*100), xy=(0.5, threshold_norm), ha="center",
        xycoords='axes fraction', va='top', xytext=(0, -3),
        textcoords="offset points", fontsize=10, color=color)

    # Write time and frequency resolutions
    default_annotate_kw = dict(xytext=(3, -13))
    default_annotate_kw.update(annotate_kw)
    write_ftres_label(
        fres * f_multiplier, tres * tres_multiplier, f_units, tres_units,
        axes=result[1][0], **default_annotate_kw)

    return result
