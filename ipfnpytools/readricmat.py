"""
READRICMAT allows to read *.mat processed profile files from RICG diagnostic.

Note: the script should be run at the directory, where *.mat files are
      stored

As an output the of 'readmat' function program returns a dictionary with 
information about the shot. Note some useful parameters 
(see https://www.aug.ipp.mpg.de/cgibin/sfread_only/isis):

     times: time base, equivalent to TIME in RIC aug database
     rhop:  area base, equivalent to RhoAnt* in RIC aug database
     ne: electron density, equivalent to Ne_Ant* in RIC aug database

One can access it by using dictionary notation,
    e.g.: mat["ne"][100,:] will return the density profile for the 
          time instance mat["time"][100]
Author:
    Egor Seliunin
"""
from __future__ import print_function

from builtins import str
import scipy.io

def readmat(shot, antenna, version=None):
    """
    Reads the processed RICG profile files *.mat.

    Inputs:
        shot: int,
              shot number
        antenna: int,
                 antenna number
        version: int,
                 processed virsion

    Output:
        mat: dict,
             dictionary with all informations from the processed file
    """

    # Create the path

    shot = str(shot)
    antenna = str(antenna)

    if version is None:
        version = ""
    else:
        version = "_v" + str(version)

    fname = "ric_" + shot + "_ant" + antenna + version + ".mat"

    mat = scipy.io.loadmat(fname)

    return mat


if __name__ == "__main__":
    shot = 34290
    antenna = 1
    ver = 2

    mat = readmat(shot, antenna, version=ver)
    print(list(mat.keys()))
    pass
