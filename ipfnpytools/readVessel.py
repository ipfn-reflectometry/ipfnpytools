from itertools import groupby
import numpy as np

array_list = []

def readVessel(file):
    with open(file) as f_data:    
        for k, g in groupby(f_data, lambda x: x.startswith("\n")):
            if not k:
                array_list.append(np.array([[float(x) for x in d.split()] for d in g if len(d.strip())]))
    return array_list

#arr = readVessel('vessel.txt')

#for entry in arr:
#    plt.plot(entry[:,0], entry[:,1], color='k')


