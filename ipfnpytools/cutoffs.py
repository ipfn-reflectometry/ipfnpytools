#!python
"""cutoffs calculates the cutoff frequencies for different waves
propagating along a plasma with a given shape

The cutoff calculation can be used and an example can be run by
calling this function as

Examples
-------
$ python cutoffs.py --help
$ python cutoffs.py


Authors
-------
L. Guimarais
D. Aguiam

"""
from __future__ import print_function
from __future__ import division
from builtins import str
__version__ = "1.0"



import numpy as np
import matplotlib.pylab as plt
import refpy

if __name__ == "__main__":
    import argparse

    # Parses the arguments passed to the script
    parser = argparse.ArgumentParser(
            description='Plots cut off frequencies for different propagation modes',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            )

    parser.add_argument('-s','--shape',
                        help='Shape of the density profile [m n]',
                        default=[2,5],
                        nargs=2,
                        type=float)
    parser.add_argument('-ne','--density',
                        help='Core electron density of the plasma in m-3',
                        default=5e19,
                        type=float)
    parser.add_argument('-edges','--edges',
                        help='Start and stop edges of the density profile in m. Rgeo is calculated from middle point',
                        default=[1.07,2.23],
                        nargs=2,
                        type=float)

    parser.add_argument('-b','--bfield',
                        help='Magnetic field in core in T',
                        default=2.5,
                        type=float)

    parser.add_argument('-r','--radius',
                        help='Calculate cut offs between these radial positions',
                        default=[1, 2.3],
                        nargs=2,
                        type=float)

    parser.add_argument('-pts','--points',
                        help='Number of points along profile',
                        default=200,
                        type=int)


    args = parser.parse_args()





    m, n = args.shape
    ne_core = args.density
    edge_start,edge_stop = args.edges
    Bzero = args.bfield
    print(Bzero)
    print(ne_core)

    # Radial limits to interpolate
    rlims = args.radius
    #Number of points to interpolate
    npts = args.points

    #Get some machine coordinates
    Rgeo = (edge_start+edge_stop)/2

    #Radial position
    radius = np.linspace(rlims[0], rlims[1], npts)


    radius, bfield = refpy.generation.create_magnetic_profile(radius,Bzero,r0=Rgeo)
    radius, density = refpy.generation.create_density_profile(radius,ne_core,
                                        edge_start=edge_start,
                                        edge_stop=edge_stop)

    Fuc = refpy.cutoffs.calc_cuttoff_xmode(density, bfield, region='upper')
    Flc = refpy.cutoffs.calc_cuttoff_xmode(density, bfield, region='lower')
    Fco = refpy.cutoffs.calc_cuttoff_omode(density)


    Fpe = refpy.calc_ne2fpe(density)
    Fce = refpy.calc_b2fce(bfield)

    plt.grid()
    plt.xlabel('Radius [m]')
    plt.ylabel('Frequency [GHz]')
    titlestr = 'Density=' + str(ne_core) + '[m-3] Bt=' + str(Bzero) + '[T] m=' + str(m) + ' n=' + str(n)

    titlestr = 'Density %0.1g [m-3] B %0.1f [T] '%(ne_core,Bzero)
    plt.title(titlestr)
    plt.plot(radius, Fce*1e-9, label='Cyclotronic',linestyle='--')
    plt.plot(radius, Fco*1e-9, label='Plasma (O-mode)')
    plt.plot(radius, Fuc*1e-9, label='Upper (X-mode)')
    plt.plot(radius, Flc*1e-9, label='Lower (X-Mode)')
    plt.legend(fontsize='x-small')
    plt.show()
