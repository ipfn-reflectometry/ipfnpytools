#!/usr/bin/python
from __future__ import print_function
from builtins import range
from ctypes import *

# --- Import libkk library
if sizeof(c_long) == 8:
  libkkso = "/afs/ipp/aug/ads/lib64/@sys/libkk8.so"
else:
  libkkso = "/afs/ipp/aug/ads/lib/@sys/libkk.so"
libkk = cdll.LoadLibrary(libkkso)

error = c_int(0)
exp   = create_string_buffer('AUGD')
diag  = create_string_buffer('EQI')
shot  = c_int(28848)
edition = c_int(0)
error   = c_int(0)

# --- Get necessary dimensions

ndgc   = c_int(0)
ndit   = c_int(0)
ndim   = c_int(0)
nvalid = c_int(0)

libkk.kkgcdimensions(byref(error), exp, diag, shot, byref(edition), byref(shot), byref(ndgc), byref(ndit), byref(ndim), byref(nvalid) ); 

# --- Read the coordinates

xygcs= (((c_float*ndgc.value)*ndit.value)*2)()
ngc  = c_int(0)
lenix= (c_int*ndgc.value)()
libkk.kkgcread(byref(error), exp, diag, shot, byref(edition), ndgc, ndit, byref(xygcs), byref(ngc), byref(lenix))

# --- Print the coordinates

for igc in range (0, ngc.value-1):

    for i in range (0, lenix[igc] - 1):
	print(xygcs[0][i][igc], ' ', xygcs[1][i][igc])

    print(xygcs[0][0][igc], ' ', xygcs[1][0][igc])	# Close structure
    print()
