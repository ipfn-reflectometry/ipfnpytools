"""Baseline estimation algorithms.

The MIT License (MIT)

Copyright (c) 2014 Lucas Hermann Negri

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
"""

import numpy as np
import scipy.linalg as la
import math
from .getsig import getsig
from dataclasses import dataclass


def ceiling(x, deg=3, max_it=100, tol=1e-3):
    """Computes the ceiling of a given data.
    Iteratively performs a polynomial fitting in the data to detect its
    ceiling. At every iteration, the fitting weights on the regions with
    valleys are reduced to identify the ceiling only.
    Parameters
    ----------
    x : nd array
        Data to detect the ceiling.
    deg : int
        Degree of the polynomial that will estimate the data ceiling. A low
        degree may fail to detect all the ceiling present, while a high
        degree may make the data too oscillatory, especially at the edges.
    max_it : int
        Maximum number of iterations to perform.
    tol : float
        Tolerance to use when comparing the difference between the current
        fit coefficient and the ones from the last iteration. The iteration
        procedure will stop when the difference between them is lower than
        *tol*.
    Returns
    -------
    ceil: nd array
        Ceiling amplitude for every original point in *x*. If *deg* is one, all values in *x* are equal.

    Note:
        This function is a wrapper around *baseline*.
        A degree of zero is the same as computing the maximum of *x*.
    """

    ceil = -1.0 * baseline(-1.0 * x, deg=deg, max_it=max_it, tol=tol)

    return ceil


def baseline(x, deg=3, max_it=100, tol=1e-3):
    """Computes the baseline of a given data.
    Iteratively performs a polynomial fitting in the data to detect its
    baseline. At every iteration, the fitting weights on the regions with
    peaks are reduced to identify the baseline only.
    Parameters
    ----------
    x : nd array
        Data to detect the baseline.
    deg : int
        Degree of the polynomial that will estimate the data baseline. A low
        degree may fail to detect all the baseline present, while a high
        degree may make the data too oscillatory, especially at the edges.
    max_it : int
        Maximum number of iterations to perform.
    tol : float
        Tolerance to use when comparing the difference between the current
        fit coefficient and the ones from the last iteration. The iteration
        procedure will stop when the difference between them is lower than
        *tol*.
    Returns
    -------
    base: nd array
        Baseline amplitude for every original point in *x*. If *deg* is one, all values in *x* are equal.

    Note:
        To determine the ceiling of a signal pass it negated to this function, and then negate the output.
        You can also use the wrapper function *ceiling* that does exactly this.
        A degree of zero is the same as computing the minimum of *x*.

    """

    order = deg + 1
    coefficients = np.ones(order)
    
    y = np.array(x)

    # Avoid numerical issues: too large numbers
    # cond = math.pow(y.max(), 1. / (np.ceil(order) // 2 * 2 + 1))  # Odd root
    # t = np.linspace(0., cond, y.size)
    t = np.linspace(-1, 1, y.size)
    base = y.copy()

    vander = np.vander(t, order)
    vander_pinv = la.pinv(vander)

    for _ in range(max_it):
        coefficients_new = np.dot(vander_pinv, y)

        if la.norm(coefficients_new - coefficients) / la.norm(coefficients) < tol:
            break

        coefficients = coefficients_new
        base = np.dot(vander, coefficients)
        y = np.minimum(y, base)

    return base


def current_flattop(shot):
    """Estimate the current flattop for a given shot.
    
    Parameters
    ----------
    shot: int
        Shot number, dah.
        
    Returns
    -------
    [t0, t1]: float
        Beginig and end of the estimated flattop time interval.
    """

    @dataclass
    class Signal:
        time: np.ndarray = 0
        data: np.ndarray = 0
    
    current = Signal()
    
    current.data = getsig(shot, 'MAG', 'Ipa')
    current.time = getsig(shot, 'MAG', 'T-MAG-1')

    try:  # Everything should work unless the shot is in reversed current
        # First stage: exclude anything below 0.5 MA
        idx = current.data > 500000
        current.time = current.time[idx]
        current.data = current.data[idx]

        # Fit the 1st stage with a zero degree polynomial (with a twist)
        flattop = ceiling(current.data, deg=0)

    except ValueError:
        current.data = getsig(shot, 'MAG', 'Ipa')
        current.time = getsig(shot, 'MAG', 'T-MAG-1')
        
         # First stage: exclude anything below 0.5 MA
        idx = current.data < -500000
        current.time = current.time[idx]
        current.data = current.data[idx]

        # Fit the 1st stage with a zero degree polynomial (with a twist)
        flattop = baseline(current.data, deg=0)

    # Second stage: only keep points within 25kA of the flattop
    idx = np.abs(flattop - current.data) < 25000
    current.time = current.time[idx]
    current.data = current.data[idx]

    # Third stage: reduce the obtained time interval by 5% just because
    time_window = current.time[-1] - current.time[0]
    gap = time_window * 0.025
    idx = (current.time > (current.time[0] + gap)) & (current.time < (current.time[-1] - gap))
    current.time = current.time[idx]
    current.data = current.data[idx]

    return [current.time[0], current.time[-1]]