"""
Save a figure to the current working directory or directory of the invoked
script.
"""
from __future__ import print_function

import sys
import os
import matplotlib.pyplot as plt


#TODO: example
def save_figure(
        fig=None, name="figure", directory=None, verbose=True, **savefig_kw):
    """Save a figure to the current working directory or directory of the
    invoked script.

    This is basically the matplotlib.pyplot.savefig routine with added
    features and automation.

    Note: It works even when the script is executed in Notepad++.

    Parameters
    ----------
    fig: matplotlib Figure
        Figure to save. Defaults to current figure.
    name: str
        Filename (without extension) of the saved figure.
    directory: str
        Path of the directory the figure is saved in. Defaults to the
        directory of the invoked script.
    verbose: bool
        If True, print the full path of the saved figure.
    **savefig_kw:
        All additional keyword arguments are passed to the savefig() call.

    Returns
    -------
    None

    Examples
    --------

    """

    # Set current figure as default
    if fig is None:
        fig = plt.gcf()

    # Set directory of the invoked script as default
    if directory is None:
        directory = os.getcwd()
        if os.path.basename(directory) == "Notepad++":
            directory = os.path.dirname(sys.argv[0])

    # Create the directory if it does not exist
    if not os.path.exists(directory):
        os.makedirs(directory)

    # Save the figure
    format = savefig_kw.setdefault("format", plt.rcParams['savefig.format'])
    full_path = os.path.abspath(
        os.path.join(directory, "{}.{}".format(name, format)))
    fig.savefig(full_path, **savefig_kw)
    if verbose:
        print("Figure saved to {}".format(full_path))
