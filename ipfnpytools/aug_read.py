# TODO: docstring
"""
Functions to read data and settings of AUG diagnostics.
"""

import warnings
from collections import namedtuple
from itertools import count
import numpy as np
from . import arguments as args
from .find_first import find_first
from .fpe_to_ne import fpe_to_ne
import aug_sfutils as sf


def open_shotfile_default(
        shotfile=None, diagnostics=None, shot=None, experiment=None,
        edition=None, default_shot=0, default_experiment="AUGD",
        default_edition=0, shot_bins=None, return_index=None):
    # TODO: docstring
    """

    Parameters
    ----------
    shotfile
    diagnostics
    shot
    experiment
    edition
    default_shot
    default_experiment
    default_edition
    shot_bins
    return_index

    Returns
    -------

    Examples
    --------

    """

    # Define default keywords
    if return_index is None:
        if shot_bins is None:
            return_index = False
        else:
            return_index = True
    if isinstance(diagnostics, str):
        diagnostics = [diagnostics]

    # If shotfile is not given as input, open it
    if shotfile is None:
        if shot is None:
            shot = default_shot
        if shot == 0:
            shot = sf.getlastshot()
        if experiment is None:
            experiment = default_experiment
        if edition is None:
            edition = default_edition
        if shot_bins is None:
            shot_bins = [shot + 1]
        diagnostic_index = np.digitize(shot, shot_bins)
        diagnostic = diagnostics[diagnostic_index]
        if (edition == 0) and (experiment == 'AUGD'):
            shotfile = sf.SFREAD(shot, diagnostic)
        else:
            shotfile = sf.SFREAD(sf=sf.manage_ed.sf_path(
                shot, diagnostic, exp=experiment, ed=edition)[0])

    # Otherwise check if it is consistent with the other inputs
    # TODO: improve error messages
    else:
        if shot is not None:
            if shot == 0:
                shot = sf.getlastshot()
            if shot != shotfile.shot:
                raise ValueError(
                    "input shot different from the one in input shotfile")
        if experiment is not None:
            if experiment != shotfile.exp:
                raise ValueError(
                    "input experiment different from the one in input "
                    "shotfile")
        if edition is not None:
            if edition != shotfile.ed:
                raise ValueError(
                    "input edition different from the one in input shotfile")
        if shot_bins is None:
            shot_bins = shotfile.shot + 1
            diagnostic_index = np.digitize(shotfile.shot, shot_bins)
        if diagnostics is not None:
            diagnostic = diagnostics[diagnostic_index]
            if diagnostic != shotfile.diag:
                raise ValueError(
                    "input diagnostic different from the one in input "
                    "shotfile")

    # Return shotfile and diagnostic index if required
    if return_index:
        return shotfile, diagnostic_index
    else:
        return shotfile


def to_open_read_close(
        diagnostics=None, names=None, shots=None, experiments=None,
        editions=None, t_begins=None, t_ends=None, signals_to_read=True):
    # TODO: docstring
    """Decide which shotfiles to open and close and which signals to read
    without unnecessary repetitions.

    Parameters
    ----------
    diagnostics
    names
    shots
    experiments
    editions
    t_begins
    t_ends
    signals_to_read

    Returns
    -------

    """

    # Determine the total number of signals and replicate arguments as numpy
    # arrays
    args_rep = [
        diagnostics, names, shots, experiments, editions, t_begins, t_ends,
        signals_to_read]
    args.listify(args_rep)
    n_signals = max(map(len, args_rep))
    args.replicate_elements(args_rep, n_signals)
    args.arrayfy(args_rep)
    (
        diagnostics, names, shots, experiments, editions, t_begins, t_ends,
        signals_to_read) = args_rep

    # Determine indices of the signals intented to be read
    signals_to_read_indices = np.flatnonzero(signals_to_read)

    # Determine which shotfiles to open
    open_shotfiles = np.logical_and(signals_to_read, np.any(
        (
            diagnostics != np.roll(diagnostics, 1),
            shots != np.roll(shots, 1),
            experiments != np.roll(experiments, 1),
            editions != np.roll(editions, 1),
            t_begins != np.roll(t_begins, 1),
            t_ends != np.roll(t_ends, 1)),
        axis = 0))
    if len(signals_to_read_indices) != 0:
        open_shotfiles[signals_to_read_indices[0]] = True

    # Determine which signals to read
    read_signals = np.full(n_signals, False, dtype=bool)
    read_signals[signals_to_read] = np.logical_or(
        open_shotfiles[signals_to_read],
        names[signals_to_read] != np.roll(names[signals_to_read], 1))

    # Determine which shotfiles to close
    close_shotfiles = np.full(n_signals, False, dtype=bool)
    close_shotfiles[signals_to_read] = np.roll(
        open_shotfiles[signals_to_read], -1)
    if len(signals_to_read_indices) != 0:
        close_shotfiles[signals_to_read_indices[-1]] = True

    # Return decisions
    return open_shotfiles, read_signals, close_shotfiles


def _warning_message(diagnostic, signal, exception):
    # TODO: docstring
    """Print a warning message if a signal is not read due to an error
    occurred when reading it or opening its shotfile."""

    warnings.warn(
        "not reading signal {}/{} due to {}: {!r}".format(
            diagnostic, signal, type(exception).__name__, exception.args),
        stacklevel=4)


# Named tuple to be returned by many_signals()
_many_signals_output = namedtuple(
    "aug_read_many_signals_output",
    [
        "shots", "diagnostics", "names", "experiments", "editions",
        "areas", "signals", "times"])


# TODO: allow negative shot numbers
# TODO: think of (and add function/method?) an easy way of getting specific signals or other elements from the output of many_signals(), given the signal name, for example
# TODO: read units
def many_signals(
        diagnostics=None, names=None, shots=0, experiments='AUGD',
        editions=0, t_begins=None, t_ends=None, channels=None):
    # TODO: docstring
    """Read several AUG signals without unnecessary repetitions.

    Parameters
    ----------
    diagnostics
    names
    shots
    experiments
    editions
    t_begins
    t_ends
    channels

    Returns
    -------

    """

    # Determine the total number of signals and replicate arguments as numpy
    # arrays
    args_rep = [
        diagnostics, names, shots, experiments, editions, t_begins, t_ends,
        channels]
    args.listify(args_rep)
    n_signals = max(map(len, args_rep))
    args.replicate_elements(args_rep, n_signals)
    args.arrayfy(args_rep)
    (
        diagnostics, names, shots, experiments, editions, t_begins, t_ends,
        channels) = args_rep

    # Define default value for shot numbers (last shot number)
    indices = shots == 0
    if np.any(indices):
#         shots[indices] = dd.getLastAUGShotNumber()
        shots[indices] = sf.getlastshot()

    # Add channels to signals
    indices = np.not_equal(channels, None)
    names[indices] += [str(channel) for channel in channels[indices]]

    # Decide which shotfiles to open and close and which signals to read
    # without unnecessary repetitions
    open_shotfile, read_signal, close_shotfile = to_open_read_close(
        diagnostics=diagnostics, names=names, shots=shots,
        experiments=experiments, editions=editions, t_begins=t_begins,
        t_ends=t_ends)

    # For all signals
    times, signals, areas = ([None] * n_signals for i in range(3))
    for i, shot, diag, name, experiment, edition, t_begin, t_end, in zip(
            count(), shots, diagnostics, names, experiments, editions,
            t_begins, t_ends):

        # Open shotfile if needed
        try:
            if open_shotfile[i]:
                #.item() is a workaround because of a bug in SFREAD that makes it not handle numpy types properly
                shotfile = sf.SFREAD(
                    shot.item(), diag, exp=experiment, ed=edition)
                # if (edition == 0) and (experiment == 'AUGD'):
                #     shotfile = sf.SFREAD(shot, diag)
                # else:
                #     shotfile = sf.SFREAD(sf=sf.manage_ed.sf_path(
                #         shot, diag, exp=experiment, ed=edition)[0])
            elif error_shotfile:
                raise ex_shotfile
        except Exception as ex_shotfile:
            error_shotfile = True
            _warning_message(diag, name, ex_shotfile)

        # Read data if needed
        else:
            error_shotfile = False
            try:
                if read_signal[i]:
                    signal = shotfile.getobject(name, tbeg=t_begin, tend=t_end)
                elif error_signal:
                    raise ex_signal
            except Exception as ex_signal:
                error_signal = True
                _warning_message(diag, name, ex_signal)
            else:
                error_signal = False
                times[i] = shotfile.gettimebase(name, tbeg=t_begin, tend=t_end)
                signals[i] = signal
                areas[i] = shotfile.getareabase(name, tbeg=t_begin, tend=t_end)

            # Close shotfile if needed
            finally:
                if close_shotfile[i]:
#                     shotfile.close()
                    pass

    # Return data
    return _many_signals_output(
        shots, diagnostics, names, experiments, editions, areas,
        signals, times)


#TODO: read hopping frequencies correctly
def fmcw_settings(
        shot=None, shotfile=None, experiment=None, edition=None,
        close_shotfile=True, print_settings=False):
    # TODO: docstring
    """Read settings and names of FMCW reflectometer in FF."""

    # Print all warnings, even if repeated
    warnings.simplefilter('always', UserWarning)

    # Open shotfile if required
    shotfile, diag_index = open_shotfile_default(
        shotfile=shotfile, diagnostics=["REF", "RFL"], shot=shot,
        experiment=experiment, edition=edition, shot_bins=[23494])

    # Get general shotfile data
    shot = shotfile.shot
    diagnostic = shotfile.diag
    edition = shotfile.ed
    experiment = shotfile.exp

    # Define parameter names
    bands_HFS = ['K', 'Ka', 'Q', 'V']
    if diag_index == 0:
        bands_LFS = ['K', 'Ka', 'Q', 'V', 'W']
        time_base_name = "T-BASE"
        gain_set_name = "GAINS"
        gain_char = ''
        signals_LFS = [band + 'Low' for band in bands_LFS]
        signals_HFS = [band + 'High' for band in bands_HFS]
    else:
        bands_LFS = ['K', 'Ka', 'Q', 'V']
        time_base_name = "T-Sio"
        gain_set_name = "REFgain"
        gain_char = '-'
        signals_LFS = [band + '-LFS' for band in bands_LFS]
        signals_HFS = [band + '-HFS' for band in bands_HFS]

    # Read probing frequencies
    if diag_index == 0:
        default_f_probings_GHz = None
    else:
        freq_set = shotfile("REFfreq")
        if shotfile.shot <= 31776:
            default_f_probings_GHz = [
                freq_set[band + "-LFS"] for band in bands_LFS]
        elif shotfile.shot <= 33723:
            default_f_probings_GHz = [
                freq_set[band] for band in bands_LFS]
        else:
            default_f_probings_GHz = [
                freq_set[band][0] for band in bands_LFS]

    # Correct probing frequencies
    f_probings_GHz = {
        21379: [20., 35., 49., 59.9, 72.],
        21380: [20., 35., 49., 59.9, 72.],
        21381: [20., 35., 49., 59.9, 72.],
        21382: [20., 35., 49., 59.9, 72.],
        21383: [20., 35., 49., 59.9, 72.],
        21384: [20., 35., 49., 59.9, 72.],
        21385: [20., 35., 49., 59.9, 72.],
        21386: [20., 35., 49., 59.9, 72.],
        21387: [20., 35., 49., 59.9, 72.],
        21388: [20., 35., 49., 59.9, 72.],
        21389: [20., 35., 49., 59.9, 72.],
        21390: [20., 35., 49., 59.9, 72.],
        21391: [20., 35., 49., 59.9, 72.],
        21392: [20., 35., 49., 59.9, 72.],
        21393: [20., 35., 49., 59.9, 72.],
        21394: [20., 35., 49., 59.9, 72.],
        21395: [20., 35., 49., 59.9, 72.],
        21396: [20., 35., 49., 59.9, 72.],
        21397: [20., 35., 49., 59.9, 72.],
        21416: [20., 34.7, 40.1, 56, 72.],
        21417: [20., 34.7, 40.1, 56, 72.],
        21418: [20., 34.7, 40.1, 56, 72.],
        21419: [20., 34.7, 40.1, 56, 72.],
        31975: [24., 36., 44., 55.],
        35930: [25., 37., 45., 55.],
    }.get(shotfile.shot, default_f_probings_GHz)
    if f_probings_GHz is None:
        f_probings_GHz = [None] * len(bands_LFS)
        f_probings_Hz = f_probings_GHz[:]
        warnings.warn(
            (
                "probing frequencies must be read in the physical logbook "
                "because in the past they were not written in the shofile"),
            stacklevel=2)
    else:
        if f_probings_GHz == default_f_probings_GHz:
            warnings.warn(
                (
                    "probing frequencies read directly from shofile (some "
                    "shotfiles may have mistakes)"),
                stacklevel=2)
        f_probings_Hz = [f * 1E9 for f in f_probings_GHz]

    # Read gains
    gain_set = shotfile(gain_set_name)
    gains_LFS = [gain_set[band + gain_char + 'LFS'] for band in bands_LFS]
    gains_HFS = [gain_set[band + gain_char + 'HFS'] for band in bands_HFS]
    if diag_index == 0:
        gains_HFS.append(None)

    # Read time of first and last samples and compute sampling frequency
#    time_base_info = shotfile.getTimeBaseInfo(time_base_name)
#    t_first_s = time_base_info.tBegin
#    t_last_s = time_base_info.tEnd
#    f_sampling_Hz = (time_base_info.ntVal - 1) / (t_last_s - t_first_s)
    time_base = shotfile(time_base_name)
    t_first_s = time_base[0]
    t_last_s = time_base[-1]
    f_sampling_Hz = (len(time_base) - 1) / (t_last_s - t_first_s)

    # Print settings
    if print_settings:
        print("\n {} FMCW REFLECTOMETER SETTINGS - shot #{}, {}, edition "
              "{}\n".format(diagnostic, shot, experiment, edition))
        print(" time range: {} to {} s".format(t_first_s, t_last_s))
        print(" sampling frequency: {} MHz\n".format(f_sampling_Hz / 1E6))
        headers = ["band","probing frequency (GHz)","LFS gain","HFS gain"]
        column_string = ("{{:^{}}}"*len(headers)).format(
            *[len(x) + 2 for x in headers])
        print(column_string.format(*headers))
        for index, band in enumerate(bands_LFS):
            print(column_string.format(
                band, f_probings_GHz[index], gains_LFS[index],
                gains_HFS[index]))
        print("")

    # Close shotfile
    if close_shotfile:
#        shotfile.close()
        pass

    # Return data
    return (
        shot, diagnostic, edition, experiment, bands_LFS, bands_HFS,
        f_probings_Hz, gains_LFS, gains_HFS, t_first_s, t_last_s,
        f_sampling_Hz, signals_LFS, signals_HFS)


def fmcw_data(
        t_begin=None, t_end=None, LFS=True, HFS=True, **read_settings_kw):
    # TODO: docstring
    """Read data of FMCW reflectometer in FF."""

    # Read reflectometer settings and names
    (
        shot, diagnostic, edition, experiment, bands_LFS, bands_HFS,
        f_probings_Hz, gains_LFS, gains_HFS, t_first_s, t_last_s,
        f_sampling_Hz, signals_LFS, signals_HFS) = fmcw_settings(
        **read_settings_kw)

    # Read LFS data
    LFS_data = None if not LFS else many_signals(
        diagnostics=diagnostic, names=signals_LFS, shots=shot,
        experiments=experiment, editions=edition, t_begins=t_begin,
        t_ends=t_end)

    # Read HFS data
    HFS_data = None if not HFS else many_signals(
        diagnostics=diagnostic, names=signals_HFS, shots=shot,
        experiments=experiment, editions=edition, t_begins=t_begin,
        t_ends=t_end)

    # Return data, settings and names
    return (
        LFS_data, HFS_data, shot, diagnostic, edition, experiment, bands_LFS,
        bands_HFS, f_probings_Hz, gains_LFS, gains_HFS, t_first_s, t_last_s,
        f_sampling_Hz)


def hop_settings(
        shot=None, shotfile=None, experiment=None, edition=None,
        close_shotfile=True, print_settings=False):
    # TODO: docstring
    """Read settings of hopping reflectometer.

    Parameters
    ----------
    shot
    shotfile
    experiment
    edition
    close_shotfile
    print_settings

    Returns
    -------

    Examples
    --------

    """

    # Print all warnings, even if repeated
    warnings.simplefilter('always', UserWarning)

    # Open shotfile if required and define band names
    shotfile = open_shotfile_default(
        shotfile=shotfile, diagnostics="RFL", shot=shot,
        experiment=experiment, edition=edition)
    bands = ["Q", "V"]

    # Read total number of probing frequency steps and duration of each one
    n_steps_tot = shotfile("PPG-HOPP")["PULS"][0]
    dt_step_s = shotfile("PPG-HOPP")["FREQ"][0] / 1E3

    # Read probing frequencies and number of steps per stairway of each band
    default_f_probings_GHz = [
        shotfile("MicroC" + band)["FRQ"] for band in bands]
    for index, f in enumerate(default_f_probings_GHz):
        crop_index = find_first(f == 0)
        if len(crop_index) > 0:
            default_f_probings_GHz[index] = f[: crop_index[0]]
    f_probings_GHz = {
        33928: [
            np.array([43.99000168], dtype=np.float32),
            np.array([70.69999695], dtype=np.float32)]
    }.get(shotfile.shot, default_f_probings_GHz)
    if all([np.array_equal(x, y) for x, y in zip(
            f_probings_GHz, default_f_probings_GHz)]):
        warnings.warn(
            "probing frequencies read directly from shofile (some shotfiles "
            "may have mistakes)", stacklevel=2)
    f_probings_Hz = [f * 1E9 for f in f_probings_GHz]
    n_steps_stairways = [len(f) for f in f_probings_Hz]

    # Read time of first and last samples and compute sampling frequency
#    time_base_info = shotfile.getTimeBaseInfo("T-Sio")
#    t_first_s = time_base_info.tBegin
#    t_last_s = time_base_info.tEnd
#    f_sampling_Hz = (time_base_info.ntVal - 1) / (t_last_s - t_first_s)
    time_base = shotfile("T-Sio")
    t_first_s = time_base[0]
    t_last_s = time_base[-1]
    f_sampling_Hz = (len(time_base) - 1) / (t_last_s - t_first_s)

    # Print settings
    if print_settings:
        print("\n {} HOPPING REFLECTOMETER SETTINGS - shot #{}, {}, edition "
              "{}\n".format(
            shotfile.diag, shotfile.shot, shotfile.exp, shotfile.ed))
        print(" total number of steps in the shot: {}".format(n_steps_tot))
        print(" duration of each step: {} ms".format(dt_step_s * 1E3))
        print(" time range: {} to {} s".format(t_first_s, t_last_s))
        print(" sampling frequency: {} MHz\n".format(f_sampling_Hz / 1E6))
        headers = ["band", "steps/stairway", "probing frequencies (GHz)"]
        align = ["^", "^", "<"]
        column_string = "".join([
            "{{:{}{}}}".format(align[index], len(header) + 2)
            for index, header in enumerate(headers)])
        print(column_string.format(*headers))
        for index, band in enumerate(bands):
            print(column_string.format(
                band, n_steps_stairways[index], f_probings_GHz[index]))
        print("")

    # Close shotfile if desired and return quantities
    if close_shotfile:
        shot = shotfile.shot
        diagnostic = shotfile.diag
        edition = shotfile.ed
        experiment = shotfile.exp
        #shotfile.close()
        return (
            bands, f_probings_Hz, n_steps_stairways, dt_step_s, n_steps_tot,
            t_first_s, t_last_s, f_sampling_Hz, shot, diagnostic, edition,
            experiment)
    else:
        return (
            bands, f_probings_Hz, n_steps_stairways, dt_step_s, n_steps_tot,
            t_first_s, t_last_s, f_sampling_Hz, shotfile)

def hop_times(
        channel=0, t_begin=None, t_end=None, full_stairways=False,
        close_shotfile=True, **read_settings_kw):
    # TODO: docstring

    # Open shotfile and read settings
    (
        bands, f_probings_Hz, n_steps_stairways, dt_step_s, _, t_first_s,
        t_last_s, f_sampling_Hz, shotfile) = hop_settings(
        close_shotfile=False, **read_settings_kw)
    band = bands[channel]
    n_steps_stairway = n_steps_stairways[channel]
    f_probings_Hz = f_probings_Hz[channel]

    # Set default time period to read
    if t_begin is None:
        t_begin = t_first_s
    if t_end is None:
        t_end = t_last_s

    # Compute number of samples per step
    n_samples_step = round(dt_step_s * f_sampling_Hz)

    # Determine the starting and ending time of each step and stairway
    t_steps_start_s = np.arange(max(t_last_s // dt_step_s + 1, 1)) * dt_step_s
    t_steps_end_s = t_steps_start_s + (n_samples_step - 1) / f_sampling_Hz
    t_stairways_start_s = t_steps_start_s[::n_steps_stairway]
    t_stairways_end_s = (
        t_stairways_start_s + t_steps_end_s[n_steps_stairway - 1])

    # Define error message when time interval to read has no measurements
    error_message = (
        "You must choose a time interval with measurements (it must contain "
        "part of the interval {} - {} s (necessary but not always "
        "sufficient). You can check the starting and ending time of each {} "
        "to choose with certainty)")

    # Determine stairways to read and their starting and ending times
    chosen_stairways = np.flatnonzero(np.logical_and(
        t_stairways_end_s >= t_begin, t_stairways_start_s <= t_end))
    n_chosen_stairways = len(chosen_stairways)
    if n_chosen_stairways == 0:
        raise ValueError(error_message.format(
                t_stairways_start_s[0], t_stairways_end_s[-1]), "stairway")
    t_stairways_start_s = t_stairways_start_s[chosen_stairways]
    t_stairways_end_s = t_stairways_end_s[chosen_stairways]

    # Determine steps to read and their starting and ending times
    if full_stairways:
        t_begin = t_stairways_start_s[0]
        t_end = t_stairways_end_s[-1]
        n_chosen_steps = n_chosen_stairways * n_steps_stairway
        start_step = chosen_stairways[0] * n_steps_stairway
        chosen_steps = np.arange(start_step, start_step + n_chosen_steps)
    else:
        chosen_steps = np.flatnonzero(np.logical_and(
            t_steps_end_s >= t_begin, t_steps_start_s <= t_end))
        n_chosen_steps = len(chosen_steps)
        if n_chosen_steps == 0:
            raise ValueError(error_message.format(
                    t_steps_start_s[0], t_steps_end_s[-1]), "step")
    t_steps_start_s = t_steps_start_s[chosen_steps]
    t_steps_end_s = t_steps_end_s[chosen_steps]

    # Determine probing frequencies and probed densities of the read steps
    f_probings_Hz = f_probings_Hz[chosen_steps % n_steps_stairway]
    n_e_probed_m03 = fpe_to_ne(f_probings_Hz)

    # Close shotfile if desired and return read and computed quantities
    if close_shotfile:
        shot = shotfile.shot
        diagnostic = shotfile.diag
        edition = shotfile.ed
        experiment = shotfile.exp
        #shotfile.close()
        return (
            band, t_steps_start_s, t_steps_end_s, t_stairways_start_s,
            t_stairways_end_s, n_steps_stairway, n_samples_step, dt_step_s,
            t_begin, t_end, f_sampling_Hz, f_probings_Hz, n_e_probed_m03,
            shot, diagnostic, edition, experiment)
    else:
        return (
            band, t_steps_start_s, t_steps_end_s, t_stairways_start_s,
            t_stairways_end_s, n_steps_stairway, n_samples_step, dt_step_s,
            t_begin, t_end, f_sampling_Hz, f_probings_Hz, n_e_probed_m03,
            shotfile)


# Time period with invalid samples in the beginning of each hopping step
_delta_t_invalid_samples_s = 4E-3


def hop_valid_samples(
        t_steps_start_s, dt_step_s, time_s, t_begin_valid_s=None,
        t_end_valid_s=None):
    # TODO: docstring
    """Determine valid hopping reflectometer samples by ignoring those in the
    beginning of each step where the frequency hop has not been completed
    yet."""

    if t_begin_valid_s is None:
        t_begin_valid_s = time_s.min()
    if t_end_valid_s is None:
        t_end_valid_s = time_s.max()

    return (
            (
            ((time_s - t_steps_start_s[0]) % dt_step_s) >
            _delta_t_invalid_samples_s) &
            (time_s >= t_begin_valid_s) &
            (time_s <= t_end_valid_s))


def hop_offsets(signal_I, signal_Q):
    # TODO: docstring
    """Estimate the offsets of the hopping reflectometer."""

    percentiles = [1, 99]
    return np.mean(
        np.percentile(signal_I, percentiles) +
        1j * np.percentile(signal_Q, percentiles))


# Named tuple to be returned by hop_data()
_hop_data_output = namedtuple("aug_read_hop_data_output", [
    "shot", "diagnostic", "experiment", "edition", "band", "time_s",
    "signal_I", "signal_Q", "complex_signal", "complex_phase", "phase",
    "unwrapped_phase", "amplitude", "offsets", "t_steps_start_s",
    "t_steps_end_s", "t_stairways_start_s", "t_stairways_end_s",
    "n_steps_stairway", "n_samples_step", "dt_step_s", "t_begin", "t_end",
    "f_probings_Hz", "n_e_probed_m03", "f_sampling_Hz", "shotfile"])


#TODO: split in pure reading part and computation / processing part
#TODO: add option not to read signals I and Q but only step times
def hop_data(
        t_begin_valid_s=None, t_end_valid_s=None, full_steps=False,
        crop_invalid_samples=False, remove_offsets=True, close_shotfile=True,
        return_complex=False, return_complex_phase=False, return_phase=False,
        return_phase_unwrap=False, return_amplitude=False, return_all=False,
        **read_times_kw):
    # TODO: docstring
    """Read data of hopping reflectometer."""

    # Read settings and starting and ending times of steps and stairways
    (
        band, t_steps_start_s, t_steps_end_s, t_stairways_start_s,
        t_stairways_end_s, n_steps_stairway, n_samples_step, dt_step_s,
        t_begin, t_end, f_sampling_Hz, f_probings_Hz, n_e_probed_m03,
        shotfile) = hop_times(close_shotfile=False, **read_times_kw)

    # Adjust time period if full steps are desired
    if full_steps:
        t_begin = t_steps_start_s[0]
        t_end = t_steps_end_s[-1]

    # Read the desired part of the I/Q signals in Volts (cropping to desired
    # time range because the dd library reads the nearest samples, but not
    # necessarily inside the input time range)
    time_s = shotfile.gettimebase("T-Sio", tbeg=t_begin, tend=t_end)
    indices = np.logical_and(time_s >= t_begin, time_s <= t_end)
    time_s = time_s[indices]
    signal_name = "FL{}-".format(band)
    signal_I, signal_Q = (
        shotfile.getobject(signal_name + s, tbeg=t_begin, tend=t_end)[indices] 
        for s in ["I", "Q"])

    # Determine valid samples and compute offsets
    valid_samples = hop_valid_samples(
        t_steps_start_s, dt_step_s, time_s, t_begin_valid_s=t_begin_valid_s,
        t_end_valid_s=t_end_valid_s)
    offsets = hop_offsets(signal_I[valid_samples], signal_Q[valid_samples])

    # If desired, remove samples in the beginning of each step where the
    # frequency hop has not been completed yet
    if crop_invalid_samples:
        if not np.any(valid_samples):
            raise ValueError(
                "You must choose a time interval with measurements after the "
                "cropping of invalid samples (the first {} micro seconds of "
                "each step are cropped). You can check the starting and "
                "ending time of each step to choose with certainty)".format(
                    _delta_t_invalid_samples_s * 1E6))
        if time_s[-1] < t_steps_start_s[-1] + _delta_t_invalid_samples_s:
            t_steps_start_s = t_steps_start_s[:-1]
            t_steps_end_s = t_steps_end_s[:-1]
            if (
                    time_s[-1] <
                    t_stairways_start_s[-1] + _delta_t_invalid_samples_s):
                t_stairways_start_s = t_stairways_start_s[:-1]
                t_stairways_end_s = t_stairways_end_s[:-1]
                f_probings_Hz = f_probings_Hz[:-1]
                n_e_probed_m03 = n_e_probed_m03[:-1]
        time_s = time_s[valid_samples]
        signal_I = signal_I[valid_samples]
        signal_Q = signal_Q[valid_samples]
        n_samples_step -= int(_delta_t_invalid_samples_s * f_sampling_Hz)

    # Define complex signal, remove offsets and compute phase and amplitude
    complex_signal = complex_phase = phase = unwrapped_phase = amplitude = None
    if return_all:
        return_complex = return_complex_phase = return_phase = \
            return_phase_unwrap = return_amplitude = True
    if any((
            return_complex, return_complex_phase, return_phase,
            return_phase_unwrap, return_amplitude)):
        complex_signal = signal_I + 1j * signal_Q
        # TODO: improve offset removal (ex. remove offset from each step)
        if remove_offsets:
            complex_signal -= offsets
        if return_amplitude or return_complex_phase:
            amplitude = np.absolute(complex_signal)
            complex_phase = np.nan_to_num(complex_signal / amplitude)
        if return_phase or return_phase_unwrap:
            phase = np.angle(complex_signal)
            if return_phase_unwrap:
                unwrapped_phase = np.unwrap(phase)

    # Close shotfile if desired and return read and computed quantities
    shot = shotfile.shot
    diagnostic = shotfile.diag
    edition = shotfile.ed
    experiment = shotfile.exp
    if close_shotfile:
        #shotfile.close()
        pass
    return _hop_data_output(
        shot, diagnostic, experiment, edition, band, time_s, signal_I,
        signal_Q, complex_signal, complex_phase, phase, unwrapped_phase,
        amplitude, offsets, t_steps_start_s, t_steps_end_s,
        t_stairways_start_s, t_stairways_end_s, n_steps_stairway,
        n_samples_step, dt_step_s, t_begin, t_end, f_probings_Hz,
        n_e_probed_m03, f_sampling_Hz, shotfile)


def mh(shot=0, coils_br=None, coils_bp=None, br=True, bp=True):
    # TODO: docstring
    # TODO: automatically choose diagnostic according to each coil
    # TODO: automatically write poloidal position according to each coil
    # TODO: allow any number of coils to be read
    """Reads signals of magnetic coils."""

    # Define common variables
    shot_mhi = 33700
    if shot == 0:
        shot = sf.getlastshot()

    # Read radial magnetic field
    if br:
        coils_br, = args.extend_args(coils_br, n=3)
        args.set_default(coils_br, ['B31-14', 'B31-34', 'B31-32'])
        if shot >= shot_mhi:
            diagnostics = 'MHI'
        else:
            diagnostics = ['MHA', 'MHH', 'MHH']
        data_br = many_signals(
            shots=shot, diagnostics=diagnostics, names=coils_br)
        if not bp:
            return data_br

    # Read poloidal magnetic field
    if bp:
        coils_bp, = args.extend_args(coils_bp, n=4)
        args.set_default(coils_bp, ['C09-01', 'C09-16', 'C09-09', 'C09-24'])
        if shot >= shot_mhi:
            diagnostics = 'MHI'
        else:
            diagnostics = ['MHA', 'MHD', 'MHD', 'MHE']
        data_bp = many_signals(
            shots=shot, diagnostics=diagnostics, names=coils_bp)
        if not br:
            return data_bp

    # Return data
    return data_br, data_bp


# Named tuple to be returned by read_cN
_read_cN_output = namedtuple("read_cN_output", ["time", "signal", "error"])


# TODO: docstring
def read_cN(filepath):
    """Read divertor nitrogen concentration data in ascii file output from
    Stuart Henderson's analysis.

    Parameters
    ----------
    filepath : string
        Path to file outputted from Stuart's analysis

    Returns
    ----------
    _cN_output named tuple
        A named tuple with three fields:
            time: 1D float array
                Time points.
            signal: 1D float array
                Nitrogen concentration data.
            error: 1D float array
                Data uncertainty.

    Examples
    --------

    """

    # Open file
    with open(filepath, 'r') as f:

        # Skip initial lines
        for line in f:
            if line[:-1] == 'Time [s]':
                break

        # Read time
        s = ''
        for line in f:
            if line[:-1] == 'cN [%]':
                break
            s += line[:-1]
        time = np.fromstring(s, sep=' ')

        # Read signal
        s = ''
        for line in f:
            if line[:-1] == 'cN +/- error [%]':
                break
            s += line[:-1]
        signal = np.fromstring(s, sep=' ')

        # Read error
        s = ''
        for line in f:
            if line[:-1] == 'End':
                break
            s += line[:-1]
        error = np.fromstring(s, sep=' ')

    # Return all data
    return _read_cN_output(time, signal, error)


# TODO: docstring
def find_contiguous_regions(a):
    """Find start and end indices of regions with contiguous True values."""
    return (
        np.flatnonzero(
            np.diff(np.concatenate(([False], np.array(a) != 0, [False]))))
        .reshape(-1, 2))


# TODO: docstring
def find_invalid_cN_indices(
        time_cN, time_Tdiv, signal_Tdiv, threshold_Tdiv=-0.5):
    """Find indices where cN measurements are not valid."""
    boundary_indices = np.searchsorted(
        time_cN,
        time_Tdiv[find_contiguous_regions(signal_Tdiv < threshold_Tdiv)])
    if boundary_indices.size == 0:
        return np.empty(0, dtype=int)
    return np.concatenate([np.arange(i[0], i[1]) for i in boundary_indices])

# TODO: docstring
def read_cN_valid(
        filepath, time_Tdiv, signal_Tdiv, replacement_value=np.nan,
        **find_invalid_cN_indices_kw):
    """Read divertor nitrogen concentration data in ascii file output from
    Stuart Henderson's analysis and remove invalid measurements.

    Parameters
    ----------
    filepath : string
        Path to file outputted from Stuart's analysis.
    time_Tdiv : 1-D float array
        Time array of the Tdiv signal.
    signal_Tdiv : 1-D float array
        Tdiv signal.
    replacement_value : float
        Value to replace invalid cN measurements.
    **find_invalid_cN_indices_kw
        All additional keyword arguments are passed to the
        find_invalid_cN_indices() call.

    Returns
    ----------
    _cN_output named tuple
        A named tuple with three fields:
            time: 1D float array
                Time points.
            signal: 1D float array
                Nitrogen concentration data.
            error: 1D float array
                Data uncertainty.

    Examples
    --------

    """
    data_cN = read_cN(filepath)
    invalid_indices = find_invalid_cN_indices(
        data_cN.time, time_Tdiv, signal_Tdiv, **find_invalid_cN_indices_kw)
    data_cN.signal[invalid_indices] = replacement_value
    data_cN.error[invalid_indices] = replacement_value
    return data_cN


# Named tuple to be returned by read_divertor
_read_divertor_output = namedtuple(
    "read_divertor_output", ["time", "area", "signal"])


# TODO: docstring
def read_divertor(filepath, area_1D=None):
    """Read data in ascii file output from DIVERTOR.

    Parameters
    ----------
    filepath : string
         Path to file outputted from DIVERTOR, usually named "prof_...",
         "timtr_..." or "3D_...".
    area_1D : bool
        If True, assume area is a 1D array. Otherwise, assume 2D. Default is
        read from file.

    Returns
    ----------
    _read_divertor_output named tuple
        A named tuple with three fields:
            time: 1D float array
                Time points
            area: 1D or 2D float array
                Y axis, usually Delta S, but rho is also possible
            signal: 2D float array
                jsat, density, etc., data.

    Examples
    --------

    """

    # Read file content and dimensions
    with open(filepath, 'r') as f:
        content = f.readlines()
    n_t = int(content[1])
    n_area = int(content[4])
    scale_factor = float(content[7])

    # Read time
    i = 10
    j = i + n_t
    time = np.array(content[i:j], dtype=float)

    # Read area
    if area_1D is None:
        area_1D = content[j + 1][15:21] == 'vector'
    i = j + 2
    if area_1D:
        j = i + n_area
        area = np.array(content[i:j], dtype=float)
    else:
        j = i + n_t
        area = np.array([s.split() for s in content[i:j]], dtype=float)

    # Read signal
    i = j + 2
    signal = np.array([s.split() for s in content[i:i + n_t]], dtype=float)

    # Return all data
    return _read_divertor_output(time, area, signal)
