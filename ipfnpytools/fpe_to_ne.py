"""
Compute the plasma critical density.
"""
from __future__ import division

from scipy.constants import pi, epsilon_0, e, m_e


def fpe_to_ne(f):
    # TODO: example
    """Compute the plasma critical density.

    Parameters
    ----------
    f: float
        Frequency

    Returns
    -------
    float
        Plasma critical density

    Examples
    --------

    """

    return 4 * pi ** 2 * f ** 2 * epsilon_0 * m_e / e ** 2