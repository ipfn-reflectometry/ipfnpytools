"""
Compute the average curve from a set of curves in the xy plane.
"""
from __future__ import absolute_import

from .interpolate_curves import interpolate_curves


def average_curve(ax, ay, interpolate=True, **interpolate_kw):
    #TODO: docstring
    """Compute the average curve from a set of curves in the xy plane."""

    # Interpolate curves if desired
    if interpolate:
        ax, ay = interpolate_curves(ax, ay, **interpolate_kw)

    # Compute average curve
    return ax.mean(axis=0), ay.mean(axis=0)
