import warnings
import numpy as np
# try:
#     import dd_20200525 as dd
# except ImportError:
#     from . import dd_dummy as dd
#     warnings.warn("DD library wrapper not available. Importing the dummy version instead")
import aug_sfutils as sf


def getsig(shotnr, diag, sig, tBegin=0.0, tEnd=10.0, exper='AUGD', edition=0):
    """Returns an object with the shotfile data
    
    Parameters
    -----------
    shotnr: int
        Number of the discharge
    diag: str
        Three letter code for the diagnostic
    sig: str
        Name of signal or signal group.
    tBegin: float
        Beginning of analysis window
    tEnd: float
        End of analysis window
    exper: str
        Experiment where to read the data. For private shotfiles it is the username in question.
    edition: int
        Version of the shotfile to be opened
        
    Returns
    -----------
    An object with the data respective to the signal or signal group. Look for more info in the DD library wrapper.
    """
    
    if (edition == 0) and (exper == 'AUGD'):
        shotfile = sf.SFREAD(shotnr, diag)
    else:
        shotfile = sf.SFREAD(sf=sf.manage_ed.sf_path(shotnr, diag, exp=exper, ed=edition)[0])
        
    if shotfile.status:
        return shotfile.getobject(sig)
    else:
        return None

def gettime(shotnr, diag, sig, exper='AUGD', edition=0):
    """Returns an object with the shotfile data
    
    Parameters
    -----------
    shotnr: int
        Number of the discharge
    diag: str
        Three letter code for the diagnostic
    sig: str
        Name of signal or signal group.
    exper: str
        Experiment where to read the data. For private shotfiles it is the username in question.
    edition: int
        Version of the shotfile to be opened
        
    Returns
    -----------
    An object with the data respective to the signal or signal group. Look for more info in the DD library wrapper.
    """
    
    if (edition == 0) and (exper == 'AUGD'):
        shotfile = sf.SFREAD(shotnr, diag)
    else:
        shotfile = sf.SFREAD(sf=sf.manage_ed.sf_path(shotnr, diag, exp=exper, ed=edition)[0])
        
    if not shotfile.status:
        return None
        
    data = shotfile.gettimebase(sig)
    
    if data is None:
        raise ValueError(f"Could not fetch time base for signal {diag} / {sig}")

    return data
