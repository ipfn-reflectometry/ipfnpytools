from builtins import str
from builtins import range
import warnings
import aug_sfutils as sf

def spec_channels(shotnr, shotfile='EVL'):
    """Reads the channels from spectroscopy shotfiles
    
    Parameters
    ---------------
    shotnr: int
        Come on!
    shotfile: str
        Usually 'EVL', 'FVL', 'GVL' or 'HVL'
    
    Returns
    ---------------
    chans: list of str
        A list of strings with the ordered name of the channels
    """
    
    evs = sf.SFREAD(shotnr, shotfile)
    chans = []
    for i in range(1,40):
        pname = 'CHAN_%.2d'%(i)
        try:
#             channame = evs.getParameter('LOS', pname)
            channame = evs.getparset('LOS')[pname][0]
            print(channame)
            channame = channame.decode().strip()
            print(channame)
        except:
            continue
        
        if channame != '':
                chans.append(channame)
#     evs.close()
    return chans