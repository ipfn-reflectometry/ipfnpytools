import numpy as np

def closest(array, value, dtype=None):
    """Return the index of "array" with the value closest to "value"
    Parameters
    ----------
    array: iterable
        Array of value in which to search for element closest to `value`
    value: float or iterable
        Search value. If multiple values are provided, the return is an array with the respective indexes
    dtype: np.dtype, optional
        Data type of `array`. If undefined, data type will be infered from the elements of `array`.
        
    Return
    ------
    idx: int, or int_ndarray
        Indexes of `array` absolutely closest to `value`

    """
    if dtype is not None:
        x = np.array(array, dtype=dtype)
    else:
        x = np.array(array)
    
    if hasattr(value, '__iter__'):
        if len(value) == 0:  # User provided an empty array.
            return np.array((), dtype=int)  # Return an empty array
        
        elif len(value) > 1:
            y = np.array(value)
            y.shape = (len(y), 1)
            
        else:
            y = value[0]
    else:
        y = value
        
        
    idx = np.nanargmin(np.abs(x-y), axis=-1)
#     idx = (np.abs(array-value)).argmin()
    return idx

def closest_val(array, value):
    idx = (np.abs(array-value)).nanargmin()
    return array[idx]
