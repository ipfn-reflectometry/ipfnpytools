"""
Mark steps and values of a hopping system in a plot.
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.transforms import blended_transform_factory

def mark_hop(
        values, steps_starts, steps_ends, units="", pos="top",
        format_values="", axes=None, compact_style=None, n_compact_style=5,
        vline_kw={}, **annotate_kw):
    # TODO: doctring
    # TODO: add features of IDL lg_mark_hopping_double
    """

    Parameters
    ----------
    values
    steps_starts
    steps_ends
    units
    pos
    format
    axes
    style
    annotate_kw
    **vline_kw

    Returns
    -------

    """

    # Convert some arguments to numpy arrays
    values, steps_starts, steps_ends = np.atleast_1d(
        values, steps_starts, steps_ends)
    n = len(values)

    # Set default keywords
    if axes is None:
        axes = plt.gca()
    if compact_style is None:
        compact_style = n >= n_compact_style
    if units != "":
        units = " " + units

    # Define position of labels
    if compact_style:
        pad_y = 8
        units_pad_x = -2
        units_pad_y = pad_y + plt.rcParams['font.size']
    else:
        pad_y = 10
    if pos == "top":
        pos_y = 1
        pad_y *= -1
        if compact_style:
            units_pad_y *= -1
    else:
        pos_y = 0
    (xmin, xmax) = axes.get_xlim()
    pos_x = (steps_starts + steps_ends) / 2
    pos_x[0] = max(pos_x[0], (xmin + min(steps_ends[0], xmax)) / 2)
    pos_x[-1] = min(pos_x[-1], (xmax + max(steps_starts[-1], xmin)) / 2)

    # Define properties of lines and labels
    if axes.images:
        color = "white"
    else:
        color = plt.rcParams['axes.edgecolor']
    default_vline_kw = {
        "linestyle": 'dashed', "linewidth": plt.rcParams['axes.linewidth'],
        "color": color}
    default_vline_kw.update(vline_kw)
    trans = blended_transform_factory(axes.transData, axes.transAxes)
    default_annotate_kw = {
        "xycoords": trans, "xytext": (0, pad_y),
        "textcoords": "offset points", "ha": "center", "va": "center",
        "color": color, "annotation_clip":True}
    default_annotate_kw.update(annotate_kw)

    # Mark steps, values and units
    text_format = "{{:{}}}".format(format_values)
    for index, value in enumerate(values):
        axes.axvline(steps_starts[index], **default_vline_kw)
        axes.axvline(steps_ends[index], **default_vline_kw)
        text = text_format.format(value) + (units if not compact_style else "")
        default_annotate_kw.update({"text": text, "xy": (pos_x[index], pos_y)})
        axes.annotate(**default_annotate_kw)
    if compact_style and units is not None:
        default_annotate_kw.update({
            "text": "{}".format(units), "xy": (1, pos_y),
            "xytext": (units_pad_x, units_pad_y), "xycoords": "axes fraction",
            "ha": "right"})
        axes.annotate(**default_annotate_kw)
    axes.set_xlim((xmin, xmax))
