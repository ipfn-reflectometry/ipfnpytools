#TODO: docstring
"""
Functions to localize fixed frequency O-mode reflectometry measurements.
"""

from builtins import zip
from itertools import count
import numpy as np
#TODO: replace with scipy.interpolate.InterpolatedUnivariateSpline
from scipy.interpolate import interp1d


def len_or_1(obj):
    #TODO: docstring
    """Return the length of an object if it has a len() method. Otherwise
    return 1."""
    return len(obj) if hasattr(obj, '__len__') else 1


#TODO: interpolate only in the required regions (do not consider shadowed regions or regions outside the probing density range)
def calc_r_prob(
        ne_prob, r, ne_profile, ne_dec=None, choose_r_min=False,
        **interp1d_kw):
    #TODO: docstring
    """Compute the positions of O-mode reflectometry cutoff layers in a
    density profile."""

    # Define default keywords
    if ne_dec is None:
        ne_dec = ne_profile[-1] < ne_profile[0]
    interp1d_default_kw = dict(bounds_error=False, assume_sorted=True)
    interp1d_default_kw.update(interp1d_kw)

    # Reverse profile if density is decreasing (required for interpolation)
    if ne_dec:
        r = r[::-1]
        ne_profile = ne_profile[::-1]

    # Determine monotonic portions of the profile
    indices = np.diff(ne_profile) > 0
    ne_profile = np.append(ne_profile[:-1][indices], ne_profile[-1])
    r = np.append(r[:-1][indices], r[-1])
    changepoints = (
        2 + np.flatnonzero(np.diff(np.sign(np.diff(ne_profile))) < 0))

    # Interpolate each profile portion to find the possible cutoffs
    r_prob = np.empty((1 + len(changepoints), len_or_1(ne_prob)))
    for index, ne_portion, r_portion in zip(
            count(), np.split(ne_profile, changepoints),
            np.split(r, changepoints)):
        if len(ne_portion) >= 2:
            f = interp1d(ne_portion, r_portion, **interp1d_default_kw)
            r_prob[index] = f(ne_prob)
        else:
            r_prob[index] = np.nan

    # Return the outermost cutoff
    if choose_r_min:
        return np.nanmin(r_prob, axis=0)
    return np.nanmax(r_prob, axis=0)


def calc_r_prob_all(ne_prob, time, area, data, **calc_r_prob_kw):
    #TODO: docstring
    """Compute the positions of O-mode reflectometry cutoff layers in many
    density profiles."""

    r_prob = np.full((len(time), len_or_1(ne_prob)), np.nan)
    for index, r, ne_profile in zip(count(), area, data):
        r_prob[index] = calc_r_prob(ne_prob, r, ne_profile, **calc_r_prob_kw)
    return r_prob


def calc_r_prob_hop(
        ne_prob_hop, t_steps_start, t_steps_end, time, area, data,
        **calc_r_prob_kw):
    #TODO: docstring
    """Compute the position of hopping O-mode reflectometry cutoff layers in
    many density profiles."""

    # For each probing frequency step
    r_prob = np.full(len(time), np.nan)
    for ne_prob, t_start, t_end, in zip(
            ne_prob_hop, t_steps_start, t_steps_end):
        indices = np.flatnonzero(np.logical_and(
            time >= t_start, time <= t_end))

        # Compute the position of the cutoff layer for each profile
        for index, r, ne_profile in zip(
                count(), area[indices], data[indices]):
            r_prob[indices[index]] = calc_r_prob(
                ne_prob, r, ne_profile, **calc_r_prob_kw)
    return r_prob


#TODO: use smart linewidth
def mark_r_prob(
        ne_prob, axes, signals_time, signals_area, signals_data, ne_dec=None,
        calc_r_prob_all_kw={}, **plot_kw):
    #TODO: docstring
    """Draw the positions of O-mode reflectometry cutoff layers in many
    density profiles of many signals."""

    # Define default keywords
    default_plot_kw = dict(color='c', linewidth=1)
    default_plot_kw.update(plot_kw)
    default_calc_r_prob_all_kw = dict(ne_dec=ne_dec)
    default_calc_r_prob_all_kw.update(calc_r_prob_all_kw)

    # Accept single signals
    axes = np.atleast_1d(axes)
    if axes.size == 1:
        signals_time = [signals_time]
        signals_area = [signals_area]
        signals_data = [signals_data]

    # Compute and draw the positions of the cutoff layers
    for ax, time, area, data in zip(
            axes, signals_time, signals_area, signals_data):
        ax.plot(
            time,
            calc_r_prob_all(
                ne_prob, time, area, data, **default_calc_r_prob_all_kw),
            **default_plot_kw)


def mark_r_prob_hop(
        ne_prob_hop, t_steps_start, t_steps_end, axes, signals_time,
        signals_area, signals_data, ne_dec=None, calc_r_prob_all_kw={},
        **plot_kw):
    #TODO: docstring
    """Draw the positions of O-mode reflectometry cutoff layers in many
    density profiles of many signals."""

    # Define default keywords
    default_plot_kw = dict(color='c', linewidth=1)
    default_plot_kw.update(plot_kw)
    default_calc_r_prob_all_kw = dict(ne_dec=ne_dec)
    default_calc_r_prob_all_kw.update(calc_r_prob_all_kw)

    # Accept single signals
    axes = np.atleast_1d(axes)
    if axes.size == 1:
        signals_time = [signals_time]
        signals_area = [signals_area]
        signals_data = [signals_data]

    # Compute and draw the positions of the cutoff layers
    for ax, time, area, data in zip(
            axes, signals_time, signals_area, signals_data):
        ax.plot(
            time,
            calc_r_prob_hop(
                ne_prob_hop, t_steps_start, t_steps_end, time, area, data,
                **default_calc_r_prob_all_kw),
            **default_plot_kw)
