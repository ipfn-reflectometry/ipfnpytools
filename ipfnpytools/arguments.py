#TODO: example
"""
Functions for easier argument input.

This module provides functions useful for transforming arguments in python
functions such that they can be given either as single values or as lists,
tuples or numpy arrays.

Example
-------

"""

from builtins import range
from past.builtins import basestring
import sys
from copy import copy as copy_
import numpy as np


def special_object_array(x):
    # TODO: example
    """Transforms the input into a numpy object array whose each element is a
    None, a string or an array with at least one dimension.

    Parameters
    ----------
    x: any type
        Object to be transformed into a numpy object array.

    Returns
    -------
    numpy object array
        Numpy object array whose each element is a None, a string or an array
        with at least one dimension.

    Examples
    --------

    """

    if not isinstance(x, (list, tuple, np.ndarray)):
        x = [x]
    if isinstance(x, np.ndarray):
        if x.dtype.type is not np.string_ and x.dtype != object:
            x = [x]
    n = len(x)
    output = np.empty(n, dtype=object)
    for index, element in enumerate(x):
        if element is not None and not isinstance(element, basestring):
            element = np.atleast_1d(element)
        output[index] = element
    return output


# TODO: use numpy.atleast_1d instead of listify and update all functions which call listify (maybe arrayfy stops being needed and can be erased (don't forget special treatment with strings))
def listify(args_list):
    #TODO: example
    """Transforms each element of the argument into a list if it is not a list
    or a tuple or a numpy array.

    Parameters
    ----------
    args_list : list
        List of objects to transform into a list.

    Returns
    -------
    None

    Examples
    --------

    """

    for index, arg in enumerate(args_list):
        if not isinstance(arg, (list, tuple, np.ndarray)):
            args_list[index] = [arg]


def replicate_elements(args_list, n):
    #TODO: example
    """Replicates each element of a list of sequences (list, tuple or numpy
    array) to a desired length.

    Parameters
    ----------
    args_list : list of lists / tuples / numpy arrays
        List of sequences whose elements will be replicated.
    n : integer
        Desired length of the output sequences.

    Examples
    --------

    """

    for index, arg in enumerate(args_list):
        if len(arg) == 1 and n != 1:
            if isinstance(arg, np.ndarray):
                temp = np.empty(n, dtype=arg.dtype)
                for idx in range(n):
                    temp[idx] = arg[0]
                args_list[index] = temp
            else:
                args_list[index] = [arg[0] for _ in range(n)]
        elif len(arg) != n:
            raise ValueError(
                ("Element {0} of {1} has length {2}, which is different from "
                 "{3}={4}. All the elements must have a length of 1 or {3} "
                 "for {5}({1}, {3})".format(
                    index, "args_list", len(arg), "n", n,
                    sys._getframe().f_code.co_name)))


def arrayfy(args_list, str_to_obj=True):
    #TODO: example
    """Transforms each element of the argument into a numpy array.

    Parameters
    ----------
    args_list : list
        List of objects to transform into a numpy array.
    str_to_obj : bool
        If True, string lists are converted to numpy object arrays instead of
        string arrays.

    Returns
    -------
    list
        List of numpy arrays.

    Examples
    --------

    """

    if str_to_obj:
        for index, arg in enumerate(args_list):
            if any(isinstance(x, basestring) for x in arg):
                args_list[index] = np.array(arg, dtype=object)
            else:
                args_list[index] = np.array(arg)
    else:
        for index, arg in enumerate(args_list):
            args_list[index] = np.array(arg)


# TODO: add a default value for n that makes sense and is useful
def extend_args(*args, **kwargs):
    #TODO: example
    """Returns a list of the replicated input arguments as numpy arrays. This
    is achieved by applying listify, replicate_elements and arrayfy to a list
    containing the input arguments.

    Parameters
    ----------
    *args
        Arguments that will be replicated and converted to numpy arrays.
    n : integer, optional
        Number of replications.
    **kwargs
        All additional keyword arguments are passed to the arrayfy() call.

    Returns
    -------
    list
        List of the replicated input arguments as numpy arrays.

    Examples
    --------

    """

    args_list = list(args)
    listify(args_list)
    if 'n' not in kwargs:
        kwargs['n'] = 1
    replicate_elements(args_list, kwargs.pop('n'))
    arrayfy(args_list, **kwargs)
    return args_list


def set_default(
        variable, values, indices=None, indices_for_values=True, copy=False):
    #TODO: example
    """Set default values of variable elements which are None.

    Parameters
    ----------
    variable : numpy array
        Array of values which may contain Nones.
    values : single value or sequence of values
        Default values to replace Nones in variable.
    indices : int or int/bool list/array
        If given, only replace the values in 'variable'['indices'].
    indices_for_values : bool
        If True, 'values'['indices'] are used as default values. If false,
        'values' are used instead. Ignored if 'indices' is None.
    copy: bool
        If True and 'values' has only one element, shallow copies of 'values'
        are used to set the elements of 'variable'. If False, 'values' is used
        directly to set these elements without making copies. Usually 'copy'
        is set to True when 'values' is a mutable object.

    Returns
    -------
    None

    Examples
    --------

    """

    values = np.atleast_1d(values)
    if values.size == 1:
        if copy:
            values = np.array(
                [copy_(values[0]) for _ in range(len(variable))])
        else:
            values = np.repeat(values, len(variable))
    none_indices = [x is None for x in variable]
    if indices is None:
        variable[none_indices] = values[none_indices]
    else:
        indices = np.array(indices)
        if indices.dtype != bool:
            temp = np.full(len(variable), False)
            temp[indices] = True
            indices = temp
        idx = np.logical_and(indices, none_indices)
        if indices_for_values:
            variable[idx] = values[idx]
        else:
            variable[idx] = values[none_indices[indices]]
