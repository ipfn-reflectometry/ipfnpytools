"""
Define default color normalization range.
"""

import numpy as np


def default_color_normalization(
        c, clip_low, clip_high, portion, norm, vmin=None, vmax=None):
    # TODO: examples
    """Define default color normalization range.
    
    Parameters
    ----------
    c : 2D array-like
        The image or mesh data.
    clip_low, clip_high : float
        Fraction of low / high values to clip, assuming 'portion'==0. Actual 
        clipped fraction is lower if 'portion'>0.
    portion : float
        Extra portion of the color scale that is not clipped. Reduces the
        effect of 'clip_low' and 'clip_high'.
    norm : str ({'linear', 'log'})
        The normalization method used to scale scalar data to the [0, 1] range 
        before mapping to colors.
    vmin, vmax : float, optional
        Minimum / maximum 'c' values to display.

    Returns
    -------
    vmin, vmax: float
        Minimum / maximum 'c' values to display.

    Examples
    --------

    """

    # Define auxiliary parameters
    cmax = np.nanmax(c)
    pmin, pmax = np.nanpercentile(
        c, [clip_low * 100, (1 - clip_high) * 100])
    a = (1 - portion) / (1 - 2 * portion)
    b = portion / (1 - 2 * portion)

    # Compute default values for logarithmic scale
    if norm == 'log':
        if vmin is None:
            vmin = max(
                (pmin ** a) / (pmax ** b),
                np.nanmin(c[c > 0]))
        if vmax is None:
            vmax = min((pmax ** a) / (pmin ** b), cmax)
    
    # Compute default values for linear scale
    elif norm == 'linear':
        if vmin is None:
            vmin = max(pmin * a - pmax * b, np.nanmin(c))
        if vmax is None:
            vmax = min(pmax * a - pmin * b, cmax)
    return vmin, vmax
