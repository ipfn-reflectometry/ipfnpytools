import numpy as np

from .getVessel import getVessel
from .closest import closest
from .trz_to_rhop import fast_trz_to_rhop
import aug_sfutils as sf


def resample_polygon(xy: np.ndarray, n_points: int = 100, min_distance: float = None) -> np.ndarray:
    """Resample a polygon. Adapted from https://stackoverflow.com/a/70664846"""
    # Cumulative Euclidean distance between successive polygon points.
    # This will be the "x" for interpolation
    d = np.cumsum(np.r_[0, np.sqrt((np.diff(xy, axis=0) ** 2).sum(axis=1))])

    # get linearly spaced points along the cumulative Euclidean distance
    d_sampled = np.linspace(0, d.max(), n_points if min_distance is None else int(d.max()/min_distance))

    # interpolate x and y coordinates
    xy_interp = np.c_[
        np.interp(d_sampled, d, xy[:, 0]),
        np.interp(d_sampled, d, xy[:, 1]),
    ]
    
    return xy_interp


# Get smallest rho for a vessel component
def get_shadow(vessel_component, eq_shotfile):
    """Return the vessel point with the lowest flux. Corresponding to where the vessel limits the plasma
    
    Parameters
    ----------
    vessel_component: Nx2 array
        Array defining the border of the vessel component
    eq_shotfile: aug_sfutils.EQU object
        Equillibrium shotfile already initialized
        
    Returns
    -------
    rho, R, Z: float
        Returns the smallest rho and corresponding R, Z coordinates.
    """
    
    rz = resample_polygon(vessel_component, min_distance=1e-3)
    
    rho = fast_trz_to_rhop(eq_shotfile.time[:, None], rz[:,0], rz[:,1], eq_shotfile=eq_shotfile)
    
    return np.nanmin(rho, axis=-1), rz[np.nanargmin(rho, axis=-1), 0], rz[np.nanargmin(rho, axis=-1), 1]


def get_limiters(shot: int, equilibrium : str = 'EQH'):
    """Get the different limiters' shadows in rho_poloidal
    
    Parameters
    ----------
    shot: int
        Shot number
        
    Returns
    -------
    time: ndarray
        1D array of time instants for which the limiter shadow is calculated
    results: dictionary
        For each of the four limiters {"outer_shield", "top_baffle", "lower_baffle", "inner_shield"},
        the rho, R, and Z coordinates of the limiting points
    """
    
    if (shot < 30136) or (shot > 41570):
        raise ValueError("get_limiters() only works for shots between [30136, 41570]")
    
    structures = {
        "outer_shield" : {'indices': [4],},
        "top_baffle" : {'indices': [30, 31],},
        "lower_baffle" : {'indices': [17, 18],},
        "inner_shield" : {'indices': [3],},
    }
    
    results = {key: {} for key in structures}

    eq = sf.EQU(shot, diag=equilibrium)
    vessel = getVessel(shot)
    
    for key in structures:
        shadow_rho, shadow_r, shadow_z = get_shadow(vessel[structures[key]['indices'][0]], eq_shotfile=eq)
        for i in structures[key]['indices'][1:]:
            new_shadow_rho, new_shadow_r, new_shadow_z = get_shadow(vessel[i], eq_shotfile=eq)
            replace_idx = np.argwhere((new_shadow_rho < shadow_rho) | np.isnan(shadow_rho))
            shadow_rho[replace_idx] = new_shadow_rho[replace_idx]
            shadow_r[replace_idx] = new_shadow_r[replace_idx]
            shadow_z[replace_idx] = new_shadow_z[replace_idx]

        results[key]['rho'] = shadow_rho
        results[key]['r'] = shadow_r
        results[key]['z'] = shadow_z
        
    return eq.time, results