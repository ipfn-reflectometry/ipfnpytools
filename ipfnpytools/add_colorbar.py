"""
Add a colorbar to a plot.
"""

import matplotlib as mpl
import matplotlib.pyplot as plt


def add_colorbar(
        mappable=None, cax=None, ax=None, label=None, pad=0.025, width=0.025, 
        axes_kw=None, set_label_kw=None, **colorbar_kw):
    #TODO: doctstring
    """Add a colorbar to a plot.

    This is basically the matplotlib.pyplot.colorbar with added features and
    automation.
    
    Parameters
    ----------
    mappable : `~matplotlib.cm.ScalarMappable`, optional
        The `matplotlib.cm.ScalarMappable` (i.e., `.AxesImage`, `.ContourSet`, 
        etc.) described by this colorbar. If None, defaults to the current 
        colorable artist (matplotlib.pyplot.gci()).
    cax : `~matplotlib.axes.Axes`, optional
        Axes into which the colorbar will be drawn. If `None`, then a new Axes 
        is created next to the Axes(s) specified in *ax*.
    ax : `~matplotlib.axes.Axes` or iterable or `numpy.ndarray` of Axes, optional
        The one or more parent Axes next to which a new colorbar Axes will be 
        drawn. This parameter is only used if *cax* is None. If None, defaults 
        to the Axes that contains the mappable used to create the colorbar.
    label : str, optional
        Color bar label. If None, defaults to no label.
    pad : float, optional
        Space between the colorbar and the plot axis, in fraction of the
        figure size in rc settings. This parameter is only used if *cax* is 
        None. Defaults to 0.025.
    width : float, optional
        Width of the color bar, in fraction of the figure size in rc settings. 
        This parameter is only used if *cax* is None. Defaults to 0.025.
    axes_kw : dict, optional
        Dict with keywords passed to the matplotlib.pyplot.axes() call used to 
        create the color bar axes. This parameter is only used if *cax* is 
        None. If None, defaults to {}.
    set_label_kw : dict, optional
        Dict with keywords passed to the matplotlib.colorbar.set_label() call 
        used to add a label to the axis of the colorbar. This parameter is only 
        used if *label* is not None. If None, defaults to {}.
    **colorbar_kw
        All additional keyword arguments are passed to the 
        matplotlib.pyplot.colorbar() call used to create the color bar. If 
        None, defaults to {}.

    Returns
    -------
    `~matplotlib.colorbar.Colorbar` instance
        Colorbar object.

    Examples
    --------

    """

    # Define default arguments
    if mappable is None:
        mappable = plt.gci()
    if ax is None:
        ax = getattr(mappable, "axes", None)
    if axes_kw is None:
        axes_kw = {}
    if cax is None:
        h_factor = (
            ax.get_figure().get_size_inches()[0] /
            plt.rcParams['figure.figsize'][0])
        ax_pos = ax.get_position()
        cax = plt.axes([
            ax_pos.x0 + ax_pos.width + pad / h_factor,
            ax_pos.y0, width / h_factor, ax_pos.height],
            **axes_kw)

    # Draw color bar and label
    color_bar = plt.colorbar(mappable, cax=cax, ax=ax, **colorbar_kw)
    if label is not None:
        if set_label_kw is None:
            set_label_kw = {}
        # Explicit size required because of matplotlib bug:
        # https://github.com/matplotlib/matplotlib/issues/25298/
        default_set_label_kw = dict(size=mpl.rcParams['axes.labelsize'])
        default_set_label_kw.update(set_label_kw)
        color_bar.set_label(label, **default_set_label_kw)
    return color_bar
