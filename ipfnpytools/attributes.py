"""
Useful functions to set and get attributes on nested objects.

Taken from
https://stackoverflow.com/questions/31174295/getattr-and-setattr-on-nested-objects
"""

import functools


def rsetattr(obj, attr, val):
    #TODO: docstring
    """"Drop-in replacement for setattr which can also handle dotted attr
    strings."""

    pre, _, post = attr.rpartition('.')
    return setattr(rgetattr(obj, pre) if pre else obj, post, val)


sentinel = object()
def rgetattr(obj, attr, default=sentinel):
    # TODO: docstring
    """"Drop-in replacement for getattr which can also handle dotted attr
    strings."""

    if default is sentinel:
        _getattr = getattr
    else:
        def _getattr(obj, name):
            return getattr(obj, name, default)
    return functools.reduce(_getattr, [obj]+attr.split('.'))