"""
Compute the height ratios required to have one subplot with a specific aspect
ratio.
"""

from __future__ import division
from __future__ import absolute_import
from builtins import range
import matplotlib.pyplot as plt
from .subplotting import subplotting


#TODO: docstring
def height_ratios(aspect_ratio, nrows, pos=-1, **subplotting_kw):
    """Compute the height ratios required to have one subplot with a specific
    aspect ratio."""

    fig, axes = subplotting(nrows=nrows, **subplotting_kw)
    axes[pos].set_aspect(aspect_ratio, 'box-forced')
    fig.canvas.draw()
    a = (
        axes[pos].get_position().bounds[-1] /
        axes[pos - 1].get_position().bounds[-1])
    plt.close(fig)
    ratios = [1 for _ in range(nrows)]
    ratios[pos] = (nrows - 1) * a / (nrows - a)
    return ratios
