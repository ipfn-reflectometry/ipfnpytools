import numpy as np


def has_even_sampling(t, **allclose_kw):
    # TODO: docstring
    """Check if all elements of a 1D array are evenly spaced."""

    default_allclose_kw = dict(atol=(t[-1] * 1E-5))
    default_allclose_kw.update(allclose_kw)
    dt = (t - np.roll(t, 1))[1:]
    return np.allclose(dt, dt[0], **default_allclose_kw)
