import numpy as np
import aug_sfutils as sf
from scipy.interpolate import interpn, RegularGridInterpolator


def trhopz_to_r(time, rho, z, shot=None, equilibrium='EQH', eq_shotfile=None):
    """Convert time, rho poloidal, and z coordinates to R.
    
    Parameters
    ----------
    time: float, ndarray
        Time instants.
    rho: float, ndarray
        Rho poloidal coordinates.
    z: float, ndarray
        Height z coordinates.
    shot: int
        Shot number.
    equilibrium: str {'EQH', 'IDE'}
        Equilibrium shot file name, optional. If `shot` is provided, load the corresponding equilibrium.
    eq_shotfile: dd.Shotfile
        Equilibrium shot file with prefilled scalar fields, optional.
        
    Returns
    -------
    R_HFS, R_LFS: ndarray
        1D array of size N with the R (major radius) on both the high and low field sides.
    
    """
    
    
    # Check the validity of the arguments ------------------------------------------
    try:
        [time, rho, z] = np.broadcast_arrays(time, rho, z)
    except ValueError:
        raise ValueError("time, rho, and z must be scalars or have equal shapes")
        
    shape = time.shape
        
#     if (time.shape != rho.shape) or (time.shape != z.shape):
#         raise TypeError("time, rho, and z must all have the same shape.")

    # Check what equilibrium parameters the user provided --------------------------
    if eq_shotfile is not None:
        eq = eq_shotfile
    else:
        try:
            eq = sf.EQU(shot, diag=equilibrium)
        except TypeError:
            raise ValueError("You must provide either `shot` or `eq_shotfile`")
            
    if (eq_shotfile is not None) and (shot is not None):
        raise ValueError("You can only provide `shot` or `eq_shotfile`, but not both")
    
    # Algorithm definitions -------------------------------------------------------
    
    # Number of points in the R axis used for sucessive aproximation. MUST BE EVEN.
    R_resolution = 40
    
    # Step in [m] between each point in the R axis
    current_step = ((eq.Rmesh[-1] - eq.Rmesh[0]) / float(R_resolution - 1)) 
    
    # Initial R axis at the start of the algorithm
    R = np.linspace(eq.Rmesh[0], eq.Rmesh[-1], R_resolution, dtype=np.float32)
    
    # Maximum number of iterations, will define the final resolution
    max_iterations = 3
    
    # Data type, float32 will typically be faster
    data_type = np.float64
    
    # Array to hold the solutions to the mapping problem
    answer = np.full((time.size, 2), np.nan)

    
    # Decide whether to run in midplane, optimized mode (z==0) ---------------------
    midplane = True if (z==0).all() else False       
    if midplane:
        print('Optimus prime')
        try:
            zero_idx = np.where(eq.Zmesh==0)[0][0]
        except IndexError:
            warning.warn("Zmesh of equilibrium does not contain z=0. Sub-optimal interpolation speed")
            midplane = False
            
    
    # Produce arrays for interpolation ---------------------------------------------
    time_grid, R_grid = np.meshgrid(time.flatten(), R, indexing='ij')
    if not midplane:
        z_grid, _ = np.meshgrid(z.flatten(), R, indexing='ij')
    rho_grid, _, = np.meshgrid(rho.flatten(), R, indexing='ij')

    # Begin the iterative algorithm ------------------------------------------------
    for iteration in range(max_iterations):
        
        # Define the interpolator, only on the first loop
        try:
            interpolator
        except NameError:
            if midplane:
                interpolator = RegularGridInterpolator(
                    points=(
                        np.array(eq.time, dtype=data_type), 
                        np.array(eq.Rmesh, dtype=data_type)
                    ), 
                    values=np.array(np.swapaxes(np.sqrt((eq.pfm - eq.psi0)/(eq.psix-eq.psi0)), 0, 2), dtype=data_type)[:, zero_idx, :], 
                    bounds_error=False,
                    fill_value=np.nan,
                )
            else:
                interpolator = RegularGridInterpolator(
                    points=(
                        np.array(eq.time, dtype=data_type), 
                        np.array(eq.Zmesh, dtype=data_type), 
                        np.array(eq.Rmesh, dtype=data_type)
                    ), 
                    values=np.array(np.swapaxes(np.sqrt((eq.pfm - eq.psi0)/(eq.psix-eq.psi0)), 0, 2), dtype=data_type), 
                    bounds_error=False,
                    fill_value=np.nan,
                )
            
        # Interpolate the grid points (t, Z, R) mapping them to rho
        if midplane:
            rho_trz = interpolator(
                np.array([
                    time_grid.flatten(),
                    R_grid.flatten(),
                ]).astype(data_type).T
            )
        else:
            rho_trz = interpolator(
                np.array([
                    time_grid.flatten(),
                    z_grid.flatten(),
                    R_grid.flatten(),
                ]).astype(data_type).T
            )
        
        # Difference between rho(t, r, z) and target rho 
        stage1 = np.reshape(rho_trz, (len(time.flatten()), len(R))) - rho_grid
        # Roll this difference and multiply by it self
        stage2 = np.roll(stage1, shift=1, axis=-1)
        # stage3 will be negative when the rho(t, r, z) field crosses the target rho
        stage3 = stage2 * stage1
        
        # Find the points on the grid adjacent to the desired flux surfaces
        crossings = (stage3<0)
        # If the mapping (t, rho, z) -> (t, r, z) is possible, it has at least one, but often, two solutions
        valid_solutions = np.sum(crossings, axis=-1)==2
        valid_crossings = crossings[valid_solutions]
        
        # Substitute in answer for the points with two solutions
        answer[valid_solutions] = R_grid[valid_solutions][valid_crossings].reshape((valid_crossings.shape[0], 2))

        if iteration == (max_iterations - 1):
#             answer[valid_solutions] += current_step/2.0  # Slighty improve accuracy, might be irrelevant
            break
    
        else:  # I know this else is irrelevant, it's just good practice
            # Refine the search grid
            R_grid[valid_solutions] = np.array([
                np.concatenate((
                    np.linspace(left - current_step, left, int(R_resolution//2), dtype=np.float32),
                    np.linspace(right - current_step, right, int(R_resolution//2), dtype=np.float32)
                ))
                for [left, right] in answer[valid_solutions]
            ])
            # Update the step in [m] on the R axis
            current_step /= R_resolution / 2.0 - 1.0
        
    return (answer[:, 0].reshape(shape), answer[:, 1].reshape(shape)) if (shape!=(1,)) else (answer[:, 0, 0], answer[:, 1, 0])


def trhopz_to_r_dumb_implementation(time, rho, z, shot, equillibrium='EQH', eq_shotfile=None):
    """Convert time, rho poloidal, and z coordinates to R.
    
    Parameters
    ----------
    time: ndarray
        Time instants 1D array of size N.
    rho: ndarray
        Rho poloidal coordinates 1D array of size N.
    z: float
        Height (z) coordinate.
    shot: int
        Shot number.
    equillibrium: str, optional
        Equillibrium shot file.
        
    Returns
    -------
    R_HFS, R_LFS: ndarray
        1D array of size N with the R (major radius) on both the high and low field sides.
    
    """

    
    step_resolution = 100  # MUST BE EVEN
    
    R = np.linspace(1, 2.5, step_resolution)
    sample_points = np.array(np.meshgrid(time, z, R, indexing='ij'))
    
    if eq_shotfile is None:
        eq = map_equ.equ_map()
        eq.Open(shot, equillibrium)
        #Populate the fields
        eq.read_pfm()
        eq.read_scalars()
    else:
        eq = eq_shotfile
    
    max_iterations = 4
    
    times_with_two_solutions = time  # Assume that all time instants have solutions
    
    for iteration in range(max_iterations):
        
        grid_shape = sample_points[0].shape
        
        rho_trz = interpn(
            points=(eq.time, eq.Zmesh, eq.Rmesh), 
            values=np.swapaxes(np.sqrt((eq.pfm - eq.psi0)/(eq.psix-eq.psi0)), 0, 2), 
            xi=np.reshape(sample_points, (3, -1)).T,
#             bounds_error=False,
#             fill_value=0.0
        )

        isto, _, _ = np.meshgrid(rho, z, R, indexing='ij')

        stage1 = np.reshape(rho_trz, grid_shape) - isto
        stage2 = np.roll(stage1, shift=1, axis=2)
        stage3 = stage2 * stage1

        times_with_two_solutions = times_with_two_solutions[np.sum((stage3<0),axis=-1)[:,0]==2]
        
        if iteration == (max_iterations - 1): 
            break
          
        time_idx_with_two_solutions = np.argwhere(np.sum((stage3<0),axis=-1)[:,0]==2).flatten()
        rho = rho[(np.sum((stage3<0),axis=-1)[:,0]==2)]
        
        new_sample_points = np.array(np.meshgrid(times_with_two_solutions, z, R, indexing='ij'))
        
        for i, j in enumerate(time_idx_with_two_solutions):
            # Indices of points closest to the surface (to the right)
            [i1, i2] = np.argwhere(stage3[j][0]<0).flatten().tolist()
            
            # Values of such points
            hfs_leftbound = sample_points[2, j, 0, i1 - 1]
            hfs_rightbound = sample_points[2, j, 0, i1]
            
            lfs_leftbound = sample_points[2, j, 0, i2 - 1]
            lfs_rightbound = sample_points[2, j, 0, i2]
            
            
            new_sample_points[2][i][0] = np.concatenate((
                np.linspace(hfs_leftbound, hfs_rightbound, int(step_resolution/2)),
                np.linspace(lfs_leftbound, lfs_rightbound, int(step_resolution/2)),
            ))
        
        sample_points = new_sample_points
        
    if eq_shotfile is None:
        eq.Close()
    
    cross_times = sample_points[0][stage3<0]
    cross_R = sample_points[2][stage3<0]

    unique_times = np.unique(sample_points[0][stage3<0])
    R_LFS = np.empty_like(unique_times)
    R_HFS = np.empty_like(unique_times)

    prev_time = cross_times[0]
    prev_r = cross_R[0]
    R_LFS[0] = prev_r
    R_HFS[0] = prev_r

    i = 0
    for t, r in zip(cross_times[1:], cross_R[1:]):
        if t == prev_time:
            R_LFS[i] = max(prev_r, r)
            R_HFS[i] = min(prev_r, r)
        else:
            i += 1

        prev_time = t
        prev_r = r
    
    r_hfs = np.full_like(time, np.nan)
    r_hfs[np.isin(time, unique_times, assume_unique=True)] = R_HFS
    
    r_lfs = np.full_like(time, np.nan)
    r_lfs[np.isin(time, unique_times, assume_unique=True)] = R_LFS
    
    return r_hfs, r_lfs


def very_slow_trhopz_to_r(time, rho, z, shot, equillibrium='EQH'):
    """Convert time, rho poloidal, and z coordinates to R.
    
    Parameters
    ----------
    time: float, ndarray
        Time instants. Float or 1D array of size N.
    rho: float, ndarray
        Rho poloidal coordiantes. Float or 1D array of size N.
    z: float
        Height (z) coordinates.
    shot: int
        Shot number.
    equillibrium: str
        Equillibrium shot file.
        
    Returns
    -------
    R: float, ndarray
        Major radius matching the (t, rho, z) triplet. In general there are two R values for each triplet.
        The dimensions of R vary, depending on the dimensions of time and rho.
        (time, rho, z) = (N, M, 1) -> R = (N, M, 2)
        (time, rho, z) = (N, 1, 1) or (1, N, 1) -> R = (N, 2)
        (time, rho, z) = (1, 1, 1) -> R = (2,)
    """
    
    eq = map_equ.equ_map()
    eq.Open(shot, equillibrium)
    #Populate the fields
    eq.read_pfm()
    eq.read_scalars()
    r, z = eq.cross_surf(rho=rho, r_in=1.5, z_in=z, theta_in=0.0, t_in=time)
    eq.Close()
    
    try:
        len_time = len(time)
    except TypeError:
        len_time = 1
    try:
        len_rho = len(rho)
    except TypeError:
        len_rho = 1
        
    if len_time == 1 and len_rho > 1:
        return r[0]
    
    if len_time > 1 and len_rho == 1:
        return r[:,0,:]
    
    if len_time == 1 and len_rho == 1:
        return r[0, 0]
    
    return r