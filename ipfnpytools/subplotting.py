"""
Create a figure with a set of correctly placed subplots and labels.
"""
from __future__ import absolute_import
from __future__ import division

from builtins import zip
import string
import sys
import warnings
import numpy as np
import matplotlib.pyplot as plt
from . import arguments


# TODO: examples
def subplotting(
        nplots=None, nrows=None, ncols=None, label=None, label_pos_x=1,
        label_pos_y=1, label_color='black', label_pad_x=None,
        label_pad_y=None, label_start=0, h_factor=None, v_factor=None,
        order='column', left=None, bottom=None, right=None, top=None,
        wspace=None, hspace=None, sharex=False, sharey=False, squeeze=True,
        subplot_kw=None, gridspec_kw=None, annotate_kw=None, **fig_kw):
    """Create a figure with a set of correctly placed subplots and labels.

    This is basically the matplotlib.pyplot.subplots routine with added
    features and automation.

    Label arguments can be given either as single values or as lists, tuples
    or numpy arrays.

    Parameters
    ----------
    nplots : int, optional
        Number of subplots (used to choose default 'nrows' and 'ncols').
        Defaults to 'nrows' * 'ncols'.
    nrows, ncols : int, optional
        Number of rows/columns of the subplot grid. Defaults to
        -(-'nplots' // 'ncols')/-(-'nplots' // 'nrows').
    label : bool or bool list, optional
        If True, draw label in each subplot ((a), (b), (c), etc.). Defaults to
        True if 'nplots'>1 and False otherwise.
    label_pos_x, label_pos_y : float or float list, optional
        X/Y coordinate of each subplot label, in axes fraction.
    label_color : any matplotlib color or color list, optional
        Color of each subplot label.
    label_pad_x, label_pad_y : float or float list, optional
        X/Y offset of each subplot label with respect to
        'label_pos_x'/'label_pos_y', in points. Defaults to -7/-9 if
        'label_pos_x'/'label_pos_y' > 0.5 and 7/9 otherwise.
    label_start : int, optional
        Index of the starting alphabet letter in the labels.
    h_factor, v_factor: float, optional
        Factor to multiply the horizontal / vertical size of the figure.
        Defaults to min('ncols', 2)/min((1 + 'nrows') / 2, 2).
    order: {'column', 'row'}, optional
        Order in which to label the subplots.
    left, right, top, bottom : float, optional
        Extent of the subplots as a fraction of figure width or height. Left
        cannot be larger than right, and bottom cannot be larger than top.
        Defaults based on rcParams, 'h_factor' and 'v_factor'.
    wspace, hspace: float, optional
        Amount of width/height reserved for space between subplots,
        expressed as a fraction of the average axis width/height. Defaults
        based on rcParams, 'sharey'/'sharex', 'ncols'/'nrows',
        'h_factor'/'v_factor' and ('right' - 'left')/('top' - 'bottom')
    sharex, sharey : bool or {'none', 'all', 'row', 'col'}, optional
        Controls sharing of properties among x (sharex) or y (sharey) axes:
            - True or 'all': x- or y-axis will be shared among all subplots.
            - False or 'none': each subplot x- or y-axis will be independent.
            - 'row': each subplot row will share an x- or y-axis.
            - 'col': each subplot column will share an x- or y-axis.
        When subplots have a shared x-axis along a column, only the x tick
        labels of the bottom subplot are visible. Similarly, when subplots
        have a shared y-axis along a row, only the y tick labels of the first
        column subplot are visible.
    squeeze : bool, optional
        If True, extra dimensions are squeezed out from the returned Axes
        object:
            - if only one subplot is constructed (nrows=ncols=1), the
            resulting single Axes object is returned as a scalar.
            - for Nx1 or 1xN subplots, the returned object is a 1D numpy
              object array of Axes objects are returned as numpy 1D arrays.
            - for NxM, subplots with N>1 and M>1 are returned as a 2D arrays.
        If False, no squeezing at all is done: the returned Axes object is
        always a 2D array containing Axes instances, even if it ends up being
        1x1.
    subplot_kw : dict, optional
        Dict with keywords passed to the add_subplot() call used to create
        each subplot.
    gridspec_kw : dict, optional
        Dict with keywords passed to the GridSpec constructor used to create
        the grid the subplots are placed on.
    annotate_kw : dict, optional
        Dict with keywords passed to the matplotlib.axes.Axes.annotate() call
        used to write each subplot label.
    **fig_kw :
        All additional keyword arguments are passed to the figure() call.

    Returns
    -------
    fig : matplotlib.figure.Figure object
        Dict with keywords passed to the matplotlib.axes.Axes.text() call used
        to write each subplot label.
    ax : Axes object or array of Axes objects
        ax can be either a single matplotlib.axes.Axes object or an array of
        Axes objects if more than one subplot was created. The dimensions of
        the resulting array can be controlled with the squeeze keyword, see
        above.

    Examples
    --------

    """

    # Define default number of subplots, rows and columns
    if nplots is None:
        if nrows is None:
            nrows = 1
        if ncols is None:
            ncols = 1
        nplots = nrows * ncols
    else:
        if nrows is None:
            if ncols is None:
                ncols = 1
            nrows = -(-nplots // ncols)
        else:
            if ncols is None:
                ncols = -(-nplots // nrows)
    nplots_tot = nrows * ncols
    if nplots != nplots_tot:
        warnings.warn(
            "creating space for 'nrows' * 'ncols' = {} * {} = {} subplots "
            "although the 'nplots' input value was {}.".format(
                sys._getframe().f_code.co_name, nrows, ncols, nplots_tot,
                nplots),
            stacklevel=2)

    # Replicate label arguments to the total number of plots
    (
        label, label_pos_x, label_pos_y, label_color, label_pad_x,
        label_pad_y, annotate_kw) = arguments.extend_args(
        label, label_pos_x, label_pos_y, label_color, label_pad_x,
        label_pad_y, annotate_kw, n=nplots)

    # Define default dict arguments
    arguments.set_default(annotate_kw, {})

    # Define default scaling factors and figure size with respect to default
    if h_factor is None:
        h_factor = min(ncols, 2)
    if v_factor is None:
        v_factor = min((1 + nrows) // 2, 2)
    figure_size = [a * b for a, b in zip(
        plt.rcParams['figure.figsize'], [h_factor, v_factor])]

    # Define default layout parameters based on scaling factors
    if left is None:
        left = plt.rcParams['figure.subplot.left'] / h_factor
    if right is None:
        right = 1 - (1 - plt.rcParams['figure.subplot.right']) / h_factor
    if top is None:
        top = 1 - (1 - plt.rcParams['figure.subplot.top']) / v_factor
    if bottom is None:
        bottom = plt.rcParams['figure.subplot.bottom'] / v_factor
    # TODO: make the value subtracted to w/h depend on the default w/hspace
    if wspace is None:
        w = 0.56 * plt.rcParams['figure.subplot.wspace']
        if sharey in [True, 'all', 'row', 'col']:
            if plt.rcParams['ytick.direction'] == "in":
                w -= 0.12
            else:
                w -= 0.15
        wspace = ncols * w / (h_factor * (right - left) - (ncols - 1) * w)
    if hspace is None:
        h = 0.66 * plt.rcParams['figure.subplot.hspace']
        if sharex in [True, 'all', 'row', 'col']:
            if plt.rcParams['xtick.direction'] == "in":
                h -= 0.09
            else:
                h -= 0.12
        hspace = nrows * h / (v_factor * (top - bottom) - (nrows - 1) * h)

    # Create the figure and subplots
    default_fig_kw = dict(figsize=figure_size)
    default_fig_kw.update(fig_kw)
    fig, axes = plt.subplots(
        nrows, ncols, sharex=sharex, sharey=sharey, squeeze=squeeze,
        subplot_kw=subplot_kw, gridspec_kw=gridspec_kw, **default_fig_kw)
    flattened_axes = np.array([axes]).flatten({"column":"F", "row":"C"}[order])

    # Adjust layout parameters
    fig.subplots_adjust(
        left=left, bottom=bottom, right=right, top=top, wspace=wspace,
        hspace=hspace)

    # Hide unused subplots and make the x axis labels of the last plot visible
    for ax in flattened_axes[nplots:nplots_tot]:
        ax.axis('off')
    if sharex and nplots != nplots_tot:
        flattened_axes[nplots - 1].xaxis.set_tick_params(labelbottom=True)

    # Write label for each subplot
    for index, ax in enumerate(flattened_axes[:nplots]):
        if label[index] is None:
            if nplots > 1:
                label[index] = True
            else:
                label[index] = False
        if label[index]:
            if label_pad_x[index] is None:
                label_pad_x[index] = -7 if label_pos_x[index] > 0.5 else 7
            if label_pad_y[index] is None:
                label_pad_y[index] = -9 if label_pos_y[index] > 0.5 else 9
            default_annotate_kw = dict(
                text="({})".format(string.ascii_letters[index + label_start]),
                xy=(label_pos_x[index], label_pos_y[index]),
                xycoords="axes fraction",
                xytext=(label_pad_x[index], label_pad_y[index]),
                textcoords="offset points",
                color=label_color[index],
                ha="right" if label_pos_x[index] > 0.5 else "left",
                va="top" if label_pos_y[index] > 0.5 else "bottom")
            default_annotate_kw.update(annotate_kw[index])
            ax.annotate(**default_annotate_kw)

    # Return the figure and axes
    return fig, axes
