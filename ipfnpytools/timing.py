"""
Timing decorator to print the time a function takes to execute.
"""
from __future__ import print_function

from builtins import str
from functools import wraps
from time import time

def short_string(x, threshold=300, replacement='...', postfix=''):
    # TODO: docstring

    if len(x) == 0:
        x_str = ''
    else:
        x_str = str(x)
        if len(x_str) > threshold:
            x_str = replacement
        x_str += postfix
    return x_str


def timing(f):
    """Timing decorator to print the time a function takes to execute.
    """

    @wraps(f)
    def wrap(*args, **kw):

        # Time and execute function
        ts = time()
        result = f(*args, **kw)
        te = time()

        # Print elapsed time, shortening the argument strings if required
        args_str = short_string(args, postfix=', ')
        kw_str = short_string(kw, replacement="{...}")
        print("{}({}{}) took {:g} seconds".format(
            f.__name__, args_str, kw_str, te-ts))
        return result
    return wrap
