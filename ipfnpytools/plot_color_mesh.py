"""
Create a pseudocolor plot of a 2D quadrilateral mesh with an optional color bar.
"""

import warnings
import numpy as np
import matplotlib.pyplot as plt
from .add_colorbar import add_colorbar
from .default_color_normalization import default_color_normalization


_WARNING_MESSAGE_SIZE_1_ELEMENT = (
    "using a cell size of 1 in the {} direction because it only has one " + 
    "element")

_WARNING_MESSAGE_SIZE_1_DIFFERENT_VALUES = (
    "using the average {} spacing ({:.3g}) as cell size in the {} direction " + 
    "because that dimension has size 1")

_WARNING_MESSAGE_SIZE_1_SAME_VALUES = (
    "using a cell size of {} in the {} direction because that dimension has " + 
    "size 1 and all the points have the same {} value")

_WARNING_MESSAGE_NON_MONOTONIC = (
    "{} data is not monotonic, so the conversion from cell centers to " + 
    "corners may lead to incorrectly calculated cell edges, in which case, " + 
    "please supply explicit cell corners")


#TODO: make it work better with irregular grids, such that the input always corresponds to the center of the output cells?
def centers_to_corners(x, y):
    #TODO: docstring
    """Determine the corners of a grid from its centers."""

    # View inputs as numpy arrays of at least dimension 1
    x, y = np.atleast_1d(x, y)

    # Create an array like the input with extra size for the corners
    x_new, y_new = (
        np.empty(tuple(d + 1 for d in a.shape), dtype=a.dtype) for a in (x, y))

    # Compute corners multiplied by 2 (for more compact formulas) for 
    # single-element inputs
    for a, a_new, s, cell in zip(
            [y, x], [y_new, x_new], ["y", "x"], np.indices((2,2))):
        if a.size == 1:
            warnings.warn(
                _WARNING_MESSAGE_SIZE_1_ELEMENT.format(s), stacklevel=3)            
            a_new[:] = 2 * (a + cell[:a.ndim]) - 1

    # Compute spacing between cell centers and warn if non monotonic
    #TODO: make use of np.gradient?
    x_diff, x_cross_diff, y_diff, y_cross_diff = (
        np.diff(a, axis=ax) for a, ax in zip ([x, x, y, y], [-1, 0, 0, -1]))
    for a, s in zip (
            [x_diff, x_cross_diff, y_diff, y_cross_diff], ["x", "x", "y", "y"]):
        if ((a < 0).any()) & ((a > 0).any()):
            warnings.warn(
                _WARNING_MESSAGE_NON_MONOTONIC.format(s), stacklevel=3)

    # Compute corners multiplied by 2 (for more compact formulas) for 1D inputs
    #TODO: make use of np.diff (see _pcolorargs code of matplotlib?) or np.gradient?
    for a, a_new in zip([x, y], [x_new, y_new]):
        if a.ndim == 1 and a.size != 1:
            a_new[1:-1] = a[:-1] + a[1:]
            a_new[0] = 3 * a[0] - a[1]
            a_new[-1] = 3 * a[-1] - a[-2]

    # Compute corners multiplied by 2 (for more compact formulas) for 2D inputs
    #TODO: make use of np.diff (see _pcolorargs code of matplotlib?) or np.gradient?
    #TODO: improve corner calculation (compare with default behavior of pcolormesh)
    #TODO: factor out code commonalities between x and y
    if x.ndim >= 2 and x.size != 1:
        if x.shape[0] == 1:
            if x.shape[1] == 1:
                x_new[:, 0] = x - 1
                x_new[:, 1] = x + 1
            x_new[:, 1:-1] = x[:, :-1] + x[:, 1:]
            x_new[:, 0] = 3 * x[:, 0] - x[:, 1]
            x_new[:, -1] = 3 * x[:, -1] - x[:, -2]
        elif x.shape[1] == 1:
            x0 = x[:, 0]
            cell_size_x = np.mean(np.abs(x_cross_diff))
            if cell_size_x == 0:
                cell_size_x = 1
                warnings.warn(
                    _WARNING_MESSAGE_SIZE_1_SAME_VALUES.format(
                        cell_size_x, "x", "x"), 
                    stacklevel=3)
            else:
                warnings.warn(
                    _WARNING_MESSAGE_SIZE_1_DIFFERENT_VALUES.format(
                        "x", cell_size_x, "x"), 
                    stacklevel=3)
            x_mid = np.empty_like(x_new[:, 0])
            x_mid[1:-1] = x0[:-1] + x0[1:]
            x_mid[0] = 3 * x0[0] - x0[1]
            x_mid[-1] = 3 * x0[-1] - x0[-2]
            x_new[:, 0] = x_mid - cell_size_x
            x_new[:, 1] = x_mid + cell_size_x
        else:
            x_new[1:-1, 1:-1] = x[:-1, :-1] + x[1:, 1:]
            x_new[0, :-2] = 3 * x[0, :-1] - x[1, 1:]
            x_new[:-2, 0] = 3 * x[:-1, 0] - x[1:, 1]
            x_new[-1, 2:] = 3 * x[-1, 1:] - x[-2, :-1]
            x_new[2:, -1] = 3 * x[1:, -1] - x[:-1, -2]
            x_new[0, -2:] = 3 * x[0, -2:] - x[1, -3:-1]
            x_new[-2:, 0] = 3 * x[-2:, 0] - x[-3:-1, 1]
            x_new[-1, 1] = 3 * x[-1, 1] - x[-2, 2]
            x_new[1, -1] = 3 * x[1, -1] - x[2, -2]
    if y.ndim >= 2 and y.size != 1:
        if y.shape[1] == 1:
            y_new[1:-1] = y[:-1] + y[1:]
            y_new[0] = 3 * y[0] - y[1]
            y_new[-1] = 3 * y[-1] - y[-2]
        elif y.shape[0] == 1:
            y0 = y[0]
            cell_size_y = np.mean(np.abs(y_cross_diff))
            if cell_size_y == 0:
                cell_size_y = 1
                warnings.warn(
                    _WARNING_MESSAGE_SIZE_1_SAME_VALUES.format(
                        cell_size_y, "y", "y"), 
                    stacklevel=3)
            else:
                warnings.warn(
                    _WARNING_MESSAGE_SIZE_1_DIFFERENT_VALUES.format(
                        "y", cell_size_y, "y"), 
                    stacklevel=3)
            y_mid = np.empty_like(y_new[0])
            y_mid[1:-1] = y0[:-1] + y0[1:]
            y_mid[0] = 3 * y0[0] - y0[1]
            y_mid[-1] = 3 * y0[-1] - y0[-2]
            y_new[0] = y_mid - cell_size_y
            y_new[1] = y_mid + cell_size_y
        else:
            y_new[1:-1, 1:-1] = y[:-1, :-1] + y[1:, 1:]
            y_new[0, :-2] = 3 * y[0, :-1] - y[1, 1:]
            y_new[:-2, 0] = 3 * y[:-1, 0] - y[1:, 1]
            y_new[-1, 2:] = 3 * y[-1, 1:] - y[-2, :-1]
            y_new[2:, -1] = 3 * y[1:, -1] - y[:-1, -2]
            y_new[0, -2:] = 3 * y[0, -2:] - y[1, -3:-1]
            y_new[-2:, 0] = 3 * y[-2:, 0] - y[-3:-1, 1]
            y_new[-1, 1] = 3 * y[-1, 1] - y[-2, 2]
            y_new[1, -1] = 3 * y[1, -1] - y[2, -2]

    # Finalize computation (divide by 2) and return the corners
    return tuple(a_new / 2 for a_new in (x_new, y_new))


def corners_to_centers(a):
    #TODO: doctstring
    """Determine the centers of a grid from its corners."""

    # View input as a numpy array
    a = np.asarray(a)

    # Compute centers for 1D input
    if a.ndim == 1:
        return (a[:-1] + a[1:]) / 2

    # Compute centers for 2D input
    return (a[:-1, :-1] + a[:-1, 1:] + a[1:, :-1] + a[1:, 1:]) / 4


# TODO: examples
# TODO: add options for other norms, like symlog, logit, etc. (https://matplotlib.org/stable/tutorials/colors/colormapnorms.html)
# TODO: add data keyword
# TODO: make x and y arguments optional
# TODO: test alpha argument and whether it can be an array like in image_show 
def plot_color_mesh(
        x, y, c, alpha=None, axes=None, log=False, clip_low=0, clip_high=0, 
        portion=0.15, xmin=None, xmax=None, ymin=None, ymax=None, cmap=None, 
        norm=None, vmin=None, vmax=None, shading=None, x_relevant_min=None, 
        x_relevant_max=None, y_relevant_min=None, y_relevant_max=None, 
        normalize_all=False, normalize_along_x=False, normalize_along_y=False, 
        center_c=False, xy_centers_to_corners=None, color_bar=True, 
        color_bar_label=None, add_colorbar_kw=None, **pcolormesh_kw):
    """Create a pseudocolor plot of a 2D quadrilateral mesh with an optional 
    color bar.

    This is basically the matplotlib.axes.Axes.pcolormesh with added features
    and automation.

    Note: The dimensions of 'x' and 'y' can be the same size or one greater
    than those of 'c', with the result depending on the 'shading' and/or on the 
    'center_c' and 'xy_centers_to_corners' keywords.

    Parameters
    ----------
    x, y : 1D or 2D array-like
        The coordinates of the corners (or centers) of the colored 
        quadrilaterals:

            (x[i+1, j], y[i+1, j])       (x[i+1, j+1], y[i+1, j+1])
                                    ●╶───╴●
                                    │     │
                                    ●╶───╴●
                (x[i, j], y[i, j])       (x[i, j+1], y[i, j+1])

        Note that the column index corresponds to the x-coordinate, and the row 
        index corresponds to y. For details, see
        https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.pcolormesh.html#axes-pcolormesh-grid-orientation
        If 'shading'="flat" the dimensions of 'x' and 'y' should be one greater 
        than those of 'c', and the quadrilateral is colored with the value at 
        'c[i, j]'.  If 'x', 'y' and 'c' have equal dimensions, a warning will 
        be raised and the last row and column of 'c' will be ignored.
        If 'shading'="nearest" or "gouraud", the dimensions of 'x' and 'Y' 
        should be the same as those of 'c' (if not, a ValueError will be 
        raised). For "nearest" the color 'c[i, j]' is centered on 
        '(x[i, j], y[i, j])'. For "gouraud", a smooth interpolation is caried 
        out between the quadrilateral corners.
        If 'x' and/or 'y' are 1-D arrays or column vectors they will be 
        expanded as needed into the appropriate 2D arrays, making a 
        rectangular grid.
    c : array-like
        The mesh data. Supported array shapes are:
        - (M, N) or M*N: a mesh with scalar data. The values are mapped to
            colors using normalization and a colormap. See parameters 'norm',
            'cmap', 'vmin', 'vmax'.
        - (M, N, 3): an image with RGB values (0-1 float or 0-255 int).
        - (M, N, 4): an image with RGBA values (0-1 float or 0-255 int),
            i.e. including transparency.
        The first two dimensions (M, N) define the rows and columns of
        the mesh data.
    alpha : float, optional
        The alpha blending value, between 0 (transparent) and 1 (opaque).
    axes : matplotlib.axes.Axes instance, optional
        An Axes instance where the color mesh is plotted. If None, defaults to 
        matplotlib.pyplot.gca().
    log : bool, optional
        If True, use a logarithmic color scale. Defaults to False.
    clip_low, clip_high : float, optional
        Fraction of low / high values to clip, assuming 'portion'==0. Actual 
        clipped fraction is lower if 'portion'>0. Defaults to 0. (currently 
        only implemented for "linear" and "log" 'norm')
    portion : float, optional
        Extra portion of the color scale that is not clipped. Reduces the
        effect of 'clip_low' and 'clip_high'. Defaults to 0.15. (currently only 
        implemented for "linear" and "log" 'norm')
    xmin, xmax, ymin, ymax : float, optional
        minimum / maximum  x / y coordinates to display.
    x_relevant_min, x_relevant_max, y_relevant_min, y_relevant_max : float, optional
        minimum / maximum  x / y coordinates used to determine the default 
        color normalization. If None, defaults to xmin / xmax / ymin / ymax.
    cmap : str or matplotlib.colors.Colormap instance, optional
        The Colormap instance or registered colormap name used to map scalar 
        data to colors. If None, defaults to plt.get_cmap(). 
    norm : str or matplotlib.colors.Normalize instance, optional
        The normalization method used to scale scalar data to the [0, 1] range 
        before mapping to colors using 'cmap'.
        If given, this can be one of the following:
        - An instance of matplotlib.colors.Normalize or one of its subclasses 
        (see https://matplotlib.org/stable/tutorials/colors/colormapnorms.html)
        - A scale name, i.e. one of "linear", "log", "symlog", "logit", etc. 
        For a list of available scales, call 
        matplotlib.scale.get_scale_names(). In that case, a suitable 
        matplotlib.colors.Normalize subclass is dynamically generated and 
        instantiated.
        Overrides 'log'. If None, defaults to "linear" if log is False and to 
        "log" if 'log' is True.
    vmin, vmax : float, optional
        When using scalar data and no explicit 'norm', vmin and vmax define the
        minimum / maximum 'c' values to display. It is an error to use 
        vmin / vmax when a 'norm' instance is given (but using an str 'norm' 
        name together with vmin / vmax is fine). If None, defaults to a range 
        computed based on 'clip_low', 'clip_high', and 'portion' if 'norm' is 
        "linear" or "log", and otherwise to covering the full value range of 
        'c'.
    shading : {'flat', 'nearest', 'gouraud', 'auto'}, optional
        The fill style for the quadrilateral. Possible values:
        - 'flat': A solid color is used for each quad. The color of the
            quad (i, j), (i+1, j), (i, j+1), (i+1, j+1) is given by
            ``c[i, j]``. The dimensions of *x* and *y* should be
            one greater than those of *c*; if they are the same as *c*,
            then a deprecation warning is raised, and the last row
            and column of *c* are dropped.
        - 'nearest': Each grid point will have a color centered on it,
            extending halfway between the adjacent grid centers.  The
            dimensions of *x* and *y* must be the same as *c*.
        - 'gouraud': Each quad will be Gouraud shaded: The color of the
            corners (i', j') are given by ``c[i', j']``. The color values of
            the area in between is interpolated from the corner values.
            The dimensions of *x* and *y* must be the same as *C*.
        - 'auto': Choose 'flat' if dimensions of *x* and *y* are one
            larger than *c*.  Choose 'nearest' if dimensions are the same.
        For more description, see 
        https://matplotlib.org/stable/gallery/images_contours_and_fields/pcolormesh_grids.html
        If None, defaults to matplotlib.rcParams["pcolor.shading"].
    normalize_all: bool, optional
        If True, normalize 'c' to its maximum value (after centering). Defaults 
        to True. (currently only implemented for "linear" and "log" 'norm')
    normalize_along_x, normalize_along_y: bool, optional
        If True, normalize 'c' to its maximum values along the x / y direction
        (after centering). Defaults to False. (currently only implemented for 
        "linear" and "log" 'norm')
    center_c : bool, optional
        If True, transform 'c' to the average of its nearest neighbors (the
        size of each dimension is reduced by 1). Useful when 'c' corresponds
        to the color at each cell corner point ('x', 'y') and not at the center 
        of the cells, but the grid is not uniform and you want to be completely 
        sure the cell geometry does not become distorted (although the colors 
        will be averaged). Defaults to False.
    xy_centers_to_corners : bool, optional
        If True, transform 'x' and 'y' to the grid vertices, assuming they
        represented the centers (the size of each dimension is increased by 1). 
        Useful when one of the dimensions has size 1, which is not handled 
        properly by matplotlib.axes.Axes.pcolormesh. If None, defaults to True 
        if at least one of the dimensions has size 1 and False otherwise.
    color_bar : bool, optional
        If True, draw a color bar. Defaults to True.
    color_bar_label : str, optional
        Color bar label. If None, defaults to no label.
    add_colorbar_kw : dict, optional
        Dict with keywords passed to the ipfnpytools.add_colorbar() call used 
        to create the color bar. If None, defaults to {}.
    **pcolormesh_kw
        All additional keyword arguments are passed to the 
        matplotlib.pyplot.pcolormesh() call.

    Returns
    -------
    matplotlib.collections.QuadMesh instance
        Quadrilateral mesh object.

    Examples
    --------

    """

    # View inputs as numpy arrays of appropriate dimensions
    #TODO: broadcast arrays similarly to plot_scatter to allow a wider range of input dimensions
    x, y = np.atleast_1d(x, y)
    c = np.atleast_2d(c)
    nx = x.shape[-1]
    ny = y.shape[0]
    
    # Get default axes and define other optional keywords
    if axes is None:
        axes = plt.gca()
    if norm is None:
        norm = "log" if log else "linear"
    if add_colorbar_kw is None:
        add_colorbar_kw = {}
    if xy_centers_to_corners is None:
        xy_centers_to_corners = (nx == 1) or (ny == 1)

    # Define default relevant data range for normalizations and colors
    if x_relevant_min is None:
        x_relevant_min = xmin if xmin is not None else x.min()
    if x_relevant_max is None:
        x_relevant_max = xmax if xmax is not None else x.max()
    if y_relevant_min is None:
        y_relevant_min = ymin if ymin is not None else y.min()
    if y_relevant_max is None:
        y_relevant_max = ymax if ymax is not None else y.max()

    # Check input shapes and determine relevant data ranges
    xy_shape = (ny, nx)
    if x.ndim != 2:
        x = np.broadcast_to(x[np.newaxis, :], xy_shape)
    if y.ndim != 2:
        y = np.broadcast_to(y[:, np.newaxis], xy_shape)
    mask_relevant_for_sure = (
        (x >= x_relevant_min) & (x <= x_relevant_max) &
        (y >= y_relevant_min) & (y <= y_relevant_max))
    mask_possibly_relevant = (
            mask_relevant_for_sure |
            np.roll(mask_relevant_for_sure, -1, axis=0) |
            np.roll(mask_relevant_for_sure, -1, axis=1) |
            np.roll(
                np.roll(mask_relevant_for_sure, -1, axis=0), -1, axis=1))
    if xy_shape == (c.shape[0] + 1, c.shape[1] + 1):
        mask_possibly_relevant = mask_possibly_relevant[:-1, :-1]
    else:
        try:
            c = np.broadcast_to(c, shape=(xy_shape + c.shape[2:]))
        except ValueError:
            raise TypeError(
                (
                    'Dimensions of c {} are incompatible with x ({}) and/or ' + 
                    'y ({}).').format(c.shape, nx, ny))
    
    # Normalize data if desired
    #TODO: make it work with c.ndim > 2? (RGB and RGBA values)
    if c.ndim == 2:
        if normalize_all:
            c /= np.nanmax(c[mask_possibly_relevant])
        #TODO: use masked array in more places?
        c_masked = np.ma.array(c, mask=~mask_possibly_relevant)
        if normalize_along_x:
            c /= np.nanmax(c_masked, axis=1)[:, np.newaxis]
        if normalize_along_y:
            c /= np.nanmax(c_masked, axis=0)
        
        # Define default color normalization range
        if norm in ["linear", "log"]:
            vmin, vmax = default_color_normalization(
                c[mask_possibly_relevant], clip_low, clip_high, portion, norm, 
                vmin, vmax)

    # Convert color values from cell corners to centers (averaging) if desired
    if center_c:
        c = corners_to_centers(c)

    # Convert x and y coordinates from cell centers to corners if desired
    if xy_centers_to_corners:
        x, y = centers_to_corners(x, y)

    # Plot color mesh
    default_pcolormesh_kw = dict(
        norm=norm, cmap=cmap, alpha=alpha, vmin=vmin, vmax=vmax, 
        shading=shading)
    default_pcolormesh_kw.update(pcolormesh_kw)
    quadmesh = axes.pcolormesh(x, y, c, **default_pcolormesh_kw)

    # Set visualization range
    mesh_coordinates = quadmesh.get_coordinates()
    mask_mesh_relevant = np.pad(mask_relevant_for_sure, (0, 1))
    mask_mesh_relevant[1:, :] = (
        mask_mesh_relevant[1:, :] | mask_mesh_relevant[:-1, :])
    mask_mesh_relevant[:, 1:] = (
        mask_mesh_relevant[:, 1:] | mask_mesh_relevant[:, :-1])
    mesh_x_relevant = mesh_coordinates[mask_mesh_relevant, 0]
    mesh_y_relevant = mesh_coordinates[mask_mesh_relevant, 1]
    if xmin is None:
        xmin = mesh_x_relevant.min()
    if xmax is None:
        xmax = mesh_x_relevant.max()
    if ymin is None:
        ymin = mesh_y_relevant.min()
    if ymax is None:
        ymax = mesh_y_relevant.max()
    axes.set_xlim(xmin, xmax)
    axes.set_ylim(ymin, ymax)

    # Draw color bar
    if color_bar:
        default_add_colorbar_kw = dict(
            mappable=quadmesh, ax=axes, label=color_bar_label)
        default_add_colorbar_kw.update(add_colorbar_kw)
        add_colorbar(**default_add_colorbar_kw)
    return quadmesh
