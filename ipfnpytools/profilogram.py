#TODO: dosctring
"""
Compute profilogram.
"""

import numpy as np
from .interpolate_curves import interpolate_curves
from .sliding_window import sliding_window
from .isotonic_regression import isotonic_regression


#TODO: use number of pixels instead of number of profiles to average
#TODO: add keywords of called functions as dict arguments
#TODO: check if interpolate, average and isotonic work correctly with 2D non regular x
#TODO: compute and return x and y (average) resolutions
#TODO: allow overlap of average profiles
#TODO: make it work correctly with non-monotonic x
#TODO: treat x and y in the same manner?
def profilogram(
        x, y, z, xmin=None, xmax=None, n_avg=1, interpolate=False,
        isotonic=False, all_x=False, isotonic_regression_kw={},
        **interpolate_curves_kw):
    # TODO: docstring
    """Compute profilogram."""

    # View inputs as numpy arrays of appropriate dimensions
    #TODO: broadcast arrays similarly to plot_scatter to allow a wider range of input dimensions
    x, y, z = np.atleast_1d(x, y, z)
    if all(a.ndim == 1 for a in (x, y, z)):
        y, z = (
            a.reshape(x.size, a.size // x.size) 
            for a in np.broadcast_arrays(y, z))

    # Define default x range
    if xmin is None:
        xmin = x.min()
    if xmax is None:
        xmax = x.max()

    # Determine potentially visible portion of the data
    if not all_x:
        visible_x_indices = np.nonzero((x >= xmin) & (x <= xmax))[0]
        if visible_x_indices.size > 0:
            visible_x_slice = slice(
                max(visible_x_indices[0] - 2, 0), visible_x_indices[-1] + 3)
            x = x[visible_x_slice]
            if y.ndim == 2:
                y = y[visible_x_slice]
            if z.ndim == 2:
                z = z[visible_x_slice]
        else:
            return tuple(a[[]] for a in (x, y, z))

    # Interpolate profiles if desired
    if interpolate:
        y, z = interpolate_curves(y, z, **interpolate_curves_kw)

    # Average profiles if desired
    if n_avg > 1:
        def avg(a):
            return sliding_window(a, n_avg, n_avg, axis=0, copy=False).mean(
                axis=-1)
        x = avg(x)
        if y.ndim >= 2:
            y = avg(y)
        if z.ndim >= 2:
            z = avg(z)

    # Apply isotonic regression if desired
    if isotonic:
        default_isotonic_regression_kw = dict(strict=True)
        default_isotonic_regression_kw.update(isotonic_regression_kw)
        y = np.apply_along_axis(
            isotonic_regression, -1, y, **default_isotonic_regression_kw)

    return x, y, z
